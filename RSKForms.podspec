Pod::Spec.new do |s|
 
  # 1
  s.platform = :ios
  s.ios.deployment_target = '8.2'
  s.name = "rskforms"
  s.summary = "RSKForms is a way to create views faster than storyboards."
  s.requires_arc = true
 
  # 2
  s.version = "0.1.6"
 
  # 3
  s.license = { :type => "MIT", :file => "LICENSE" }
 
  # 4 - Replace with your name and e-mail address
  s.author = { "Vladimir Espinola" => "vespinola@roshka.com" }
 
  # For example,
  # s.author = { "Joshua Greene" => "jrg.developer@gmail.com" }
 
 
  # 5 - Replace this URL with your own Github page's URL (from the address bar)
  s.homepage = "https://bitbucket.org/vespinola/rskforms"
 
  # For example,
  # s.homepage = "https://github.com/JRG-Developer/RWPickFlavor"
 
 
  # 6 - Replace this URL with your own Git URL from "Quick Setup"
  s.source = { :git => "https://vespinola@bitbucket.org/vespinola/rskforms.git", :tag => "#{s.version}"}
 
  # For example,
  # s.source = { :git => "https://github.com/JRG-Developer/RWPickFlavor.git", :tag => "#{s.version}"}
 
 
  # 7
  s.framework = "UIKit"
  s.dependency 'BRYXBanner' , '~> 0.7.1' 
  s.dependency 'NVActivityIndicatorView', '~> 3.1'

  # 8
  s.source_files = "rskforms/**/*.{swift}"
 
  # 9
  s.resources = "rskforms/**/*.{png,jpeg,jpg,storyboard,xib}"
end
