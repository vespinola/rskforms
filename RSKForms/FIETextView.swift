//
//  FIETextView.swift
//  Fielco-iOS
//
//  Created by Roshka on 6/17/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

class FIETextView: UITextView {

    override func becomeFirstResponder() -> Bool {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "InputOnFocus"), object: nil, userInfo: [ "view": self ])
        return super.becomeFirstResponder()
    }
}
