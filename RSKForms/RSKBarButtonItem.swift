//
//  RSKBarButtonItem.swift
//  Fielco-iOS
//
//  Created by Juan Carlos Gomez on 4/28/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

class RSKBarButtonItem: UIBarButtonItem {
    
    var onClick: ((RSKBarButtonItem) -> Void)?
    
    convenience init(title: String?, onClick: ((RSKBarButtonItem) -> Void)!) {
        self.init(title: title, style: UIBarButtonItemStyle.plain, onClick: onClick)
    }
    
    convenience init(title: String?, style: UIBarButtonItemStyle, onClick: ((RSKBarButtonItem) -> Void)!) {
        self.init(title: title, style: style, target: nil, action: nil)
        self.target = self
        self.action = #selector(RSKBarButtonItem.buttonPressed(_:))
        self.onClick = onClick
    }
    
    convenience init(named: String, action: ((RSKBarButtonItem) -> Void)!) {
        let image = UIImage(named: named)?.withRenderingMode(.alwaysOriginal)
        self.init(image: image, style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        self.target = self
        self.action = #selector(RSKBarButtonItem.buttonPressed(_:))
        self.onClick = action
    }
    
    convenience init(barButtonSystemItem: UIBarButtonSystemItem, onClick: ((RSKBarButtonItem) -> Void)!) {
        self.init(barButtonSystemItem: barButtonSystemItem, target: nil, action: #selector(RSKBarButtonItem.buttonPressed(_:)))
        self.target = self
        self.onClick = onClick
    }
    
    func buttonPressed(_ sender: RSKBarButtonItem) {
        
        onClick?(sender)
    
    }
    
    
    

}
