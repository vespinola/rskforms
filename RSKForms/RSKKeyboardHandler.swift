//
//  RSKKeyboardHandler.swift

//
//  Created by Juan Carlos Gomez on 5/4/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

class RSKKeyboardHandler: NSObject {
    
    var scrollView: UIScrollView?
    var activeResponder: UIView?
    
    
    override init() {
        super.init()
        
        // Register for keyboard appearance notifications so we can make sure no fields are ever hidden by the keyboard.
        NotificationCenter.default.addObserver(self, selector: #selector(RSKKeyboardHandler.inputViewWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RSKKeyboardHandler.inputViewWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(RSKKeyboardHandler.scrollViewOnAdd(_:)), name: NSNotification.Name(rawValue: "ScrollViewOnAdd"), object: nil)
        //        NSNotificationCenter.defaultCenter().addObserver(self, selector: "scrollViewOnRemove:", name: BFConstants.Notifications.ScrollViewOnRemove, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(RSKKeyboardHandler.inputOnFocus(_:)), name: NSNotification.Name(rawValue: "InputOnFocus"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RSKKeyboardHandler.inputOnBlur(_:)), name: NSNotification.Name(rawValue: "InputOnBlur"), object: nil)
    }
    
    func scrollViewOnAdd(_ notification: Notification) {
        self.scrollView = (notification as NSNotification).userInfo!["scrollView"] as? UIScrollView
    }
    
    func inputOnFocus(_ notification: Notification) {
        self.activeResponder = (notification as NSNotification).userInfo!["view"] as? UIView
    }
    
    func inputOnBlur(_ notification: Notification) {
        self.activeResponder = nil
    }
    
    func inputViewWillShow(_ notification: Notification) {
        if scrollView != nil && activeResponder != nil {
            // Make the scroll view content available in the are not covered by the keyboard.
            let info: NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
            let value: NSValue = info.value(forKey: UIKeyboardFrameBeginUserInfoKey) as! NSValue
            let keyboardRect = value.cgRectValue
            print("keyboardRect: \(keyboardRect)")
            let contentInsets = UIEdgeInsetsMake(0, 0, keyboardRect.height, 0)
            print("contentInsets: \(contentInsets)")
            scrollView!.contentInset = contentInsets
            scrollView!.scrollIndicatorInsets = contentInsets
            let activeResponderRect = scrollView!.convert(activeResponder!.frame, from: activeResponder!.superview!)
            print("activeResponderRect: \(activeResponderRect)")
            print("self.activeScrollView!.frame: \(scrollView!.frame)")
            //        self.mainScrollView.setContentOffset(CGPointZero, animated: true)
            let visibleRect = UIEdgeInsetsInsetRect(scrollView!.frame, contentInsets)
            print("visibleRect: \(visibleRect)")
            
            if !visibleRect.contains(activeResponderRect) {
                let yOffset = -(activeResponderRect.origin.y - visibleRect.size.height) - activeResponderRect.height - 10 // Add here extra padding between bottom of input and top of keyboard
                print("yOffset: \(yOffset)")
                scrollView!.setContentOffset(CGPoint(x: 0, y: -yOffset), animated: true)
            }
        }
        
    }
    
    func inputViewWillHide(_ notification: Notification) {
        if self.scrollView != nil {
            self.scrollView!.contentInset = UIEdgeInsets.zero
            self.scrollView!.scrollIndicatorInsets = UIEdgeInsets.zero
            self.scrollView!.setContentOffset(CGPoint.zero, animated: true)
        }
    }
    
    class func rsk_handleScrollView(_ scrollView: UIScrollView?, forActiveResponder activeResponder: UITextField?, withNotification notification: Notification) {
        
    }
}
