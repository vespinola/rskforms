//
//  FIETimelineLoanView.swift
//  Fielco-iOS
//
//  Created by Roshka on 7/8/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

class FIETimelineLoanView: FIETimelineView {

    init(_ loan: RSKDictionary) {
        super.init(rows: [
            (
                "Préstamo a Pagar",
                FIEUtility.loanNameAndNumber(loan)
            )
        ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
