//
//  FIETimelineView.swift
//  Fielco-iOS
//
//  Created by Paul Von Schrottky on 6/14/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

typealias FIETimelineRow = (titleString: String?, subtitleString: String?)

class FIETimelineView: UIView {

    fileprivate var hasCreatedConstraints = false
    
    var rows: [FIETimelineRow]!
    var allLabels = [(titleLabel: UILabel, subtitleLabel: UILabel)]()
    var hasLaidoutConstraints = false
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    init(rows: [FIETimelineRow]) {
        super.init(frame: CGRect.zero)
        self.rows = rows
        
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = FIEConstants.Colors.Background
        
        for row in rows {
            let (titleLabel, subtitleLabel) = (UILabel(), UILabel())
            
            addSubview(titleLabel)
            addSubview(subtitleLabel)
            titleLabel.text = row.titleString
            subtitleLabel.text = row.subtitleString
            allLabels.append((titleLabel, subtitleLabel))
            
            [titleLabel, subtitleLabel].forEach {
                $0.lineBreakMode = .byWordWrapping
                $0.numberOfLines = 0
            }
            
            titleLabel.textColor = FIEConstants.Colors.Blue
            subtitleLabel.textColor = FIEConstants.Colors.Text
            
            [titleLabel, subtitleLabel].forEach { $0.font = FIEConstants.Fonts.MediumBold }

        }
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        if hasLaidoutConstraints == false {
            for (index, labelPair) in allLabels.enumerated() {
                let (titleLabel, subtitleLabel) = labelPair
                [titleLabel, subtitleLabel].forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
                
                addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[titleLabel]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["titleLabel": titleLabel]))
                addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[subtitleLabel]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["subtitleLabel": subtitleLabel]))
                addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[titleLabel][subtitleLabel]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["titleLabel": titleLabel, "subtitleLabel": subtitleLabel]))
                
                if index == 0 {
                    addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .top, relatedBy: .equal, toItem: self, attribute: .topMargin, multiplier: 1, constant: 0))
                } else {
                    addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .top, relatedBy: .equal, toItem: allLabels[index - 1].subtitleLabel, attribute: .bottom, multiplier: 1, constant: FIEUtility.HalfSpace))
                }
            }
            
            if let lastSubtitleLabel = allLabels.last?.subtitleLabel {
                addConstraint(NSLayoutConstraint(item: lastSubtitleLabel, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottomMargin, multiplier: 1, constant: 0))
            }
            
            hasLaidoutConstraints = true
        }
    }

}
