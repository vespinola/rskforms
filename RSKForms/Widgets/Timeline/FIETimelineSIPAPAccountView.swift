//
//  FIETimelineSIPAPAccountView.swift
//  Fielco-iOS
//
//  Created by Paul Von Schrottky on 6/14/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

struct FIETransactionTransferSIPAPInfo {
    var bank: String!
    var bankName: String!
    var mode: FIESIPAPMode!
    var concept: String!
    var destinations = [FIETransactionTransferSIPAPDestinationInfo]()
}

struct FIETransactionTransferSIPAPDestinationInfo {
    var destinationAccount: String!
    var documentType: Int!
    var documentTypeName: String!
    var documentNumber: String!
    var userName: String!
    var zone: String!
    var zoneName: String!
    var city: String!
    var transactionAmount: FIETransactionAmount?
}

struct FIETransactionTransferOtherInfo {
    var documentCountry: Int!
    var documentCountryName: String!
    var documentType: Int!
    var documentTypeName: String!
    var documentNumber: String!
    var accountNumber: String?
    var accountNumberComponents: [String]? {
        if let accountNumber = accountNumber?.rsk_trimmed {
            let components = accountNumber.components(separatedBy: "-")
            
            guard components.count == 2 &&
                !components[0].isEmpty &&
                !components[1].isEmpty
                else {
                    return nil
            }
            
            return [components.first!, components.last!]
        }
        return nil
    }
    var isAccountNumberValid: Bool {
        guard let components = accountNumberComponents , components.count == 2 else { return false }
        return Int(components[0]) != nil && Int(components[1]) != nil
    }
    var accountNumberBeginning: String? {
        guard isAccountNumberValid else { return nil }
        return accountNumberComponents!.first!
    }
    var accountNumberEnd: String? {
        guard isAccountNumberValid else { return nil }
        return accountNumberComponents!.last!
    }
    var pin: String?
    var recipientAccountAlias: String!
    var recipientName: String!
}


class FIETimelineSIPAPAccountView: FIETimelineView {

    init(account: RSKDictionary, info: FIETransactionTransferSIPAPInfo) {
        super.init(rows: [
            ("Cuenta a debitar", FIEUtility.accountNameNumberAndAmount(account)),
            ("Crédito", info.bank),
            ("Modalidad", info.mode.value()),
            ("Motivo", info.concept),
        ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
