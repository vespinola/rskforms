//
//  FIETimelineAccountView.swift
//  Fielco-iOS
//
//  Created by Paul Von Schrottky on 6/14/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

class FIETimelineAccountView: FIETimelineView {

    init(_ account: RSKDictionary, title: String? = nil) {
        var text = title
        if text == nil {
            text = "Cuenta a debitar"
        }
        super.init(rows: [
            (text, FIEUtility.accountNameNumberAndAmount(account))
        ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}