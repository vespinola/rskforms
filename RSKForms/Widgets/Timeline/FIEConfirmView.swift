//
//  FIEConfirmView.swift
//  Fielco-iOS
//
//  Created by Juan Carlos Gomez on 6/16/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

//import UIKit

class FIEConfirmView: UIView {

    fileprivate var hasUpdatedConstraints = false

    fileprivate var confirm: FIEConfirm!
    var closeButton: UIButton!
    var titleLabel: UILabel!
    var codeTextField: FIETextField!
    var extraLabel: UILabel!
    var acceptButton: UIButton!
    var onCloseCallback: ((String?) -> Void)?

    func onClose(_ callback: @escaping ((String?) -> Void)) {
        onCloseCallback = callback
    }

    init(title: String, extra: String? = nil, confirm: FIEConfirm, keyboardType: UIKeyboardType = UIKeyboardType.numberPad) {
        super.init(frame: CGRect.zero)
        self.confirm = confirm
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = FIEConstants.Colors.Foreground

        closeButton = UIButton.rsk_newAutolayoutInParentView(self)
        let image = UIImage(named: "close-white")?.withRenderingMode(.alwaysTemplate)
        closeButton.tintColor = FIEConstants.Colors.Grey
        closeButton.setImage(image, for: UIControlState())
        closeButton.addTarget(self, action: #selector(closeButtonAction), for: .touchUpInside)
    

        titleLabel = UILabel.rsk_newAutolayoutInParentView(self)
        titleLabel.text = title
        titleLabel.font = FIEConstants.Fonts.SizeMediumWeightMedium
        titleLabel.textColor = FIEConstants.Colors.Blue
        titleLabel.textAlignment = .center
    

        codeTextField = FIETextField.rsk_newAutolayoutInParentView(self)
        codeTextField.autocorrectionType = .no
        codeTextField.autocapitalizationType = .none
        codeTextField.keyboardType = keyboardType
        codeTextField.placeholder = "Ingresá el código"
        codeTextField.font = FIEConstants.Fonts.SizeSmallWeightBold
        codeTextField.textColor = FIEConstants.Colors.GreyLight
        codeTextField.borderStyle = .roundedRect
        codeTextField.textAlignment = .center
        

        extraLabel = UILabel.rsk_newAutolayoutInParentView(self)
        extraLabel.text = extra
        extraLabel.font = FIEConstants.Fonts.SizeSmallWeightLight
        extraLabel.lineBreakMode = .byWordWrapping
        extraLabel.numberOfLines = 0
        extraLabel.textColor = FIEConstants.Colors.Text
        extraLabel.textAlignment = .center

        acceptButton = UIButton.rsk_newAutolayoutInParentView(self)
        acceptButton.backgroundColor = FIEConstants.Colors.Orange
        acceptButton.titleLabel?.font = FIEConstants.Fonts.SizeSmallWeightBold
        acceptButton.setTitle("ACEPTAR", for: UIControlState())
        acceptButton.titleLabel?.textColor = UIColor.white
        acceptButton.layer.masksToBounds = true
        acceptButton.addTarget(self, action: #selector(acceptButtonAction), for: .touchUpInside)
    
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


    func closeButtonAction(_ sender: UIButton) {
        if codeTextField.isFirstResponder {
            codeTextField.resignFirstResponder()
        } else {
            confirm.close()
        }
    }

    func acceptButtonAction(_ sender: UIButton) {
        if codeTextField.isFirstResponder {
            codeTextField.resignFirstResponder()
        } else {
            confirm.close()
        }
        onCloseCallback?(codeTextField.text)
    }

    override func updateConstraints() {
        if hasUpdatedConstraints == false {
            hasUpdatedConstraints = true

            let views = [
                "closeButton"       : closeButton,
                "titleLabel"        : titleLabel,
                "codeTextField"     : codeTextField,
                "extraLabel"        : extraLabel,
                "acceptButton"      : acceptButton,
                ] as [String : Any]

            let closeButtonSize = CGSize(width: 40, height: 40)

            addConstraint(NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 0, constant: 300))

            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[closeButton(\(closeButtonSize.width))]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[closeButton(\(closeButtonSize.height))]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))

            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[titleLabel]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[titleLabel]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[codeTextField]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[extraLabel]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[acceptButton]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))

            if extraLabel.text != nil {
                addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(\(closeButtonSize.height + FIEUtility.StdSpace))-[titleLabel]-[codeTextField]-[extraLabel]-[acceptButton]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
            } else {
                addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(\(closeButtonSize.height + FIEUtility.StdSpace))-[titleLabel]-[codeTextField]-[acceptButton]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
            }

        }
        super.updateConstraints()
    }
}
