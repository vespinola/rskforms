//
//  FIETimelineAccountOtherView.swift
//  Fielco-iOS
//
//  Created by Roshka on 7/11/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

class FIETimelineAccountOtherView: FIETimelineView {
    
    init(info: FIETransactionTransferOtherInfo) {
        super.init(rows: [
            ("Cuenta destino", info.accountNumber),
            ("Origen de documento", info.documentCountryName),
            ("Tipo de documento", info.documentTypeName),
            ("Número de documento", info.documentNumber),
        ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
