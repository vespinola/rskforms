//
//  FIEConfirm.swift
//  Fielco-iOS
//
//  Created by Roshka on 6/17/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

class FIEConfirm {
    
    fileprivate static let confirm = FIEConfirm()
    fileprivate var window: UIWindow?
    var bottomLayoutConstraint: NSLayoutConstraint!
    
    class func show(title: String = "Código de confirmación", extra: String? = nil, keyboardType: UIKeyboardType = UIKeyboardType.numberPad) -> FIEConfirmView {
        return confirm.display(title, extra: extra, keyboardType: keyboardType)
    }
    
    func display(_ title: String, extra: String? = nil, keyboardType: UIKeyboardType) -> FIEConfirmView {
        
        NotificationCenter.default.addObserver(self, selector: #selector(inputViewWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(inputViewWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let margin = FIEUtility.StdSpace
        let width = UIScreen.main.bounds.size.width - margin * 2
        window = UIWindow()
        window!.windowLevel = UIWindowLevelAlert
        window!.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        window!.makeKeyAndVisible()
        
        let backgroundView = UIView.rsk_newAutolayoutInParentView(window!)
        window!.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[backgroundView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["backgroundView": backgroundView]))
        window!.addConstraint(NSLayoutConstraint(item: backgroundView, attribute: .top, relatedBy: .equal, toItem: window, attribute: .top, multiplier: 1, constant: 0))
        bottomLayoutConstraint = NSLayoutConstraint(item: backgroundView, attribute: .bottom, relatedBy: .equal, toItem: window, attribute: .bottom, multiplier: 1, constant: 0)
        window!.addConstraint(bottomLayoutConstraint)
        
        let confirmDialog = FIEConfirmView(title: title, extra: extra, confirm: self, keyboardType: keyboardType)
        backgroundView.addSubview(confirmDialog)
        confirmDialog.translatesAutoresizingMaskIntoConstraints = false
        confirmDialog.rsk_centerInParentView()
        
        return confirmDialog
        
    }
    
    @objc func inputViewWillShow(_ notification: Notification) {
        let info: NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let value: NSValue = info.value(forKey: UIKeyboardFrameBeginUserInfoKey) as! NSValue
        let keyboardRect = value.cgRectValue
        print("keyboardRect: \(keyboardRect)")
        
        let animationDuration = info.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as! NSNumber
        let curve = (info[UIKeyboardAnimationCurveUserInfoKey] as! NSValue) as! UInt
        
        
        bottomLayoutConstraint.constant = -keyboardRect.height
        
        UIView.animate(withDuration: animationDuration.doubleValue, delay: 0, options: UIViewAnimationOptions(rawValue: curve), animations: {
            self.window?.layoutIfNeeded()
        }, completion: nil)
    }
    
    @objc func inputViewWillHide(_ notification: Notification) {
        let info: NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let value: NSValue = info.value(forKey: UIKeyboardFrameBeginUserInfoKey) as! NSValue
        let keyboardRect = value.cgRectValue
        print("keyboardRect: \(keyboardRect)")
        
        let animationDuration = info.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as! NSNumber
        let curve = (info[UIKeyboardAnimationCurveUserInfoKey] as! NSValue) as! UInt
        
        bottomLayoutConstraint.constant = 0
        self.window?.alpha = 0
        UIView.animate(withDuration: animationDuration.doubleValue, delay: 0, options: UIViewAnimationOptions(rawValue: curve), animations: {
            self.window?.layoutIfNeeded()
        }, completion: { _ in
            self.close()
        })
    }
    
    func close() {
        self.window?.isHidden = true
        self.window = nil
    }
}


//class FIEConfirmView: UIView {
//    
//    private var hasUpdatedConstraints = false
//    
//    private var confirm: FIEConfirm!
//    var closeButton: UIButton!
//    var titleLabel: UILabel!
//    var codeTextField: FIETextField!
//    var extraLabel: UILabel!
//    var acceptButton: UIButton!
//    var onCloseCallback: (String? -> Void)?
//    
//    func onClose(callback: (String? -> Void)) {
//        onCloseCallback = callback
//    }
//    
//    init(title: String, extra: String? = nil, confirm: FIEConfirm, keyboardType: UIKeyboardType = UIKeyboardType.NumberPad) {
//        super.init(frame: CGRectZero)
//        self.confirm = confirm
//        translatesAutoresizingMaskIntoConstraints = false
//        backgroundColor = FIEConstants.Colors.Foreground
//        
//        closeButton = UIButton.rsk_newAutolayoutInParentView(self).then {
//            let image = UIImage(named: "close-white")?.imageWithRenderingMode(.AlwaysTemplate)
//            $0.tintColor = FIEConstants.Colors.Grey
//            $0.setImage(image, forState: .Normal)
//            $0.addTarget(self, action: #selector(closeButtonAction), forControlEvents: .TouchUpInside)
//        }
//        
//        titleLabel = UILabel.rsk_newAutolayoutInParentView(self).then {
//            $0.text = title
//            $0.font = FIEConstants.Fonts.SizeMediumWeightMedium
//            $0.textColor = FIEConstants.Colors.Blue
//            $0.textAlignment = .Center
//        }
//        
//        codeTextField = FIETextField.rsk_newAutolayoutInParentView(self).then {
//            $0.autocorrectionType = .No
//            $0.autocapitalizationType = .None
//            $0.keyboardType = keyboardType
//            $0.placeholder = "Ingresá el código"
//            $0.font = FIEConstants.Fonts.SizeSmallWeightBold
//            $0.textColor = FIEConstants.Colors.GreyLight
//            $0.borderStyle = .RoundedRect
//            $0.textAlignment = .Center
//        }
//        
//        extraLabel = UILabel.rsk_newAutolayoutInParentView(self).then {
//            $0.text = extra
//            $0.font = FIEConstants.Fonts.SizeSmallWeightLight
//            $0.lineBreakMode = .ByWordWrapping
//            $0.numberOfLines = 0
//            $0.textColor = FIEConstants.Colors.Text
//            $0.textAlignment = .Center
//        }
//        
//        acceptButton = UIButton.rsk_newAutolayoutInParentView(self).then {
//            $0.backgroundColor = FIEConstants.Colors.Orange
//            $0.titleLabel?.font = FIEConstants.Fonts.SizeSmallWeightBold
//            $0.setTitle("ACEPTAR", forState: .Normal)
//            $0.titleLabel?.textColor = UIColor.whiteColor()
//            $0.layer.masksToBounds = true
//            $0.addTarget(self, action: #selector(acceptButtonAction), forControlEvents: .TouchUpInside)
//        }
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//    
//    
//    func closeButtonAction(sender: UIButton) {
//        if codeTextField.isFirstResponder() {
//            codeTextField.resignFirstResponder()
//        } else {
//            confirm.close()
//        }
//    }
//    
//    func acceptButtonAction(sender: UIButton) {
//        if codeTextField.isFirstResponder() {
//            codeTextField.resignFirstResponder()
//        } else {
//            confirm.close()
//        }
//        onCloseCallback?(codeTextField.text)
//    }
//    
//    override func updateConstraints() {
//        if hasUpdatedConstraints == false {
//            hasUpdatedConstraints = true
//            
//            let views = [
//                "closeButton"       : closeButton,
//                "titleLabel"        : titleLabel,
//                "codeTextField"     : codeTextField,
//                "extraLabel"        : extraLabel,
//                "acceptButton"      : acceptButton,
//                ]
//            
//            let closeButtonSize = CGSizeMake(40, 40)
//            
//            addConstraint(NSLayoutConstraint(item: self, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 0, constant: 300))
//            
//            addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:[closeButton(\(closeButtonSize.width))]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
//            addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-[closeButton(\(closeButtonSize.height))]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
//            
//            addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-[titleLabel]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
//            addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-[titleLabel]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
//            addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-[codeTextField]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
//            addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-[extraLabel]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
//            addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-[acceptButton]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
//            
//            if extraLabel.text != nil {
//                addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-(\(closeButtonSize.height + FIEUtility.StdSpace))-[titleLabel]-[codeTextField]-[extraLabel]-[acceptButton]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
//            } else {
//                addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-(\(closeButtonSize.height + FIEUtility.StdSpace))-[titleLabel]-[codeTextField]-[acceptButton]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
//            }
//            
//        }
//        super.updateConstraints()
//    }
//}
