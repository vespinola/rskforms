//
//  FIEFooterView.swift
//  Fielco-iOS
//
//  Created by Roshka on 6/24/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

enum FIEFooterState {
    case closed
    case open
}

enum FIEHomeFooterState {
    case closed
    case open
}

class FIEFooterView: UIView {
    
    var leftButtonCallback: ((Void) -> Void)?
    var centerButtonCallback: ((Void) -> Void)?
    var rightButtonCallback: ((Void) -> Void)?
    
    var leftView: UIView!
    var centerView: UIView!
    var rightView: UIView!
    var toggleView: UIView!
    var toggleVisibleView: UIView!
    var arrowImageView: UIImageView!
    var toggleVisibleRoundedView: UIView!
    
    var leftButton: UIButton!
    var centerButton: UIButton!
    var rightButton: UIButton!
    var toggleButton: UIButton!
    
    var heightLayoutConstraint: NSLayoutConstraint!
    
    var footerState = FIEHomeFooterState.closed
    
    fileprivate var hasCreatedConstraints = false
    
    static let DEFAULT_HEIGHT: CGFloat = 15
    static let EXPANDED_HEIGHT: CGFloat = 50
    static let ANIMATION_DURATION: TimeInterval = 0.5
    static let TOGGLE_VIEW_HEIGHT: CGFloat = 45
    static let TOGGLE_BUTTON_VISIBLE_HEIGHT: CGFloat = 22
    
    fileprivate var containerView: UIView? {
        return self.superview?.superview
    }

    init() {
        super.init(frame: CGRect.zero)
        translatesAutoresizingMaskIntoConstraints = false
        
        // It's important that this view is added before leftView, centerView, and rightView so that
        // its shadow is clipped and no bottom shadow is visible.
        toggleVisibleView = UIView.rsk_newAutolayoutInParentView(self)
        leftView = UIView.rsk_newAutolayoutInParentView(self)
        centerView = UIView.rsk_newAutolayoutInParentView(self)
        rightView = UIView.rsk_newAutolayoutInParentView(self)
        toggleView = UIView.rsk_newAutolayoutInParentView(self)
        toggleVisibleRoundedView = UIView.rsk_newAutolayoutInParentView(toggleVisibleView)
        arrowImageView = UIImageView.rsk_newAutolayoutInParentView(toggleVisibleRoundedView)
        
        
        leftButton = UIButton.rsk_newAutolayoutInParentView(leftView)
        centerButton = UIButton.rsk_newAutolayoutInParentView(centerView)
        rightButton = UIButton.rsk_newAutolayoutInParentView(rightView)
        toggleButton = UIButton.rsk_newAutolayoutInParentView(toggleView)
        
        leftButton.addTarget(self, action: #selector(leftButtonAction), for: .touchUpInside)
        centerButton.addTarget(self, action: #selector(centerButtonAction), for: .touchUpInside)
        rightButton.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
        toggleButton.addTarget(self, action: #selector(toggleButtonAction), for: .touchUpInside)
        
        arrowImageView.image = UIImage(named: "views_icon_arrow_white-up")
        arrowImageView.contentMode = .center
        
        leftView.backgroundColor = FIEConstants.Colors.Red
        centerView.backgroundColor = FIEConstants.Colors.Orange
        rightView.backgroundColor = FIEConstants.Colors.Yellow
        toggleView.backgroundColor = UIColor.clear
        toggleVisibleView.backgroundColor = UIColor.clear
        toggleVisibleRoundedView.backgroundColor = centerView.backgroundColor
        
        [leftButton, centerButton].forEach {
            $0?.titleLabel?.numberOfLines = 0
            $0?.titleLabel?.lineBreakMode = .byWordWrapping
            $0?.titleLabel?.font = FIEConstants.Fonts.SizeVerySmallWeightHeavy
            $0?.setTitleColor(FIEConstants.Colors.TextLight, for: UIControlState())
        }
        
        let buttonEdgeInsets = UIEdgeInsetsMake(6, 6, 6, 6)
        leftButton.titleEdgeInsets = buttonEdgeInsets
        centerButton.titleEdgeInsets = buttonEdgeInsets
        rightButton.imageView?.contentMode = .scaleAspectFit
        rightButton.imageEdgeInsets = buttonEdgeInsets
        rightButton.backgroundColor = FIEConstants.Colors.Yellow
        
        clipsToBounds = false
        

    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // Shadow
        containerView?.setNeedsLayout()
        containerView?.layoutIfNeeded()
        toggleVisibleView.clipsToBounds = false
        toggleVisibleView.layer.shadowColor = UIColor.black.cgColor
        toggleVisibleView.layer.shadowOpacity = 0.7
        toggleVisibleView.layer.shadowRadius = 3    // Default
        toggleVisibleView.layer.shadowOffset = CGSize(width: 0, height: 0)
        let shadowRect = CGRect(x: 0, y: 0, width: toggleVisibleView.bounds.width, height: toggleVisibleView.bounds.height + 5)
        toggleVisibleView.layer.shadowPath = UIBezierPath(rect: shadowRect).cgPath
        
        // Rounded corners
        let path = UIBezierPath(roundedRect:toggleVisibleRoundedView.bounds, byRoundingCorners:[.topLeft, .topRight], cornerRadii: CGSize(width: 3, height: 3))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        toggleVisibleRoundedView.layer.mask = maskLayer
    }
    
    override func updateConstraints() {
        
        if hasCreatedConstraints == false {
            
            [leftButton, centerButton, rightButton].forEach { self.fillParentView($0) }
            
            let views = ["leftView": leftView, "centerView": centerView, "rightView": rightView]
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[leftView(==centerView)][centerView(==rightView)][rightView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
            
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[leftView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[centerView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[rightView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
            
            heightLayoutConstraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 0, constant: FIEFooterView.DEFAULT_HEIGHT)
            addConstraint(heightLayoutConstraint)
            
            // Toggle view (sits on top of centerView, invisible)
            addConstraint(NSLayoutConstraint(item: toggleView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 0, constant: FIEFooterView.TOGGLE_VIEW_HEIGHT))
            addConstraint(NSLayoutConstraint(item: toggleView, attribute: .left, relatedBy: .equal, toItem: centerView, attribute: .left, multiplier: 1, constant: 0))
            addConstraint(NSLayoutConstraint(item: toggleView, attribute: .right, relatedBy: .equal, toItem: centerView, attribute: .right, multiplier: 1, constant: 0))
            addConstraint(NSLayoutConstraint(item: toggleView, attribute: .top, relatedBy: .equal, toItem: centerView, attribute: .top, multiplier: 1, constant: -FIEFooterView.TOGGLE_VIEW_HEIGHT))
            
            // Toggle button (fills toggleView, invisible)
            fillParentView(toggleButton)
            
            // Toggle visible view (fills bottom part of toggleView)
            addConstraint(NSLayoutConstraint(item: toggleVisibleView, attribute: .left, relatedBy: .equal, toItem: toggleView, attribute: .left, multiplier: 1, constant: 0))
            addConstraint(NSLayoutConstraint(item: toggleVisibleView, attribute: .right, relatedBy: .equal, toItem: toggleView, attribute: .right, multiplier: 1, constant: 0))
            addConstraint(NSLayoutConstraint(item: toggleVisibleView, attribute: .bottom, relatedBy: .equal, toItem: toggleView, attribute: .bottom, multiplier: 1, constant: 1))   // The 1 removes flickering during animation
            addConstraint(NSLayoutConstraint(item: toggleVisibleView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: FIEFooterView.TOGGLE_BUTTON_VISIBLE_HEIGHT))
            
            // Arrow image view
            fillParentView(arrowImageView)
            
            fillParentView(toggleVisibleRoundedView)
            
            hasCreatedConstraints = true
        }
        
        super.updateConstraints()
    }
    
    func fillParentView(_ childView: UIView) {
        let parentView = childView.superview!
        parentView.superview!.addConstraint(NSLayoutConstraint(item: childView, attribute: .left, relatedBy: .equal, toItem: parentView, attribute: .left, multiplier: 1, constant: 0))
        parentView.addConstraint(NSLayoutConstraint(item: childView, attribute: .right, relatedBy: .equal, toItem: parentView, attribute: .right, multiplier: 1, constant: 0))
        parentView.addConstraint(NSLayoutConstraint(item: childView, attribute: .top, relatedBy: .equal, toItem: parentView, attribute: .top, multiplier: 1, constant: 0))
        parentView.addConstraint(NSLayoutConstraint(item: childView, attribute: .bottom, relatedBy: .equal, toItem: parentView, attribute: .bottom, multiplier: 1, constant: 0))
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        return super.point(inside: point, with: event)
    }
    
    // Used for allowing gestures in the gesture view (which is outside of the bounds of the footer).
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        for subview in subviews {
            if let view = subview.hitTest(convert(point, to: subview), with: event) , view.isKind(of: UIButton.self) {
                return view
            }
        }
        return super.hitTest(point, with: event)
    }
    
    // #MARK: - Button Actions
    
    
    func leftButtonAction(_ sender: AnyObject) {
        guard footerState == .open else { return }
        leftButtonCallback?()
    }
    
    func centerButtonAction(_ sender: AnyObject) {
        if footerState == .closed {
            toggle()
        } else {
            centerButtonCallback?()
        }
    }
    
    func rightButtonAction(_ sender: AnyObject) {
        guard footerState == .open else { return }
        rightButtonCallback?()
    }
    
    func toggleButtonAction(_ sender: AnyObject) {
        toggle()
    }
    
    func toggle() {
        switch footerState {
        case .closed:
            footerState = .open
            heightLayoutConstraint.constant = FIEFooterView.EXPANDED_HEIGHT
            showButtonContents()
            
        case .open:
            footerState = .closed
            heightLayoutConstraint.constant = FIEFooterView.DEFAULT_HEIGHT
            hideButtonContents()
        }
        
        UIView.animate(withDuration: FIEFooterView.ANIMATION_DURATION, animations: {
            
            // superview is the menu footerView, and its superview is the menu's containerView
            // We do this so that the animation works.
            self.containerView?.layoutIfNeeded()
        })
    }
    
    
    func showButtonContents() {
        
        leftButton.setTitle("Pagar Servicios", for: UIControlState())
        centerButton.setTitle("Transferencias", for: UIControlState())
        
        let westernImage = UIImage(named: "views_wu")?.withRenderingMode(.alwaysOriginal)
        rightButton.setImage(westernImage, for: UIControlState())

        
        [leftButton, centerButton, rightButton].forEach { $0.alpha = 0 }
        arrowImageView.fie_rotateUp()
        UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions(), animations: {
            [self.leftButton, self.centerButton, self.rightButton].forEach { $0.alpha = 1 }
        }, completion: nil)
    }
    
    func hideButtonContents() {
        
        [leftButton, centerButton, rightButton].forEach { $0.alpha = 1 }
        arrowImageView.fie_rotateDown()
        UIView.animate(withDuration: 0.2, animations: {
            [self.leftButton, self.centerButton, self.rightButton].forEach { $0.alpha = 0.2 }
        }, completion: { _ in
            self.leftButton.setTitle(nil, for: UIControlState())
            self.centerButton.setTitle(nil, for: UIControlState())
            self.rightButton.setImage(nil, for: UIControlState())
        })
    }

}
