//
//  FIEDialogue.swift
//  Fielco-iOS
//
//  Created by Roshka on 8/18/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

class FIEDialogue {
    
    // Static needed otherwise dialogue doesn't stay in memory.
    fileprivate static var dialogue: FIEDialogue!
    fileprivate var window: UIWindow?
    fileprivate var bottomLayoutConstraint: NSLayoutConstraint!
    
    /// The dialog's background view (full screen).
    fileprivate var bkgView: UIView!
    
    /// The dialog's principal view (not full screen).
    var view: UIView!
    
    init(_ setup: ((FIEDialogue) -> Void)) {
        
        FIEDialogue.dialogue = self // This keeps this FIEDialogue in memory, without it, only the window is kept in memory cause it's marked static.
        
        NotificationCenter.default.addObserver(self, selector: #selector(inputViewWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(inputViewWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        window = UIWindow()
        window!.windowLevel = UIWindowLevelAlert
        window!.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        window!.makeKeyAndVisible()
        window!.isHidden = true
        
        bkgView = UIView()
        bkgView.backgroundColor = UIColor.clear
        bkgView.translatesAutoresizingMaskIntoConstraints = false
        window!.addSubview(bkgView)
        bkgView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(close)))
        
        view = UIView()
        view.backgroundColor = FIEConstants.Colors.Foreground
        view.translatesAutoresizingMaskIntoConstraints = false
        bkgView.addSubview(view)
        
        window!.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[bkgView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["bkgView": bkgView]))
        window!.addConstraint(NSLayoutConstraint(item: bkgView, attribute: .top, relatedBy: .equal, toItem: window!, attribute: .top, multiplier: 1, constant: 0))
        bottomLayoutConstraint = NSLayoutConstraint(item: window!, attribute: .bottom, relatedBy: .equal, toItem: bkgView, attribute: .bottom, multiplier: 1, constant: 0)
        window!.addConstraint(bottomLayoutConstraint)
        
        bkgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-(40)-[view]-(40)-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["view": view]))
        bkgView.addConstraint(NSLayoutConstraint(item: view, attribute: .centerY, relatedBy: .equal, toItem: bkgView, attribute: .centerY, multiplier: 1, constant: 0))
        
        setup(self)
    }

    
    func show() {
        window?.isHidden = false
        window?.alpha = 0
        UIView.animate(withDuration: 0.3, animations: {
            self.window?.alpha = 1
        })
    }
    
    @objc func close() {
        view.endEditing(true)
        UIView.animate(withDuration: 0.3, animations: {
            self.window?.alpha = 0
        }, completion: { _ in
            self.window?.isHidden = true
            self.window = nil
        })
    }
    
    @objc func inputViewWillShow(_ notification: Notification) {
        let info: NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let value: NSValue = info.value(forKey: UIKeyboardFrameBeginUserInfoKey) as! NSValue
        let keyboardRect = value.cgRectValue
        
        let animationDuration = info.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as! NSNumber
        let curve = (info[UIKeyboardAnimationCurveUserInfoKey] as! NSValue) as! UInt
        
        bottomLayoutConstraint.constant = keyboardRect.height
        
        UIView.animate(withDuration: animationDuration.doubleValue, delay: 0, options: UIViewAnimationOptions(rawValue: curve), animations: {
            self.window?.layoutIfNeeded()
        }, completion: nil)
    }
    
    @objc func inputViewWillHide(_ notification: Notification) {
        let info: NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        
        let animationDuration = info.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as! NSNumber
        let curve = (info[UIKeyboardAnimationCurveUserInfoKey] as! NSValue) as! UInt
        
        bottomLayoutConstraint.constant = 0
 
        UIView.animate(withDuration: animationDuration.doubleValue, delay: 0, options: UIViewAnimationOptions(rawValue: curve), animations: {
            self.window?.layoutIfNeeded()
        }, completion: nil)
    }
}
