//
//  FIEImageSliderView.swift
//  Fielco-iOS
//
//  Created by Roshka on 7/6/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

//import LTMorphingLabel

enum FIEImageSliderAction {
    case new
    case edit
    case none
}

@objc protocol FIEImageSliderDataSource {
    func segueIdentifier() -> String
    @objc optional func viewTypeToRemoveFromImage() -> [AnyClass]
    func snapshotViewForViewController(_ vc: UIViewController) -> UIView
}

@objc protocol FIEImageSliderDelegate {
    @objc optional func didTapImageAtIndex(_ index: Int)
    @objc optional func didLongPressImageAtIndex(_ index: Int)
}

class FIEImageSliderItem {
    var title: String?
    var imageView: UIImageView!
    var widthLayoutConstraint: NSLayoutConstraint!
}

class FIEImageSliderView: UIView, UIViewControllerTransitioningDelegate, UIScrollViewDelegate {
    var currentAction = FIEImageSliderAction.none
    var didCancel = false
    var viewController: UIViewController!
    var currentSnapshotView: UIView?
    var editOnLoadCallback: ((UIViewController, Int) -> Void)?
    var onSaveCallback: ((UIViewController, Int) -> Void)?
    var dataSource: FIEImageSliderDataSource!
    var delegate: FIEImageSliderDelegate?
    
    let margin: CGFloat = 30
    let padding: CGFloat = 10
    
    var titleLabel: UILabel!// LTMorphingLabel
    var scrollView: UIScrollView!
    
    var items = [FIEImageSliderItem]()
    
    fileprivate var needsAllConstraints = true
    fileprivate var needsNewItemConstraints = false
    fileprivate var needsDeletedItemConstraints = false
    var firstConstraint: NSLayoutConstraint?
    var lastConstraint: NSLayoutConstraint?
    
    let DURATION: TimeInterval = 0.5
    
    fileprivate let imageSliderPresentAnimationController = FIEImageSliderPresentAnimationController()
    fileprivate let imageSliderDismissAnimationController = FIEImageSliderDismissAnimationController()
    
    /// Returns the frame (in relation to the window) of the first image.
    var currentImageRect: CGRect? {
        let window = UIApplication.shared.delegate?.window
        if currentIndex < items.count {
            let item = items[currentIndex]
            let imageFrame = imageFrameForItem(item)
            return window??.convert(imageFrame, from: item.imageView)
        }
        return nil
    }
    
    var currentIndex: Int {
        return Int(scrollView.contentOffset.x / scrollView.frame.width)
    }
    
    init(viewController: UIViewController, dataSource: FIEImageSliderDataSource, delegate: FIEImageSliderDelegate? = nil, images: [UIImage]) {
        super.init(frame: CGRect.zero)
        
        self.viewController = viewController
        self.dataSource = dataSource
        self.delegate = delegate
        
        translatesAutoresizingMaskIntoConstraints = false
        
        titleLabel = UILabel.rsk_newAutolayoutInParentView(self)    // LTMorphingLabel
        titleLabel.font = FIEConstants.Fonts.SizeMediumWeightLight
        titleLabel.textColor = FIEConstants.Colors.Text
        titleLabel.textAlignment = .center
        
        scrollView = UIScrollView.rsk_newAutolayoutInParentView(self)
        scrollView.isPagingEnabled = true
        scrollView.clipsToBounds = false
        scrollView.delegate = self
        
        for image in images {
            insertImageAtCurrentIndex(image)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// Cancel
    func cancel() {
        didCancel = true
        self.viewController.dismiss(animated: true, completion: {
            if self.currentAction == .new {
                self.removeAtCurrentIndex()
            }
        })
    }
    
    func saveViewController(_ viewController: UIViewController, withTitle title: String?) {
        didCancel = false
        viewController.view.endEditing(true)
        self.onSaveCallback?(viewController, self.currentIndex)
        viewController.dismiss(animated: true, completion: {
            self.setTitleForCurrentIndex(title)
        })
    }
    
    /// Inserts an image at the current index (existing image at current index is pushed to the right).
    func insertAtCurrentIndex(_ onSaveCallback: @escaping ((UIViewController, Int) -> Void)) {
        self.onSaveCallback = onSaveCallback
        currentAction = .new
        viewController.performSegue(withIdentifier: dataSource.segueIdentifier(), sender: nil)
    }
    
    /// Removes the image at the current index (image at next index moves to current index).
    func removeAtCurrentIndex() {
        
        if currentIndex < items.count {
            let itemToDelete = items[currentIndex]
            items.remove(at: currentIndex)
            UIView.animate(withDuration: DURATION, animations: {
                itemToDelete.widthLayoutConstraint.constant = 0
                self.layoutIfNeeded()
            }, completion: { completed in
                if completed {
                    self.needsDeletedItemConstraints = true
                    self.setNeedsUpdateConstraints()
                    self.updateConstraintsIfNeeded()
                    itemToDelete.imageView.removeFromSuperview()
                }
            })
        }
    }
    
    /// Edit the image at the current index (image at next index moves to current index).
    func editAtCurrentIndex(_ editOnLoadCallback: @escaping ((UIViewController, Int) -> Void), onSaveCallback: @escaping ((UIViewController, Int) -> Void)) {
        self.editOnLoadCallback = editOnLoadCallback
        self.onSaveCallback = onSaveCallback
        currentAction = .edit
        viewController.performSegue(withIdentifier: dataSource.segueIdentifier(), sender: nil)
    }
    

    fileprivate func itemWithImage(_ image: UIImage) -> FIEImageSliderItem {
        let imageView = UIImageView.rsk_newAutolayoutInParentView(scrollView)
        imageView.isUserInteractionEnabled = true
        imageView.image = image
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(buttonTapAction)))
        imageView.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(buttonLongPressAction)))
        imageView.backgroundColor = FIEConstants.Colors.Background
        imageView.layer.cornerRadius = 20
        imageView.layer.masksToBounds = true
        imageView.setContentHuggingPriority(1, for: .vertical)
        imageView.contentMode = .scaleAspectFit
        
        let item = FIEImageSliderItem()
        item.imageView = imageView
        item.widthLayoutConstraint = NSLayoutConstraint(item: imageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
        return item
    }
    
    @objc fileprivate func buttonTapAction(_ tapGestureRecognizer: UITapGestureRecognizer) {
        delegate?.didTapImageAtIndex?(currentIndex)
    }
    
    @objc fileprivate func buttonLongPressAction(_ logPressGestureRecognizer: UITapGestureRecognizer) {
        delegate?.didLongPressImageAtIndex?(currentIndex)
    }
    
    fileprivate func constrainButton(_ button: UIImageView) {
        self.addConstraint(NSLayoutConstraint(item: button, attribute: .top, relatedBy: .equal, toItem: titleLabel, attribute: .bottom, multiplier: 1, constant: 16))
        self.addConstraint(NSLayoutConstraint(item: button, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 0, constant: 250))
        self.addConstraint(NSLayoutConstraint(item: button, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: -16))
    }
    
    func setTitleForCurrentIndex(_ title: String?) {
        guard items.count > 0 else { return }
        items[currentIndex].title = title
        titleLabel.text = title
    }
    
    func insertImageAtCurrentIndex(_ image: UIImage, callback: ((Void) -> Void)? = nil) {
        scrollView.setContentOffset(CGPoint.zero, animated: true)
        needsNewItemConstraints = true
        let item = itemWithImage(image)
        items.insert(item, at: 0)
        setNeedsUpdateConstraints()
        setNeedsLayout()
        layoutIfNeeded()
        let newWidth = self.bounds.width - margin * 2
        item.widthLayoutConstraint.constant = newWidth
        UIView.animate(withDuration: DURATION, animations: {
            self.layoutIfNeeded()
        }, completion: { _ in
            self.setNeedsLayout()
            self.layoutIfNeeded()
            
            callback?()
        })
    }
    
    func imageFrameForItem(_ item: FIEImageSliderItem) -> CGRect {
        let imageFrame = item.imageView.frameForImageInImageViewAspectFit()
//        let insetImageFrame = imageFrame.offsetBy(dx: self.padding, dy: self.padding)
        return imageFrame
    }
    
    // #MARK: - UIViewControllerTransitioningDelegate
    
    override func updateConstraints() {
        
        if needsAllConstraints {
            titleLabel.rsk_clipTopToTopOfParentView()
            titleLabel.rsk_clipHorizontallyToParentView()
            
            scrollView.rsk_clipTopToBottomOfView(titleLabel)
            scrollView.rsk_clipHorizontallyToParentView(margin)
            scrollView.rsk_clipBottomToBottomOfParentView()
            
            needsAllConstraints = false
        }
        
        if needsNewItemConstraints {
            
            if let firstConstraint = firstConstraint {
                scrollView.removeConstraint(firstConstraint)
            }
        
            let rightView = items.count > 1 ? items[1].imageView : scrollView
            let rightViewLayoutAttribute = items.count > 1 ? NSLayoutAttribute.left : NSLayoutAttribute.right
            let newItem = items.first!
            let newView = newItem.imageView
            constrainButton(newView!)
            self.addConstraint(newItem.widthLayoutConstraint)
            let rightConstraint = NSLayoutConstraint(item: newView, attribute: .right, relatedBy: .equal, toItem: rightView, attribute: rightViewLayoutAttribute, multiplier: 1, constant: 0)
            scrollView.addConstraint(rightConstraint)
            if currentIndex == items.count - 1 {
                lastConstraint = rightConstraint
            }
            
            firstConstraint = newView?.rsk_clipLeftToLeftOfParentView(0)
            
            needsNewItemConstraints = false
        }
        else if needsDeletedItemConstraints {
            
            for imageView in scrollView.subviews as! [UIImageView] {
                for constraint in imageView.constraints {
                    if constraint.firstItem as? UIImageView == imageView && constraint.firstAttribute == .width && constraint.constant == 0 {
                        var rightView: UIView!
                        scrollView.constraints.forEach { constraint in
                            if constraint.firstItem as? UIImageView == imageView {
                                rightView = constraint.secondItem as! UIImageView
                            }
                        }
                        var leftView: UIView!
                        scrollView.constraints.forEach { constraint in
                            if constraint.secondItem as? UIImageView == imageView {
                                leftView = constraint.firstItem as! UIImageView
                            }
                        }
                        let joinConstraint = NSLayoutConstraint(item: leftView, attribute: .right, relatedBy: .equal, toItem: rightView, attribute: .left, multiplier: 1, constant: 0)
                        scrollView.addConstraint(joinConstraint)
                    }
                }
            }
            
            needsDeletedItemConstraints = false
        }
        
        super.updateConstraints()
    }
    
    /// Custom animation on Present
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        imageSliderPresentAnimationController.imageSliderView = self
        if currentAction == .new {
            imageSliderPresentAnimationController.callback = { _, snapshotView in
                self.insertImageAtCurrentIndex(UIImage.rsk_imageWithView(snapshotView)) {
                    self.imageSliderPresentAnimationController.animateOpen()
                }
            }
        } else if currentAction == .edit {
            imageSliderPresentAnimationController.callback = { toViewController, snapshotView in
                self.editOnLoadCallback?(toViewController, self.currentIndex)
                self.imageSliderPresentAnimationController.animateOpen()
            }
        }
        return imageSliderPresentAnimationController
    }
    
    
    /// Custom animation on Dismiss
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        imageSliderDismissAnimationController.imageSliderView = self
        imageSliderDismissAnimationController.targetFrame = self.currentImageRect!
        imageSliderDismissAnimationController.callback = { _, snapshotView in
            if self.currentIndex < self.items.count {
                let item = self.items[self.currentIndex]
                item.imageView.image = UIImage.rsk_imageWithView(snapshotView)
            }
        }
        return imageSliderDismissAnimationController
    }
    
    // #MARK: - UIScrollViewDelegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        titleLabel.text = items[currentIndex].title
    }
}


class FIEImageSliderAnimationController: NSObject {
    var imageSliderView: FIEImageSliderView!
    var transitionContext: UIViewControllerContextTransitioning!
    var callback: ((_: UIViewController, _ snapshotView: UIView) -> Void)!
    
    @objc(transitionDuration:) func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.4
    }
    
    func snapshotViewForViewController(_ viewController: UIViewController) -> UIView {
        let vc: UIViewController!
        if let navController = viewController as? UINavigationController, let rootViewController = navController.viewControllers.first {
            rootViewController.navigationController?.setNavigationBarHidden(true, animated: false)
            vc = rootViewController
        } else {
            vc = viewController
        }
        vc.edgesForExtendedLayout = UIRectEdge()
        vc.view.endEditing(true)
        
        // Remove any unwanted views from the screen for the snapshot, then reshow them.
        var hiddenViews = [UIView]()
        if let typesToRemove = imageSliderView.dataSource.viewTypeToRemoveFromImage?() {
            func dismissView(_ aView: UIView) {
                aView.subviews.forEach { subView in
                    let isUnwantedView = typesToRemove.filter { subView.isKind(of: $0) }.count > 0
                    if isUnwantedView {
                        subView.isHidden = true
                    }
                    dismissView(subView)
                }
            }
            dismissView(vc.view)
        }
        let snapshotView = vc.view.snapshotView(afterScreenUpdates: true)
        hiddenViews.forEach { $0.isHidden = false }
   
//        let view = UIView(frame: viewController.view.frame)
//        view.backgroundColor = UIColor.whiteColor()
//        return view
//        return snapshotView
        
        let snap = imageSliderView.dataSource.snapshotViewForViewController(viewController)
//        snap.transform = CGAffineTransformMakeScale(0.5, 0.5)
        snap.frame = viewController.view.frame
        return snap
    }
    
    func navControllerForViewController(_ viewController: UIViewController) -> UINavigationController? {
        if let navController = viewController as? UINavigationController {
            return navController
        } else {
            return viewController.navigationController
        }
    }
    
}

class FIEImageSliderPresentAnimationController: FIEImageSliderAnimationController, UIViewControllerAnimatedTransitioning {
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        self.transitionContext = transitionContext
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        let snapshotView = snapshotViewForViewController(toViewController)
        snapshotView.frame = UIScreen.main.bounds
        //callback(toViewController, snapshotView: snapshotView)
    }
    
    func animateOpen() {
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        let containerView = transitionContext.containerView
        let finalFrame = transitionContext.finalFrame(for: toViewController)
        
        // Make a snapshot of the view of the presented view controller.
        let snapshotView = snapshotViewForViewController(toViewController)
        self.imageSliderView.currentSnapshotView = snapshotView
        let snapshotImage = UIImage.rsk_imageWithView(snapshotView)
        let snapshotImageView = UIImageView(frame: imageSliderView.currentImageRect!)
        snapshotImageView.contentMode = .scaleAspectFit
        snapshotImageView.image = snapshotImage
//        snapshotView.frame = imageSliderView.currentImageRect!
        
        containerView.addSubview(toViewController.view)
        containerView.addSubview(snapshotImageView)
        toViewController.view.isHidden = true
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
//            snapshotView.frame = finalFrame
            snapshotImageView.frame = finalFrame
        }, completion: { _ in
            toViewController.view.isHidden = false
//            snapshotView.removeFromSuperview()
            
            snapshotImageView.removeFromSuperview()
            self.transitionContext.completeTransition(true)
            self.navControllerForViewController(toViewController)?.setNavigationBarHidden(false, animated: true)
        })
    }
}

class FIEImageSliderDismissAnimationController: FIEImageSliderAnimationController, UIViewControllerAnimatedTransitioning {
    
    var targetFrame: CGRect!
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        let containerView = transitionContext.containerView
        
        let dismissAnimation = {
            
            let snapshotView: UIView!
            if self.imageSliderView.didCancel {
                snapshotView = self.imageSliderView.currentSnapshotView
            } else {
                snapshotView = self.snapshotViewForViewController(fromViewController)
            }
            
            containerView.addSubview(toViewController.view)
            
            let snapshotImage = UIImage.rsk_imageWithView(snapshotView)
            let snapshotImageView = UIImageView(frame: fromViewController.view.bounds)
            snapshotImageView.contentMode = .scaleAspectFit
            snapshotImageView.image = snapshotImage
            containerView.addSubview(snapshotImageView)
//            containerView?.addSubview(snapshotView)
            fromViewController.view.isHidden = true
            
            UIView.animate(withDuration: self.transitionDuration(using: transitionContext), animations: {
//                snapshotView.frame = self.targetFrame
                snapshotImageView.frame = self.targetFrame
                snapshotImageView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            }, completion: { _ in
                //self.callback(toViewController, snapshotView: snapshotView)
                fromViewController.view.isHidden = false
                snapshotImageView.removeFromSuperview()
//                snapshotView.removeFromSuperview()
                transitionContext.completeTransition(true)
            })
        }
        
        if let navController = navControllerForViewController(fromViewController) {
            navController.setNavigationBarHidden(true, animated: true)
            rsk_delay(TimeInterval(UINavigationControllerHideShowBarDuration)) {
                dismissAnimation()
            }
        } else {
            dismissAnimation()
        }
    }
}

extension UIImageView {
    func frameForImageInImageViewAspectFit() -> CGRect
    {
        if  let img = self.image {
            let imageRatio = img.size.width / img.size.height;
            
            let viewRatio = self.frame.size.width / self.frame.size.height;
            
            if(imageRatio < viewRatio)
            {
                let scale = self.frame.size.height / img.size.height;
                
                let width = scale * img.size.width;
                
                let topLeftX = (self.frame.size.width - width) * 0.5;
                
                return CGRect(x: topLeftX, y: 0, width: width, height: self.frame.size.height);
            }
            else
            {
                let scale = self.frame.size.width / img.size.width;
                
                let height = scale * img.size.height;
                
                let topLeftY = (self.frame.size.height - height) * 0.5;
                
                return CGRect(x: 0, y: topLeftY, width: self.frame.size.width, height: height);
            }
        }
        
        return CGRect(x: 0, y: 0, width: 0, height: 0)
    }
}
