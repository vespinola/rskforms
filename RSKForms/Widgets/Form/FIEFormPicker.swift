//
//  FIEFormPicker.swift
//  Fielco-iOS
//
//  Created by Paul Von Schrottky on 6/14/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

class FIEFormPickerRightView: UIView {
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        print("Passing all touches to the next view (if any), in the view stack.")
        return false
    }
}

typealias FIEFormPickerItem = (title: String, value: AnyObject)
typealias FIEFormPickerCallback = ((FIEFormPickerItem) -> Void)

class FIEFormPicker: FIETextField, UIPickerViewDataSource, UIPickerViewDelegate, FIEFormValidatableProtocol {
    
    var pickerView: UIPickerView!
    var contents = [FIEFormPickerItem]() {
        didSet {
            pickerView.reloadAllComponents()
            handleDefaultValue()
        }
    }
    var defaultValue: String?
    var defaultValueIndex: Int?
    
    fileprivate var hasCreatedConstraints = false
    
    var _onChange: FIEFormPickerCallback?
    
    var validations: [FIEValidation]!
    var validationMessage: String? // Deprecated
    
    init(placeholder: String, defaultValue: String? = nil, defaultValueIndex: Int? = nil, validation: FIEValidation..., onChange: FIEFormPickerCallback? = nil) {
        super.init(frame: CGRect.zero)
        
        // If no validation is passed in, an empty array is implicitly passed.
        self.validations = validation
        
        setup()
        self.defaultValue = defaultValue
        self.defaultValueIndex = defaultValueIndex
        self.placeholder = placeholder
        _onChange = onChange
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func updateConstraints() {
        if hasCreatedConstraints == false {
            hasCreatedConstraints = true
            
            addConstraint(NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 0, constant: 30))
        }
        super.updateConstraints()
    }
    
    override func setup() {
        super.setup()
        
        backgroundColor = FIEConstants.Colors.Background
        textColor = FIEConstants.Colors.Text
        font = FIEConstants.Fonts.SizeMediumWeightLight
        
        autocorrectionType = .no
        autocapitalizationType = .none
        
        // Replace this field's keyboard with the picker.
        pickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
        inputView = self.pickerView
        
        let rightView = FIEFormPickerRightView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        rightView.isUserInteractionEnabled = false
        let imageView = UIImageView(image: UIImage(named: "chevron-gray"))
        rightView.addSubview(imageView)
        imageView.frame = CGRect(x: 5, y: 5, width: 15, height: 15)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        self.rightView = rightView
        
        
        onCloseKeyboard = {
            let selectedRow = self.pickerView.selectedRow(inComponent: 0)
            let text = self.pickerView.delegate?.pickerView!(self.pickerView, titleForRow: selectedRow, forComponent: 0)
            self.text = text
            guard selectedRow < self.contents.count else { return }
            self._onChange?(self.contents[selectedRow])
        }
        
        fie_addBorder()
        
        handleDefaultValue()
    }
    
    func handleDefaultValue() {
        if let defaultValueIndex = defaultValueIndex {
            guard defaultValueIndex < contents.count - 1 else { return }
            text = contents[defaultValueIndex].title
            pickerView.selectRow(defaultValueIndex, inComponent: 0, animated: false)
            _onChange?(contents[defaultValueIndex])
        } else if let defaultValue = defaultValue {
            text = defaultValue
        } else {
            text = nil
        }
    }
    
    func selectedIndex() -> Int {
        return self.pickerView.selectedRow(inComponent: 0)
    }
    
    // MARK: Overrides
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: 0, width: bounds.width - 20, height: bounds.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: 0, width: bounds.width - 20, height: bounds.height)
    }
    
    // Overriding this hides the cursor.
    override func caretRect(for position: UITextPosition) -> CGRect {
        return CGRect.zero
    }
    
    // MARK: UIPickerViewDataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data.
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.contents.count
    }
    
    // The title to return for the row and component (column) that's being passed in.
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        guard contents.count > 0 else { return nil }
        return contents[row].0
    }
    
    // MARK: UIPickerViewDelegate
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // This method is triggered whenever the user makes a change to the picker selection.
        // The parameter named row and component represents what was selected.
        guard contents.count > 0 else { return }
        _onChange?(contents[row])
    }
}
