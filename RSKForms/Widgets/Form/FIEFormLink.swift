//
//  FIEFormLink.swift
//  Fielco-iOS
//
//  Created by Paul Von Schrottky on 6/14/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

class FIEFormLink: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    var _onTap: ((Void) -> Void)?
    
    convenience init(_ text: String, onTap: @escaping ((Void) -> Void)) {
        self.init(frame: CGRect.zero)
        
        _onTap = onTap
        
        titleLabel?.lineBreakMode = .byWordWrapping
        titleLabel?.textAlignment = .center
        titleLabel?.font = FIEConstants.Fonts.SizeSmallWeightLight
        titleLabel?.textColor = FIEConstants.Colors.Text
        
        let attributedTitle = NSAttributedString(string: text, attributes: [
            NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue
        ])
        
        setAttributedTitle(attributedTitle, for: UIControlState())
        
        addTarget(self, action: #selector(click), for: .touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func click(_ button: UIButton) {
        _onTap?()
    }
}
