//
//  FIEFormInput.swift
//  Fielco-iOS
//
//  Created by Paul Von Schrottky on 6/14/16.
//  Copyright © 2016 Roshka. All rights reserved.
// 

import UIKit


enum FIEValidationType {
    case isEmpty
    case regex(String)
    case email
}

/// Live(String): Check validation as user edits input.
///     - `String` is the validation regex.
/// Submit(FIEValidationType, String): Check validation when form is validated.
///     - `FIEValidationType`: The type of validation to perform.
///     - `String` is the validation regex.
enum FIEValidation {
    case live(String)
    case submit(FIEValidationType, String)
}


class FIEFormInput: FIETextField, FIEFormValidatableProtocol {
    
    var _onChange: ((String?) -> Void)?
    fileprivate var hasCreatedConstraints = false
    var validationMessage: String?
    var validations: [FIEValidation]!
    
    init(
        placeholder: String? = nil,
        validationMessage: String? = nil,
        defaultValue: String? = nil,
        keyboardType: UIKeyboardType? = .default,
        autocorrectionType: UITextAutocorrectionType = .no,
        autocapitalizationType: UITextAutocapitalizationType = .none,
        addDoneButton: Bool? = true,
        returnKeyType: UIReturnKeyType? = .next,
        validation: FIEValidation...,
        onChange: ((String?) -> Void)? = nil
    ) {
        super.init(frame: CGRect.zero)
        
        self.delegate = self
        
        self.validationMessage = validationMessage
        
        // If no validation is passed in, an empty array is implicitly passed.
        self.validations = validation
        
        // Handle the default value.
        if let defaultValue = defaultValue {
            text = defaultValue
            onChange?(defaultValue)
        }
        
        
        _onChange = onChange
        self.clearButtonMode = .whileEditing
        
        contentVerticalAlignment = .center
        
        self.placeholder = placeholder
        
        if let keyboardType = keyboardType {
            self.keyboardType = keyboardType
        }
        
        self.autocorrectionType = autocorrectionType
        self.autocapitalizationType = autocapitalizationType
        
        if addDoneButton == true {
            fie_addDoneButton()
        }
        
        if let returnKeyType = returnKeyType {
            self.returnKeyType = returnKeyType
        }
        
        setup()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func setup() {
        super.setup()
        
        backgroundColor = FIEConstants.Colors.Background
        textColor = FIEConstants.Colors.Text
        font = FIEConstants.Fonts.SizeMediumWeightLight
        
        fie_addBorder()
        
        NotificationCenter.default.addObserver(self, selector: #selector(textFieldDidChangeHandler), name: NSNotification.Name.UITextFieldTextDidChange, object: nil)
    }
    
    func textFieldDidChangeHandler(_ notification: Notification) {
        _onChange?(text)
    }
    
    override func updateConstraints() {
        if hasCreatedConstraints == false {
            hasCreatedConstraints = true
            
            addConstraint(NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 0, constant: 30))
        }
        super.updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // #MARK - Others
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 10, dy: 0)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 10, dy: 0)
    }

}

extension FIEFormInput: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        for validation in validations {
            if case .live(let pattern) = validation {
                let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                let regex = try! NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options(rawValue: 0))
                return regex.firstMatch(in: newText, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, newText.characters.count)) != nil
            }
        }
        return true
    }
}
