//
//  FIEFormLabel.swift
//  Fielco-iOS
//
//  Created by Paul Von Schrottky on 6/14/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

class FIEFormLabel: UILabel {

    init(text: String, textAlignment: NSTextAlignment = .left) {
        super.init(frame: CGRect.zero)
        self.textAlignment = textAlignment
        
        translatesAutoresizingMaskIntoConstraints = false
        self.text = text
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        textColor = FIEConstants.Colors.Blue
        font = FIEConstants.Fonts.SizeMediumWeightMedium
    }

}
