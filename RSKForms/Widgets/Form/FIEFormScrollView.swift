//
//  FIEFormScrollView.swift
//  Fielco-iOS
//
//  Created by Roshka on 6/20/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

protocol FIEFormValidatableProtocol {
    var text: String? { get set }
    var validations: [FIEValidation]! { get set }
    var validationMessage: String? { get set }
}

public struct Directions : OptionSet{
    public let rawValue : Int
    public init(rawValue:Int){ self.rawValue = rawValue}
    
    static let Up  = Directions(rawValue:1)
    static let Down  = Directions(rawValue:2)
    static let Left = Directions(rawValue:4)
    static let Right = Directions(rawValue:8)
}


public struct FIEFormSpace: OptionSet {
    public let rawValue : Int
    public init(rawValue:Int){ self.rawValue = rawValue}
    let space: CGFloat = 16
    static let None         	=       FIEFormSpace(rawValue: 0)
    static let Top              =       FIEFormSpace(rawValue: 1 << 1)
    static let TopDbl           =       FIEFormSpace(rawValue: 1 << 2)
    static let TopTri           =       FIEFormSpace(rawValue: 1 << 3)
    static let Right            =       FIEFormSpace(rawValue: 1 << 4)
    static let Bottom           =       FIEFormSpace(rawValue: 1 << 5)
    static let Left             =       FIEFormSpace(rawValue: 1 << 6)
    static let BottomDbl        =       FIEFormSpace(rawValue: 1 << 7)
    static let BottomTri        =       FIEFormSpace(rawValue: 1 << 8)
    static let StdWidth         =       FIEFormSpace.Left.union(.Right)
    static let Default          =       FIEFormSpace.Top.union(.StdWidth)
    static let Last             =       FIEFormSpace.Top.union(.BottomTri).union(.StdWidth)
    static let LastFullWidth    =       FIEFormSpace.Top.union(.BottomTri)
    
    var top: CGFloat {
        if contains(.TopTri) {
            return space * 3
        } else if contains(.TopDbl) {
            return space * 2
        } else if contains(.Top) {
            return space
        } else {
            return 0
        }
    }
    
    var right: CGFloat {
        return contains(.Right) ? space : 0
    }
    
    var bottom: CGFloat {
        if contains(.BottomTri) {
            return space * 3
        } else if contains(.BottomDbl) {
            return space * 2
        } else if contains(.Bottom) {
            return space
        } else {
            return 0
        }
    }
    
    var left: CGFloat {
        return contains(.Left) ? space : 0
    }
}

typealias FIEFormItem = (view: UIView, space: FIEFormSpace)

class FIEFormScrollView: UIScrollView {
    
    fileprivate var needsAllConstraints = true
    fileprivate var needsNewItemConstraints = false
    fileprivate var newItems = [FIEFormItem]()
    fileprivate var bottomConstraint: NSLayoutConstraint?
    fileprivate let keyboardHandler = RSKKeyboardHandler()
    
    var focusableFields = [UIResponder]()
    var items = [FIEFormItem]()
    var viewController: UIViewController?
    
    
    var isValid: Bool {
        
        var allItems = [FIEFormItem]()
        allItems.append(contentsOf: items)
        allItems.append(contentsOf: newItems)
        let formInputs = allItems.filter {
            $0.view is FIEFormValidatableProtocol
        }.map {
            $0.view as! FIEFormValidatableProtocol
        }
        
        for formInput in formInputs {
            
            // Is an invalid field (using legacy validation).
            if formInput.text?.rsk_containsValue != true, let validationMessage = formInput.validationMessage {
                FIEUtility.showAlertInViewController(viewController, message: validationMessage)
                return false  // Exit, form invalid.
            }
            
            for validation in formInput.validations {
                if case .submit(let validationType) = validation {
                    
                    // Stays nil if no fields are invalid, is set to a message String if a field is invalid.
                    var failedValidationMessage: String?
                    
                    if let text = formInput.text {
                        switch validationType {
                        case (.isEmpty, let msg):
                            
                            if text.rsk_containsValue == false {
                                failedValidationMessage = msg
                            }
                            
                        case (.regex(let pattern), let msg):
                            
                            let regex = try! NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options(rawValue: 0))
                            let failedRegexTest = regex.firstMatch(in: text, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, text.characters.count)) == nil
                            if failedRegexTest {
                                failedValidationMessage = msg
                            }
                            
                        case (.email, let msg):
                            
                            // Email regex taken from here: http://stackoverflow.com/a/30845969/1305067
                            let regex = try! NSRegularExpression(pattern: "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$", options: NSRegularExpression.Options(rawValue: 0))
                            let failedRegexTest = regex.firstMatch(in: text, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, text.characters.count)) == nil
                            if failedRegexTest {
                                failedValidationMessage = msg
                            }
                        }
                    }
                    
                    // If failedValidationMessage is nil, a field was found that was invalid, show the message and exit the for-loop.
                    if let msg = failedValidationMessage {
                        FIEUtility.showAlertInViewController(viewController, message: msg)
                        return false // Exit, form invalid.
                    }
                }
            }
        }
        
        return true // By default, form is valid.
    }
    
    convenience init(viewController: UIViewController) {
        self.init(frame: CGRect.zero)
        self.viewController = viewController
        viewController.view.addSubview(self)
        translatesAutoresizingMaskIntoConstraints = false
        rsk_fillParentView(0)
        setup()
    }
    
    convenience init(_ parentView: UIView) {
        self.init(frame: CGRect.zero)
        parentView.addSubview(self)
        translatesAutoresizingMaskIntoConstraints = false
        rsk_fillParentView(0)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.delaysContentTouches = true
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.delaysContentTouches = true
        setup()
    }
    
    func setup() {
        keyboardHandler.scrollView = self
        canCancelContentTouches = true
    }
    
    // Allow touches on the scrollview to close the keyboard.
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        return self.next!.touchesBegan(touches, with: event)
    }
    
    func add(_ space: FIEFormSpace = .Default, add: ((Void) -> UIView?)) -> FIEFormScrollView {
        guard let subview = add() else { return self }
        subview.translatesAutoresizingMaskIntoConstraints = false
        
        let item = (subview, space)
        
        if needsAllConstraints == true {
            items.append(item)
        } else {
            newItems.append(item)
            needsNewItemConstraints = true
        }
        self.addSubview(subview)
        
        if let textField = subview as? UITextField {
            focusableFields.append(textField)
            textField.addTarget(self, action: #selector(editingDidEndOnExit), for: UIControlEvents.editingDidEndOnExit)
        } else if let textView = subview as? FIETextView {
            focusableFields.append(textView)
        }
        
        setNeedsUpdateConstraints()
        updateConstraintsIfNeeded()
    
        return self
    }
    
    override func updateConstraints() {
        
        if needsAllConstraints == true {
            
            for (idx, item) in items.enumerated() {
        
                if idx == 0 {
                    item.view.rsk_clipTopToTopOfParentView(item.space.top)
                } else {
                    item.view.rsk_clipTopToBottomOfView(items[idx - 1].view, margin: item.space.top)
                }
                
                item.view.rsk_clipLeftToLeftOfView(superview!, margin: item.space.left)
                item.view.rsk_clipRightToRightOfView(superview!, margin: item.space.right)
                if idx == items.count - 1 {
                    bottomConstraint = item.view.rsk_clipBottomToBottomOfParentView(-item.space.bottom)
                }
            }
            
            needsAllConstraints = false
        }
        
        if needsNewItemConstraints == true, let lastItem = items.last {
            if let bottomConstraint = bottomConstraint {
                removeConstraint(bottomConstraint)
            }
            
            for (idx, item) in newItems.enumerated() {
                if idx == 0 {
                    let lastView = lastItem.view
                    item.view.rsk_clipTopToBottomOfView(lastView, margin: item.space.top)
                } else {
                    item.view.rsk_clipTopToBottomOfView(newItems[idx - 1].view, margin: item.space.top)
                }
                item.view.rsk_clipLeftToLeftOfView(superview!, margin: item.space.left)
                item.view.rsk_clipRightToRightOfView(superview!, margin: item.space.right)
                if idx == newItems.count - 1 {
                    bottomConstraint = item.view.rsk_clipBottomToBottomOfParentView(-item.space.bottom)
                }
            }
    
        }
        
        super.updateConstraints()
    }
    
    // #MARK - UIControlEvents
    
    func editingDidEndOnExit(_ responder: UIResponder) {
        let nextIndex = focusableFields.index(of: responder)! + 1
        
        focusableFields[nextIndex % focusableFields.count].becomeFirstResponder()
        print(responder)
    }
    
    // #MARK: - Others
    
    func clearForm() {
        
        var allItems = [FIEFormItem]()
        allItems.append(contentsOf: items)
        allItems.append(contentsOf: newItems)
        
        for item in allItems where item.view is FIEFormValidatableProtocol {
            var formInput = item.view as! FIEFormValidatableProtocol
            formInput.text = nil
        }
    }
}


class FIEFormView: UIView {
    
    fileprivate var needsAllConstraints = true
    fileprivate var needsNewItemConstraints = false
    fileprivate var newItems = [FIEFormItem]()
    fileprivate var bottomConstraint: NSLayoutConstraint?
    fileprivate let keyboardHandler = RSKKeyboardHandler()
    
    var focusableFields = [UIResponder]()
    var items = [FIEFormItem]()
    var viewController: UIViewController?
    
    // **** Must be same as FIEFormScrollView isValid ****
    var isValid: Bool {
        
        var allItems = [FIEFormItem]()
        allItems.append(contentsOf: items)
        allItems.append(contentsOf: newItems)
        let formInputs = allItems.filter {
            $0.view is FIEFormValidatableProtocol
            }.map {
                $0.view as! FIEFormValidatableProtocol
        }
        
        for formInput in formInputs {
            
            // Is an invalid field (using legacy validation).
            if formInput.text?.rsk_containsValue != true, let validationMessage = formInput.validationMessage {
                FIEUtility.showAlertInViewController(viewController, message: validationMessage)
                return false  // Exit, form invalid.
            }
            
            for validation in formInput.validations {
                if case .submit(let validationType) = validation {
                    
                    // Stays nil if no fields are invalid, is set to a message String if a field is invalid.
                    var failedValidationMessage: String?
                    
                    if let text = formInput.text {
                        switch validationType {
                        case (.isEmpty, let msg):
                            
                            if text.rsk_containsValue == false {
                                failedValidationMessage = msg
                            }
                            
                        case (.regex(let pattern), let msg):
                            
                            let regex = try! NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options(rawValue: 0))
                            let failedRegexTest = regex.firstMatch(in: text, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, text.characters.count)) == nil
                            if failedRegexTest {
                                failedValidationMessage = msg
                            }
                            
                        case (.email, let msg):
                            
                            // Email regex taken from here: http://stackoverflow.com/a/30845969/1305067
                            let regex = try! NSRegularExpression(pattern: "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$", options: NSRegularExpression.Options(rawValue: 0))
                            let failedRegexTest = regex.firstMatch(in: text, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, text.characters.count)) == nil
                            if failedRegexTest {
                                failedValidationMessage = msg
                            }
                        }
                    }
                    
                    // If failedValidationMessage is nil, a field was found that was invalid, show the message and exit the for-loop.
                    if let msg = failedValidationMessage {
                        FIEUtility.showAlertInViewController(viewController, message: msg)
                        return false // Exit, form invalid.
                    }
                }
            }
        }
        
        return true // By default, form is valid.
    }

    
    convenience init(_ parentView: UIView) {
        self.init(frame: CGRect.zero)
        parentView.addSubview(self)
        translatesAutoresizingMaskIntoConstraints = false
        rsk_fillParentView(0)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        
    }
    
    func add(_ space: FIEFormSpace = .Default, add: ((Void) -> UIView?)) -> FIEFormView {
        guard let subview = add() else { return self }
        subview.translatesAutoresizingMaskIntoConstraints = false
        
        let item = (subview, space)
        
        if needsAllConstraints == true {
            items.append(item)
        } else {
            newItems.append(item)
            needsNewItemConstraints = true
        }
        self.addSubview(subview)
        
        if let textField = subview as? UITextField {
            focusableFields.append(textField)
            textField.addTarget(self, action: #selector(editingDidEndOnExit), for: UIControlEvents.editingDidEndOnExit)
        } else if let textView = subview as? FIETextView {
            focusableFields.append(textView)
        }
        
        return self
    }
    
    override func updateConstraints() {
        
        if needsAllConstraints == true {
            
            for (idx, item) in items.enumerated() {
                
                if idx == 0 {
                    item.view.rsk_clipTopToTopOfParentView(item.space.top)
                } else {
                    item.view.rsk_clipTopToBottomOfView(items[idx - 1].view, margin: item.space.top)
                }
                
                item.view.rsk_clipLeftToLeftOfView(superview!, margin: item.space.left)
                item.view.rsk_clipRightToRightOfView(superview!, margin: item.space.right)
                if idx == items.count - 1 {
                    bottomConstraint = item.view.rsk_clipBottomToBottomOfParentView(-item.space.bottom)
                }
            }
            
            needsAllConstraints = false
        }
        
        if needsNewItemConstraints == true, let lastItem = items.last {
            if let bottomConstraint = bottomConstraint {
                removeConstraint(bottomConstraint)
            }
            
            for (idx, item) in newItems.enumerated() {
                if idx == 0 {
                    let lastView = lastItem.view
                    item.view.rsk_clipTopToBottomOfView(lastView, margin: item.space.top)
                } else {
                    item.view.rsk_clipTopToBottomOfView(newItems[idx - 1].view, margin: item.space.top)
                }
                item.view.rsk_clipLeftToLeftOfView(superview!, margin: item.space.left)
                item.view.rsk_clipRightToRightOfView(superview!, margin: item.space.right)
                if idx == newItems.count - 1 {
                    bottomConstraint = item.view.rsk_clipBottomToBottomOfParentView(-item.space.bottom)
                }
            }
            
        }
        
        super.updateConstraints()
    }
    
    // #MARK - UIControlEvents
    
    func editingDidEndOnExit(_ responder: UIResponder) {
        let nextIndex = focusableFields.index(of: responder)! + 1
        
        focusableFields[nextIndex % focusableFields.count].becomeFirstResponder()
        print(responder)
    }
    
    // #MARK: - Others
    
    // **** Must be same as FIEFormScrollView clearForm ****
    func clearForm() {
        
        var allItems = [FIEFormItem]()
        allItems.append(contentsOf: items)
        allItems.append(contentsOf: newItems)
        
        for item in allItems where item.view is FIEFormValidatableProtocol {
            var formInput = item.view as! FIEFormValidatableProtocol
            formInput.text = nil
        }
    }
}

