//
//  FIEFormButtonView.swift
//  Fielco-iOS
//
//  Created by Roshka on 8/18/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

class FIEFormButtonView: UIView {

    fileprivate var hasCreatedConstraints = false
    let button = UIButton()
    var onTapCallback: ((Void) -> Void)?
    
    init(title: String, backgroundColor: UIColor = FIEConstants.Colors.Orange, onTap: @escaping ((Void) -> Void)) {
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(title, for: UIControlState())
        button.titleLabel?.font = FIEConstants.Fonts.SizeSmallWeightMedium
        button.backgroundColor = backgroundColor
        
        super.init(frame: CGRect.zero)
        
        self.onTapCallback = onTap
        button.addTarget(self, action: #selector(onTapAction), for: .touchUpInside)
        
        addSubview(button)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        
        if hasCreatedConstraints == false {
            hasCreatedConstraints = true
            
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[button]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["button": button]))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[button]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["button": button]))
        }
        super.updateConstraints()
    }
    
    func onTapAction(_ button: UIButton) {
        onTapCallback?()
    }
}
