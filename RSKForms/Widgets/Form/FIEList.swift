//
//  FIEList.swift
//  Fielco-iOS
//
//  Created by Ángel Balbuena on 6/23/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

class FIEList: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var contents: [String]!
    var tableView: UITableView!

    init(contents: [String]) {
        super.init()
        self.contents = contents
        
        tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: nil)
        
        cell.textLabel?.text = contents[(indexPath as NSIndexPath).row]
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.backgroundColor = UIColor.red
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
