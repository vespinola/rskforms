//
//  FIEFormTextView.swift
//  Fielco-iOS
//
//  Created by Roshka on 6/22/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

class FIEFormTextView: FIETextView {
    
    var placeholder: String?
    var _onChange: ((String?) -> Void)?
    fileprivate var hasCreatedConstraints = false
    fileprivate var placeholderTextColor = FIEConstants.Colors.Placeholder
    fileprivate var defaultTextColor = FIEConstants.Colors.Text
    
//    override var delegate: UITextViewDelegate? {
//        didSet {
//            
//            let originalSelector = #selector(delegate!.textView(_:shouldChangeTextInRange:replacementText:))
//            let swizzledSelector = #selector(rsk_textView(_:shouldChangeTextInRange:replacementText:))
//            
//            let originalMethod = class_getInstanceMethod(object_getClass(delegate!), originalSelector)
//            let swizzledMethod = class_getInstanceMethod(object_getClass(self), swizzledSelector)
//            
//            method_exchangeImplementations(originalMethod, swizzledMethod)
//            print("Swizzled Methods")
//        }
//    }
//    
//    func rsk_textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
//        print("Calling Swizzled Method")
//        (textView as! FIEFormTextView).rsk_textView(textView, shouldChangeTextInRange: range, replacementText: text)
//        return true
//    }

    init(placeholder: String?, onChange: ((String?) -> Void)? = nil) {
        super.init(frame: CGRect.zero, textContainer: nil)
        self._onChange = onChange
        isScrollEnabled = false
        self.placeholder = placeholder
        text = self.placeholder ?? ""
        backgroundColor = FIEConstants.Colors.Background
        textColor = FIEConstants.Colors.Text
        font = FIEConstants.Fonts.SizeMediumWeightLight
        
        autocorrectionType = .no
        autocapitalizationType = .sentences
        fie_addBorder()
        
        NotificationCenter.default.addObserver(self, selector: #selector(didBeginEditingNotification), name: NSNotification.Name.UITextViewTextDidBeginEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didChangeNotification), name: NSNotification.Name.UITextViewTextDidChange, object: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        
        if hasCreatedConstraints == false {
            hasCreatedConstraints = true
            
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[textView(>=50)]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["textView": self]))
        }
        super.updateConstraints()
    }
    
    // #MARK: - NSNotificationCenter
    
    func didBeginEditingNotification(_ notification: Notification) {
        
    }
    
    func didChangeNotification(_ notification: Notification) {
        _onChange?(text)
//        if text == placeholder {
//            text = ""
//            textColor = place
//        }
    }
    
    
//    func didEndEditingNotification(notification: NSNotification) {
//        if let placeholder = placeholder where text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()) == "" {
//            text = placeholder
//            textColor = FIEConstants.Colors.Placeholder
//        }
//    }
}
