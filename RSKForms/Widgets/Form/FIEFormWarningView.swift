//
//  FIEFormWarningView.swift
//  Fielco-iOS
//
//  Created by Roshka on 7/20/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

class FIEFormWarningView: UIView {
        
    var label: UILabel!
    var imageView: UIImageView!
    var text: String!
    fileprivate var hasCreatedConstraints = false
    
    init(_ text: String? = nil) {
        super.init(frame: CGRect.zero)
        self.text = text
        setup()
    }
    
    
    func setup() {
        backgroundColor = FIEConstants.Colors.Yellow
        label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = FIEConstants.Fonts.SizeSmallWeightMedium
        label.textColor = FIEConstants.Colors.Blue
        label.text = text
        addSubview(label)
        
        imageView = UIImageView(image: UIImage(named: "exclamation-blue")!)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        addSubview(imageView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        
        if hasCreatedConstraints == false {
            hasCreatedConstraints = true
            
            let views = ["imageView": imageView, "label": label] as [String : Any]
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-(20)-[imageView]-[label]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[label]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
            
            addConstraint(NSLayoutConstraint(item: imageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
            addConstraint(NSLayoutConstraint(item: imageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 0, constant: 30))
            addConstraint(NSLayoutConstraint(item: imageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 0, constant: 30))
        }
        
        super.updateConstraints()
    }
}

