//
//  FIEFormSectionHeaderView.swift
//  Fielco-iOS
//
//  Created by Roshka on 7/20/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit


class FIEFormSectionHeaderView: UIView {
    
    var label: UILabel!
    var text: String!
    fileprivate var hasCreatedConstraints = false
    
    init(_ text: String? = nil) {
        super.init(frame: CGRect.zero)
        self.text = text
        setup()
    }
    
    
    func setup() {
        backgroundColor = UIColor.clear
        label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = FIEConstants.Fonts.SizeMediumWeightMedium
        label.textColor = FIEConstants.Colors.TextDark
        label.text = text
        addSubview(label)
        
        // Set the view's frames according to its constraints.
        setNeedsLayout()
        layoutIfNeeded()
        
        let path = UIBezierPath()
        let onePxWidth = FIEUtility.onePixelWidth()
        path.move(to: CGPoint.zero)
        path.addLine(to: CGPoint(x: frame.maxX, y: 0))
        path.close()
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = FIEConstants.Colors.Border.cgColor
        shapeLayer.lineWidth = onePxWidth
        shapeLayer.frame = bounds
        layer.addSublayer(shapeLayer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        print(rect)
        
        FIEConstants.Colors.Border.setStroke()
        
        let path = UIBezierPath()
        path.lineWidth = FIEUtility.onePixelWidth()
        path.move(to: CGPoint.zero)
        path.addLine(to: CGPoint(x: frame.maxX, y: 0))
        path.close()
        path.stroke()
    }
    
    
    override func updateConstraints() {
        
        if hasCreatedConstraints == false {
            hasCreatedConstraints = true
            
            let views = ["label": label]
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-(\(FIEUtility.StdSpace))-[label]-(\(FIEUtility.StdSpace))-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(20)-[label]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        }
        
        super.updateConstraints()
    }
}
