//
//  FIEFormMessageView.swift
//  Fielco-iOS
//
//  Created by Paul Von Schrottky on 6/14/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

class FIEFormMessageView: UIView {
    
    var label: UILabel!
    var text: String!
    fileprivate var hasCreatedConstraints = false
    
    init(_ text: String) {
        super.init(frame: CGRect.zero)
        self.text = text
        setup()
    }
    
    
    func setup() {
        backgroundColor = FIEConstants.Colors.Foreground
        label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = FIEConstants.Fonts.SizeVerySmallWeightLight
        label.textColor = FIEConstants.Colors.Text
        label.text = text
        addSubview(label)
        
        fie_addBorder()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func updateConstraints() {
    
        if hasCreatedConstraints == false {
            hasCreatedConstraints = true
            
            let views = ["label": label]
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[label]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[label]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        }
        
        super.updateConstraints()
    }
}
