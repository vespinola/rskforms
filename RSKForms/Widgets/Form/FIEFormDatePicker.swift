//
//  FIEFormDatePicker.swift
//  Fielco-iOS
//
//  Created by Ángel Balbuena on 7/13/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

typealias FIEFormDatePickerItem = (title: String, value: AnyObject)
typealias FIEFormDatePickerCallback = ((FIEFormDatePickerItem) -> Void)

class FIEFormDatePicker: FIETextField {
    
    var pickerView: UIDatePicker!
    
    fileprivate var hasCreatedConstraints = false
    
    init(placeholder: String) {
        super.init(frame: CGRect.zero)
        setup()
        self.placeholder = placeholder
    }
    
    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func updateConstraints() {
        if hasCreatedConstraints == false {
            hasCreatedConstraints = true
            
            addConstraint(NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 0, constant: 30))
        }
        super.updateConstraints()
    }
    
    override func setup() {
        super.setup()
        
        backgroundColor = FIEConstants.Colors.Background
        textColor = FIEConstants.Colors.Text
        font = FIEConstants.Fonts.SizeMediumWeightLight
        
        autocorrectionType = .no
        autocapitalizationType = .none
        
        // Replace this field's keyboard with the picker.
        pickerView = UIDatePicker()
        inputView = self.pickerView
        pickerView.datePickerMode = .date
        
        let rightView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        let imageView = UIImageView(image: UIImage(named: "chevron-gray"))
        rightView.addSubview(imageView)
        imageView.frame = CGRect(x: 5, y: 5, width: 15, height: 15)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        self.rightView = rightView
        
        
        onCloseKeyboard = {
            let date = self.pickerView.date
            //se multiplica por 1000 para convertir a milisegundos
            self.text = Date.rsk_dateForTimestamp(date.timeIntervalSince1970 * 1000)
        }
        
        fie_addBorder()
    }
    
    // MARK: Overrides
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: 0, width: bounds.width - 20, height: bounds.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: 0, width: bounds.width - 20, height: bounds.height)
    }
    
    // Overriding this hides the cursor.
    override func caretRect(for position: UITextPosition) -> CGRect {
        return CGRect.zero
    }
    
    // MARK: UIPickerViewDataSource
    
    func numberOfComponentsInPickerView(_ pickerView: UIPickerView) -> Int {
        return 1
    }
}
