//
//  FIEFormAmountInput.swift
//  Fielco-iOS
//
//  Created by Paul Von Schrottky on 6/14/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

class FIEFormAmountInput: FIETextField, UITextFieldDelegate {

    let MAX_DECIMAL_PLACES = 2
    let DECIMAL_SEPARATOR = ","
    let THOUSANDS_SEPARATOR = "."
    var currencyType: FIECurrencyType!
    var onChange: ((FIEAmount?) -> Void)?
    var onFocus: ((Void) -> Void)?
    fileprivate var hasCreatedConstraints = false

    convenience init(_ currencyType: FIECurrencyType, defaultValue: String? = nil, onChange: ((FIEAmount?) -> Void)? = nil) {
        self.init(frame: CGRect.zero)
        translatesAutoresizingMaskIntoConstraints = false
        delegate = self
        self.currencyType = currencyType
        self.text = defaultValue
        self.onChange = onChange
        keyboardType = .numberPad
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        translatesAutoresizingMaskIntoConstraints = false
        delegate = self
        keyboardType = .numberPad
    }
    
    override func setup() {
        super.setup()
        backgroundColor = FIEConstants.Colors.Background
        textColor = FIEConstants.Colors.Text
        font = FIEConstants.Fonts.SizeMediumWeightLight
        
        autocorrectionType = .no
        autocapitalizationType = .none
        
        fie_addBorder()
        
        if self.currencyType == FIECurrencyType.usd {
            
            let toolbar = UIToolbar.rsk_newAutolayout()
            toolbar.tintColor = FIEConstants.Colors.TextDark
            toolbar.backgroundColor = UIColor.white
            let closeBarButtonItem = RSKBarButtonItem(title: "Listo", onClick: { _ in
                self.resignFirstResponder()
            })
            let spaceBarButtonItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let commaBarButtonItem = RSKBarButtonItem(named: "comma") { _ in

                // Allow a maximum of one DECIMAL_SEPARATOR
                if self.text?.range(of: self.DECIMAL_SEPARATOR) == nil {
                    let newCursorPosition = self.position(from: self.endOfDocument, offset: 0)!
                    let newSelectedRange = self.textRange(from: newCursorPosition, to: newCursorPosition)
                    self.selectedTextRange = newSelectedRange
                    self.insertText(self.DECIMAL_SEPARATOR)

                }
            }
            toolbar.setItems([commaBarButtonItem, spaceBarButtonItem, closeBarButtonItem], animated: false)
            let toolbarWrapperView = UIView()
            toolbarWrapperView.addSubview(toolbar)
            toolbar.rsk_fillParentView(0)
            self.inputAccessoryView = toolbar
            self.inputAccessoryView!.backgroundColor = UIColor.white
            

//            
//            if let items = toolbar?.items {
//                var newItems = items
//                newItems.insert(commaBarButtonItem, atIndex: 0)
//                toolbar.setItems(items, animated: false)
//            } else if let toolbar = toolbar {
//                toolbar.setItems([commaBarButtonItem], animated: false)
//            }
//            inputAccessoryView = toolbar
//            toolbar.sizeToFit()
        } else {
            fie_addDoneButton()
        }
    }
    
    override func updateConstraints() {
        if hasCreatedConstraints == false {
            hasCreatedConstraints = true
            
            addConstraint(NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 0, constant: 30))
        }
        super.updateConstraints()
    }
    
    // #MARK - UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print(#function)
        let shouldChange = amountFormatter(textField, range: range, string: string)
        
        if shouldChange == true {
            let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            onChange?(newText.fie_unformatAmount())
            return true
        }
        
        onChange?(textField.text?.fie_unformatAmount())
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        onFocus?()
    }
    
    //
    func amountFormatter(_ textField: UITextField, range: NSRange, string: String) -> Bool {
        
        var range = range   // A mutable copy of the range.
        let text = textField.text! as NSString
        
        // Whether or not the user just typed the decimal separator.
        let didTypeDecimalSeparator = string.contains(DECIMAL_SEPARATOR)
        
        // If the thousands separator was deleted, adjust the range to include the next character to the left.
        if text.substring(with: range) == THOUSANDS_SEPARATOR && string.isEmpty {
            range.location -= 1
            range.length += 1
        }
        
        // The new text.
        let newText = text.replacingCharacters(in: range, with: string)
        
        // Allow the text to be erased.
        guard newText.rsk_containsValue else { return true }
        
        // Don't allow more than MAX_DECIMAL_PLACES number of decimal places (e.g. 10,432 is not allowed)
        if let decimalPointRange = newText.range(of: DECIMAL_SEPARATOR) , newText.characters.distance(from: newText.startIndex, to: decimalPointRange.lowerBound) < newText.characters.count - 1 - MAX_DECIMAL_PLACES {
            return false    // Don't make the change.
        }

        let newRawAmount = newText.fie_unformatAmount()
        let newlyFormattedText = newRawAmount?.fie_formatThousandsWithCurrency(currencyType, showCurrency: false, showMinimalCents: true)
        
//        let delta = newlyFormattedText!.characters.count - textField.text!.characters.count + (didTypeDecimalSeparator ? 1 : 0)
        
//        let previousTextPosition = textField.selectedTextRange!.start
        
        textField.text = newlyFormattedText! + (didTypeDecimalSeparator ? DECIMAL_SEPARATOR : "")
        
//        let adjustedTextPosition = textField.positionFromPosition(previousTextPosition, offset: delta)!
        
//        textField.selectedTextRange = textField.textRangeFromPosition(adjustedTextPosition, toPosition: adjustedTextPosition)
        
        return false
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 10, dy: 0)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 10, dy: 0)
    }
}


//            // Formatting *** DO NOT DELETE ***
//            if let decimalPointRange = newText.rangeOfString(DECIMAL_SEPARATOR) {
//                let decimalPointIndex = newText.startIndex.distanceTo(decimalPointRange.startIndex)
//
//                if decimalPointIndex < newText.characters.count - 1 && newText.hasSuffix(DECIMAL_SEPARATOR) == false  {
//                    let myRange = NSRange(location: decimalPointIndex + 1, length: newText.characters.count - 1 - decimalPointIndex) // range starting at location 17 with a lenth of 7: "Strings"
//                    let attributedString = NSMutableAttributedString(string: textField.text!)
//                    attributedString.addAttribute(NSFontAttributeName, value: UIFont.italicSystemFontOfSize(8), range: myRange)
//                    attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGrayColor(), range: myRange)
//                    attributedString.addAttribute(NSBaselineOffsetAttributeName, value: 4, range: myRange)
//                    textField.attributedText = attributedString
//                } else {
//                    textField.attributedText = NSAttributedString(string: textField.text!)
//                }
//            }
