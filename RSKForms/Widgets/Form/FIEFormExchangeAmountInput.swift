//
//  FIEFormExchangeAmountInput.swift
//  Fielco-iOS
//
//  Created by Roshka on 6/29/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

enum FIEFormExchangeType {
    case buying, selling, none
}


/// A struct including the transaction amount in source and destination currencies, and exchange rates.
struct FIETransactionAmount {
    
    var buyingRate: FIEAmount?
    var sellingRate: FIEAmount?
    var sourceCurrencyInfo: FIEDictionary?
    var destinationCurrencyInfo: FIEDictionary?
    
    init() {
        
    }
    
    init(amount: FIEAmount?) {
        if let amount = amount {
            self.amount = amount
        }
    }
    
    /// The source amount.
    var amount: FIEAmount?
    
    /// The destination amount.
    var destinationAmount: FIEAmount? {
        guard let buyingRate = buyingRate, let sellingRate = sellingRate else { return nil }
        if exchangeType == .buying {
            return amount?.multiplying(by: buyingRate)
        } else if exchangeType == .selling {
            return amount?.dividing(by: sellingRate)
        } else {
            return nil
        }
    }
    
    /// The source amount formatted in its currency.
    var sourceAmountFormatted: String? {
        guard let sourceCurrencyInfo = sourceCurrencyInfo else { return nil }
        return amount?.fie_formatThousandsWithCurrencyInfo(sourceCurrencyInfo)
    }
    
    /// The destination amount formatted in its currency.
    var destinationAmountFormatted: String? {
        guard let destinationCurrencyInfo = destinationCurrencyInfo else { return nil }
        return destinationAmount?.fie_formatThousandsWithCurrencyInfo(destinationCurrencyInfo)
    }
    
    /// If currencies differ, the currency info for the PYG currency.
    var pygCurrencyInfo: FIEDictionary? {
        if let sourceCurrencyInfo = sourceCurrencyInfo , FIEUtility.currencyTypeForCurrencyInfo(sourceCurrencyInfo) == .pyg {
            return sourceCurrencyInfo
        } else if let destinationCurrencyInfo = destinationCurrencyInfo , FIEUtility.currencyTypeForCurrencyInfo(destinationCurrencyInfo) == .pyg {
            return destinationCurrencyInfo
        }
        return nil
    }
    
    /// If currencies differ, the currency info for the non-PYG currency.
    var otherCurrencyInfo: FIEDictionary? {
        if let sourceCurrencyInfo = sourceCurrencyInfo , FIEUtility.currencyTypeForCurrencyInfo(sourceCurrencyInfo) != .pyg {
            return sourceCurrencyInfo
        } else if let destinationCurrencyInfo = destinationCurrencyInfo , FIEUtility.currencyTypeForCurrencyInfo(destinationCurrencyInfo) != .pyg {
            return destinationCurrencyInfo
        }
        return nil
    }
    
    /// If currencies differ, whether we are buying or selling the non-PYG currency.
    var exchangeType: FIEFormExchangeType {
        guard currenciesDiffer else { return .none }
        
        if let sourceCurrencyInfo = sourceCurrencyInfo , FIEUtility.currencyTypeForCurrencyInfo(sourceCurrencyInfo) != .pyg {
            return .buying
        } else if let destinationCurrencyInfo = destinationCurrencyInfo , FIEUtility.currencyTypeForCurrencyInfo(destinationCurrencyInfo) != .pyg {
            return .selling
        }
        return .none
    }
    
    
    var sourceCurrencySymbol: String? {
        guard let sourceCurrencyInfo = sourceCurrencyInfo else { return nil }
        return FIEUtility.currencySymbolForCurrencyInfo(sourceCurrencyInfo)
    }
    
    var destinationCurrencySymbol: String? {
        guard let destinationCurrencyInfo = destinationCurrencyInfo else { return nil }
        return FIEUtility.currencySymbolForCurrencyInfo(destinationCurrencyInfo)
    }
    
    var sourceCurrencyType: FIECurrencyType? {
        guard let sourceCurrencyInfo = sourceCurrencyInfo else { return nil }
        return FIEUtility.currencyTypeForCurrencyInfo(sourceCurrencyInfo)
    }
    
    var destinationCurrencyType: FIECurrencyType? {
        guard let destinationCurrencyInfo = destinationCurrencyInfo else { return nil }
        return FIEUtility.currencyTypeForCurrencyInfo(destinationCurrencyInfo)
    }
    
    var buyingRateFormatted: String? {
        guard let pygCurrencyInfo = pygCurrencyInfo else { return nil }
        return buyingRate?.fie_formatThousandsWithCurrencyInfo(pygCurrencyInfo)
    }
    
    var sellingRateFormatted: String? {
        guard let pygCurrencyInfo = pygCurrencyInfo else { return nil }
        return sellingRate?.fie_formatThousandsWithCurrencyInfo(pygCurrencyInfo)
    }
    
    var currenciesDiffer: Bool {
        guard let sourceCurrencyInfo = sourceCurrencyInfo else { return false }
        guard let destinationCurrencyInfo = destinationCurrencyInfo else { return false }
        return FIEUtility.currencyTypeForCurrencyInfo(sourceCurrencyInfo) != FIEUtility.currencyTypeForCurrencyInfo(destinationCurrencyInfo)
    }
}

//class FIEFormExchangeAmountInput: UIView {
//
//    private var hasCreatedConstraints = false
//    
//    var amountLabel: FIEFormLabel!
//    var amountInput: FIEFormAmountInput!
//    var exchangeAmountLabel: FIEFormLabel?
//    var exchangeAmountInput: FIEFormAmountInput?
//    
//    var transactionAmount: FIETransactionAmount!
//    
//    init(viewController: UIViewController, sourceProduct: FIEDictionary, destinationProduct: FIEDictionary, onChange: (FIETransactionAmount -> Void)) {
//        super.init(frame: CGRectZero)
//        
//        self.transactionAmount = FIETransactionAmount()
//        self.transactionAmount.sourceCurrencyInfo = FIEUtility.currencyInfoForProduct(sourceProduct)
//        self.transactionAmount.destinationCurrencyInfo = FIEUtility.currencyInfoForProduct(destinationProduct)
//
//        amountLabel = FIEFormLabel(text: "Monto de la transferencia (\(transactionAmount.sourceCurrencySymbol!))")
//        amountInput = FIEFormAmountInput(transactionAmount.sourceCurrencyType!) { amount in
//            
//            // Update the source amount.
//            self.transactionAmount.amount = amount
//            
//            // Update the destination amount.
//            if let destinationAmountFormatted = self.transactionAmount.destinationAmountFormatted {
//                self.exchangeAmountInput?.text = destinationAmountFormatted
//            }
// 
//            // Fire the on change event.
//            onChange(self.transactionAmount)
//        }
//        addSubview(amountLabel)
//        addSubview(amountInput)
//        
//        if transactionAmount.currenciesDiffer {
//            exchangeAmountLabel = FIEFormLabel(text: "Monto de la transferencia (\(self.transactionAmount.destinationCurrencySymbol!))")
//            exchangeAmountInput = FIEFormAmountInput(self.transactionAmount.sourceCurrencyType!)
//            exchangeAmountInput?.userInteractionEnabled = false
//            addSubview(exchangeAmountLabel!)
//            addSubview(exchangeAmountInput!)
//            
//            let command = "datos-generales-fielco/cotizacion/" + FIEUtility.currencyISOForCurrencyInfo(transactionAmount.otherCurrencyInfo!)
//            
//            FIEHTTP.req(command: command, viewController: viewController, onSuccess: { response in
//                let exchangeRateInfo = response.rsk_JSONToDictionary()!
//                
//                self.transactionAmount.buyingRate = FIEAmount(decimal: (exchangeRateInfo["compra"] as! NSNumber).decimalValue)
//                self.transactionAmount.sellingRate = FIEAmount(decimal: (exchangeRateInfo["venta"] as! NSNumber).decimalValue)
//            })
//        }
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//    
//    override func updateConstraints() {
//        
//        if hasCreatedConstraints == false {
//            hasCreatedConstraints = true
//            
//            amountLabel.rsk_clipTopToTopOfParentView(0)
//            amountLabel.rsk_clipHorizontallyToParentView(0)
//            amountInput.rsk_clipTopToBottomOfView(amountLabel)
//            amountInput.rsk_clipHorizontallyToParentView(0)
//
//            if let exchangeAmountLabel = exchangeAmountLabel, exchangeAmountInput = exchangeAmountInput {
//                
//                exchangeAmountLabel.rsk_clipTopToBottomOfView(amountInput)
//                exchangeAmountLabel.rsk_clipHorizontallyToParentView(0)
//            
//                exchangeAmountInput.rsk_clipTopToBottomOfView(exchangeAmountLabel)
//                exchangeAmountInput.rsk_clipHorizontallyToParentView(0)
//                exchangeAmountInput.rsk_clipBottomToBottomOfParentView(0)
//            } else {
//                amountInput.rsk_clipBottomToBottomOfParentView(0)
//            }
//        }
//        super.updateConstraints()
//    }
//
//}
