//
//  FIEPicker.swift
//  Fielco-iOS
//
//  Created by Juan Carlos Gomez on 4/28/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

class FIEPicker: FIETextField, UIPickerViewDataSource, UIPickerViewDelegate {

    var pickerView: UIPickerView!
    var contents = [String]()
    

    
    override func awakeFromNib() {
        super.setup()
        
        autocorrectionType = .no
        autocapitalizationType = .none
        
        // Replace this field's keyboard with the picker.
        self.pickerView = UIPickerView()
        self.pickerView.dataSource = self
        self.pickerView.delegate = self
        self.inputView = self.pickerView
        
        let rightView = FIEFormPickerRightView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        rightView.isUserInteractionEnabled = false
        let imageView = UIImageView(image: UIImage(named: "chevron-gray"))
        rightView.addSubview(imageView)
        imageView.frame = CGRect(x: 5, y: 5, width: 15, height: 15)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        self.rightView = rightView
        
        
        self.onCloseKeyboard = {
            let selectedRow = self.pickerView.selectedRow(inComponent: 0)
            let text = self.pickerView.delegate?.pickerView!(self.pickerView, titleForRow: selectedRow, forComponent: 0)
            self.text = text
        }
    }
    
    // Overriding this hides the cursor.
    override func caretRect(for position: UITextPosition) -> CGRect {
        return CGRect.zero
    }
    
    // MARK: UIPickerViewDataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data.
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.contents.count
    }
    
    // The title to return for the row and component (column) that's being passed in.
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        guard row < contents.count else { return nil }
        return self.contents[row]
    }
    
    func selectedIndex() -> Int {
        return self.pickerView.selectedRow(inComponent: 0)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // This method is triggered whenever the user makes a change to the picker selection.
        // The parameter named row and component represents what was selected.
        text = contents[row]
    }
}
