//
//  FIETextField.swift
//  Fielco-iOS
//
//  Created by Juan Carlos Gomez on 4/28/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

class FIETextField: UITextField {
    
    var onCloseKeyboard: ((Void) -> Void)?    // A callback to fire when this field loses focus.
    var toolbar: UIToolbar!
    
    override func awakeFromNib() {
        self.setup()
    }
    
    func setup() {

        // Add a Close button to the keyboard.
        toolbar = UIToolbar.rsk_newAutolayout()
        toolbar.tintColor = UIColor.gray
        toolbar.backgroundColor = UIColor.white
        let closeBarButtonItem = RSKBarButtonItem(title: "Listo", onClick: { _ in
            self.resignFirstResponder()
            self.onCloseKeyboard?()
        })
        rightViewMode = UITextFieldViewMode.always
        
        let spaceBarButtonItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.setItems([spaceBarButtonItem, closeBarButtonItem], animated: false)
        let toolbarWrapperView = UIView()
        toolbarWrapperView.addSubview(toolbar)
        toolbar.rsk_fillParentView(0)
        inputAccessoryView = toolbar
        inputAccessoryView!.backgroundColor = UIColor.clear
    }

    override func becomeFirstResponder() -> Bool {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "InputOnFocus"), object: nil, userInfo: [ "view": self ])
        return super.becomeFirstResponder()
    }
}
