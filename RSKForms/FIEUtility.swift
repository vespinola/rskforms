//
//  FIEUtility.swift
//  Fielco-iOS
//
//  Created by Paul Von Schrottky on 5/24/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit
import BRYXBanner
import NVActivityIndicatorView
import LocalAuthentication

@available(iOS, deprecated: 1.0, message: "I'm not deprecated, please ***FIXME**")
func TODO()
{
    
}

enum FIESIPAPMode {
    case none
    case lbtr
    case ach
    
    init(_ value: String) {
        switch value {
        case "LBTR":
            self = .lbtr
        case "ACH":
            self = .ach
        default:
            self = .none
        }
    }
    
    func pretty() -> String? {
        switch self {
        case .lbtr:
            return "Inmediata - LBTR"
        case .ach:
            return "Corte diario - ACH"
        default:
            return nil
        }
    }
    
    func value() -> String? {
        switch self {
        case .lbtr:
            return "LBTR"
        case .ach:
            return "ACH"
        default:
            return nil
        }
    }
}

enum FIEProductType {
    case account
    case card
    
    var serverType: String {
        switch self {
        case .account:
            return "CUENTA"
        case .card:
            return "TARJETA"
        }
    }
}

typealias FIEDictionary = RSKDictionary
typealias FIEArray = RSKArray

public extension Sequence {
    
    /// Categorises elements of self into a dictionary, with the keys given by keyFunc
    // See: http://stackoverflow.com/a/31220067/1305067
    
    func categorise<U : Hashable>(_ keyFunc: (Iterator.Element) -> U) -> [U:[Iterator.Element]] {
        var dict: [U:[Iterator.Element]] = [:]
        for el in self {
            let key = keyFunc(el)
            if case nil = dict[key]?.append(el) { dict[key] = [el] }
        }
        return dict
    }
}

class FIEUtility: NSObject {
    
    class func appVersion() -> String {
        let path = Bundle.main.path(forResource: "Info", ofType: "plist")!
        let dict = NSDictionary(contentsOfFile: path) as! [String: AnyObject]
        let version = dict["CFBundleShortVersionString"] as! String
        let build = dict["CFBundleVersion"] as! String
        return "v" + version + " (" + build + ")"
    }
    
    class func appVersionFull() -> String {
        return "Versión " + appVersion()
    }
    
    class func activityIndicatorWithSize(_ size: CGSize) -> NVActivityIndicatorView {
        let view = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height), type: .ballClipRotateMultiple , color: FIEConstants.Colors.Blue)
        view.startAnimating()
        rsk_delay(0.3, closure: {
            if let firstCircleLayer = view.layer.sublayers?[0] as? CAShapeLayer {
                firstCircleLayer.strokeColor = FIEConstants.Colors.Blue.cgColor
            }
            if let secondCircleLayer = view.layer.sublayers?[1] as? CAShapeLayer {
                secondCircleLayer.strokeColor = FIEConstants.Colors.Orange.cgColor
            }
        })
        return view
    }
    
    class func onePixelWidth() -> CGFloat {
        return 1 / UIScreen.main.scale * 2
    }
    
    static let TriSpace: CGFloat = StdSpace * 3
    static let DblSpace: CGFloat = StdSpace * 2
    static let StdSpace: CGFloat = HalfSpace * 2
    static let HalfSpace: CGFloat = 8
    

    
    class func navigationBarTitleViewWithTitle(_ title: String, subtitle: String, animate: Bool? = false) -> (titleView: UIView, titleLabel: UILabel, subtitleLabel: UILabel) {
        let titleView = UIView.rsk_newAutolayout()
        
        let titleLabel = UILabel.rsk_newAutolayout()
        titleLabel.textColor = FIEConstants.Colors.Light
        titleLabel.textAlignment = .center
        titleLabel.text = title
        titleView.addSubview(titleLabel)
        titleLabel.rsk_clipHorizontallyToParentView(0)
        titleLabel.rsk_clipTopToTopOfParentView(0)
        titleLabel.adjustsFontSizeToFitWidth = true
        
        let subtitleLabel = UILabel.rsk_newAutolayout()
        subtitleLabel.textColor = FIEConstants.Colors.Light
        subtitleLabel.font = FIEConstants.Fonts.SizeVerySmallWeightRegular
        subtitleLabel.textAlignment = .center
        subtitleLabel.text = subtitle
        titleView.addSubview(subtitleLabel)
        subtitleLabel.rsk_clipHorizontallyToParentView(0)
        subtitleLabel.rsk_clipTopToBottomOfView(titleLabel, margin: 0)
        subtitleLabel.rsk_clipBottomToBottomOfParentView(0)
        
        titleView.setNeedsLayout()
        titleView.layoutIfNeeded()
        titleView.sizeToFit()
        let titleViewSize = titleView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        titleView.frame = CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height)
        titleView.translatesAutoresizingMaskIntoConstraints = true
        
        if animate == true {
            rsk_delay(0.6, closure: {
                subtitleLabel.fie_zoomAnimate()
            })
        }
        return (titleView, titleLabel, subtitleLabel)
    }
    
    static let PRODUCT_NUMBER_SEPARATOR = "-"
    static let PRODUCT_NAME_NUMBER_SEPARATOR = " - "
    static let ACCOUNT_DEFAULT_NAME = "Ahorro a la vista"
    static let CARD_DEFAULT_NAME = "Tarjeta"
    static let LOAN_DEFAULT_NAME = "Préstamo"
    static let CDA_DEFAULT_NAME = "CDA"
    
    
    class func getAccounts(_ accounts: RSKArray, ofCurrency currency: FIECurrencyType) -> RSKArray {
        return accounts.filter {
            return FIEUtility.currencyTypeForProduct($0) == currency
        }
    }
    
    class func removeAccount(_ accountToRemove: RSKDictionary, fromAccounts accounts: RSKArray) -> RSKArray {
        return accounts.filter {
            FIEUtility.accountID($0) != FIEUtility.accountID(accountToRemove)
        }
    }
    
    class func statementTitleImage(_ image: UIImage) -> UIView {
        let templateImage = image.withRenderingMode(.alwaysTemplate)
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        imageView.contentMode = .scaleAspectFit
        imageView.image = templateImage
        imageView.tintColor = FIEConstants.Colors.Light
        return imageView
    }
    
    class func showAlertInViewController(_ viewController: UIViewController? = nil, message: String, wasDismissed: (() -> ())? = nil) {
        let banner = Banner(subtitle: message, image: nil, backgroundColor: FIEConstants.Colors.Orange)
        banner.dismissesOnTap = true
        if let view = viewController?.view {
            banner.show(view, duration: 8)
        } else {
            banner.show(duration: 8)
        }
        banner.didDismissBlock = wasDismissed
    }
    
    class func facebook() {
        let URL = Foundation.URL(string: "fb://profile/130235437023256")!
        if UIApplication.shared.canOpenURL(URL) {
            UIApplication.shared.openURL(URL)
        } else {
            UIApplication.shared.openURL(Foundation.URL(string: "https://www.facebook.com/financieraelcomercio")!)
        }
    }
    
    class func twitter() {
        let URL = Foundation.URL(string: "twitter://user?id=342719471")!
        if UIApplication.shared.canOpenURL(URL) {
            UIApplication.shared.openURL(URL)
        } else {
            UIApplication.shared.openURL(Foundation.URL(string: "https://twitter.com/elcomerciof")!)
        }
    }
    
    class func instagram() {
        
        let URL = Foundation.URL(string: "instagram://user?username=elcomerciof")!
        if UIApplication.shared.canOpenURL(URL) {
            UIApplication.shared.openURL(URL)
        } else {
            UIApplication.shared.openURL(Foundation.URL(string: "https://www.instagram.com/elcomerciof/")!)
        }
    }
    class func youtube() {
        let URL = Foundation.URL(string: "youtube://www.youtube.com/user/FinancieraElComercio")!
        if UIApplication.shared.canOpenURL(URL) {
            UIApplication.shared.openURL(URL)
        } else {
            UIApplication.shared.openURL(Foundation.URL(string: "https://www.youtube.com/user/FinancieraElComercio")!)
        }
    }
    class func whatsapp() {
        
        let whatsappURL: URL = URL(string: "whatsapp://app")!
        if UIApplication.shared.canOpenURL(whatsappURL) {
            UIApplication.shared.openURL(whatsappURL)
        }
    }
    
    class func call(_ viewController: UIViewController) {
        
        let style = RSKMenuDevice.device() == RSKMenuDevice.phone ? UIAlertControllerStyle.actionSheet : UIAlertControllerStyle.alert
        let buttonTitle = RSKMenuDevice.device() == RSKMenuDevice.phone ? "Cancelar" : "Ok"
        
        let alertController = UIAlertController(title: "Llamar a Fielco", message: nil, preferredStyle: style)
        
        alertController.addAction(UIAlertAction(title: buttonTitle, style: .cancel, handler: { alertAction in
            viewController.dismiss(animated: true, completion: nil)
        }))
        
        alertController.addAction(UIAlertAction(title: "*8811 - gratis para usuarios de Tigo y Personal", style: .default, handler: { alertAction in
            let URL = Foundation.URL(string: "tel://*8811")!
            if UIApplication.shared.canOpenURL(URL) {
                UIApplication.shared.openURL(URL)
            } else {
                
            }
        }))
        alertController.addAction(UIAlertAction(title: "021 6188000", style: .default, handler: { alertAction in
            let URL = Foundation.URL(string: "tel://0216188000")!
            if UIApplication.shared.canOpenURL(URL) {
                UIApplication.shared.openURL(URL)
            } else {
                
            }
        }))
        
        
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    
    
    
    // GENERAL
    
    class func foreignCurrencyInfo(_ product1: FIEDictionary, product2: FIEDictionary) -> FIEDictionary? {
        let product1CurrencyType = currencyTypeForProduct(product1)
        let product2CurrencyType = currencyTypeForProduct(product2)
        
        if product1CurrencyType == product2CurrencyType {
            return nil
        } else if product1CurrencyType != .pyg {
            return currencyInfoForProduct(product1)
        } else if product2CurrencyType != .pyg {
            return currencyInfoForProduct(product2)
        }
        return nil
    }
    
    
    
    class func currencyInfoForProduct(_ product: FIEDictionary) -> FIEDictionary {
        return product["moneda"] as! FIEDictionary
    }
    
    class func currencyISOForProduct(_ product: FIEDictionary) -> String {
        let currencyInfo = currencyInfoForProduct(product)
        return currencyISOForCurrencyInfo(currencyInfo)
    }
    
    class func currencyTypeForProduct(_ product: FIEDictionary) -> FIECurrencyType {
        let currencyISO = currencyISOForProduct(product)
        return FIECurrencyType.typeForISO(currencyISO)!
    }
    
    class func currencySymbolForProduct(_ product: FIEDictionary) -> String {
        let currencyInfo = currencyInfoForProduct(product)
        return currencySymbolForCurrencyInfo(currencyInfo)
    }
    
    class func currencySymbolForCurrencyInfo(_ currencyInfo: FIEDictionary) -> String {
        return currencyInfo["simbolo"] as! String
    }
    
    class func currencyTypeForCurrencyInfo(_ currencyInfo: FIEDictionary) -> FIECurrencyType {
        let currencyISO = currencyISOForCurrencyInfo(currencyInfo)
        return FIECurrencyType.typeForISO(currencyISO)!
    }
    
    class func currencyISOForCurrencyInfo(_ currencyInfo: FIEDictionary) -> String {
        return currencyInfo["iso"] as! String
    }
    
    
    class func typeForProduct(_ product: RSKDictionary) -> FIEProductType? {
        
        if product["numeroCuenta"] != nil && product["numeroSubCuenta"] != nil {
            return FIEProductType.account
        } else if product["tarjetaID"] != nil {
            return FIEProductType.card
        }
        TODO() // Handle CDA and loans
        return nil
    }
    
    class func idForProduct(_ product: RSKDictionary) -> String? {
        
        guard let type = typeForProduct(product) else { return nil }
        
        switch type {
        case .account:
            return accountID(product)
        case .card:
            return cardID(product)
        }
    }
    
    class func showString(_ product: FIEDictionary?, key: String) -> String? {
        if let string = product?[key] as? String {
            return string
        } else if let int = product?[key] as? Int {
            return String(int)
        }
        return nil
    }
    
    class func showAmount(_ product: FIEDictionary?, key: String, usingCurrencyFromProduct currencyProduct: FIEDictionary? = nil, showCurrency: Bool = true) -> String {
        
        guard let product = product else {
            return ""
        }
        
        guard let value = product[key] as? NSNumber else {
            return ""
        }
        let amount = FIEAmount(decimal: value.decimalValue)
        
        
        var currencyType: FIECurrencyType!
        var currencySymbol: String!
        if let currencyProduct = currencyProduct {
            currencyType = currencyTypeForProduct(currencyProduct)
            currencySymbol = currencySymbolForProduct(currencyProduct)
        } else {
            currencyType = currencyTypeForProduct(product)
            currencySymbol = currencySymbolForProduct(product)
        }
        
        return amount.fie_formatThousandsWithCurrency(currencyType, currencySymbol: currencySymbol)
    }
    
    // ACCOUNT
    
    class func createAccountFromAccountNumber(_ accountNumber: String?) -> FIEDictionary? {
        TODO()
        guard let accountNumber = accountNumber else { return nil }
        
        return [
            "id"                : accountNumber as AnyObject,
            "numeroCuenta"      : 4324234 as AnyObject,
            "numeroSubCuenta"   : 2334442 as AnyObject,
        ]
    }
    
    class func accountID(_ account: RSKDictionary) -> String {
        return account["id"] as! String
    }
    
    class func accountNumber(_ account: RSKDictionary) -> String {
        let num = account["numeroCuenta"] as! Int
        let subNum = account["numeroSubCuenta"] as! Int
        return "\(num)\(PRODUCT_NUMBER_SEPARATOR)\(subNum)"
    }
    
    class func isAccountTransactional(_ account: RSKDictionary) -> Bool {
        return account["estado"] as! String == "0"
    }
    
    class func accountPersonalizedName(_ account: FIEDictionary) -> String? {
        return account["denominacion"] as? String
    }
    
    class func accountNameAndNumber(_ account: FIEDictionary) -> String {
        let number = accountNumber(account)
        if let personalizedName = accountPersonalizedName(account) , personalizedName.rsk_containsValue {
            return "\(personalizedName) \(number)"
        } else {
            return "\(ACCOUNT_DEFAULT_NAME)\(PRODUCT_NAME_NUMBER_SEPARATOR)\(number)"
        }
    }
    
    class func accountNameNumberAndAmount(_ account: FIEDictionary) -> String {
        return accountNameAndNumber(account) + " (" + accountAvailableAmountPretty(account) + ")"
    }
    
    class func accountAvailableAmountPretty(_ account: RSKDictionary) -> String {
        return showAmount(account, key: "saldoDisponible")
    }
    
    class func accountAvailableAmount(_ account: RSKDictionary) -> FIEAmount {
        return FIEAmount(decimal: (account["saldoDisponible"] as! NSNumber).decimalValue)
    }
    
    class func accountStatePretty(_ account: RSKDictionary) -> String {
        return account["descripcionEstado"] as! String
    }
    
    
    // ACCOUNT MOVEMENT
    class func accountMovementDescription(_ movement: FIEDictionary) -> String {
        return movement["descripcion"] as! String
    }
    
    class func accountMovementDate(_ movement: FIEDictionary) -> String {
        return Date.rsk_dateForTimestamp(movement["fechaTransaccion"] as! TimeInterval)
    }
    
    class func accountMovementAmountPretty(_ movement: FIEDictionary, account: FIEDictionary) -> String {
        return showAmount(movement, key: "monto", usingCurrencyFromProduct: account)
    }
    
    class func accountMovementBalancePretty(_ movement: FIEDictionary, account: FIEDictionary) -> String {
        return showAmount(movement, key: "saldo", usingCurrencyFromProduct: account)
    }
    
    class func isAccountMovementACredit(_ movement: FIEDictionary) -> Bool {
        if let info = movement["infoAdicional"] as? FIEDictionary, let movementType = info["DH"] as? String {
            if movementType == "Cr" {
                return true
            }
        }
        return false
    }
    
    // CARD
    
    class func cardID(_ card: RSKDictionary) -> String {
        return card["tarjetaID"] as! String
    }
    
    class func cardNumberMasked(_ card: RSKDictionary) -> String {
        return "*" + (card["nroTarjeta"] as! String)
    }
    
    class func cardPersonalizedName(_ card: FIEDictionary) -> String? {
        return card["denominacion"] as? String
    }
    
    class func cardNameAndNumber(_ card: FIEDictionary) -> String {
        let number = cardNumberMasked(card)
        if let personalizedName = cardPersonalizedName(card) , personalizedName.rsk_containsValue {
            return "\(personalizedName)\(PRODUCT_NAME_NUMBER_SEPARATOR)\(number)"
        } else {
            return "\(CARD_DEFAULT_NAME)\(PRODUCT_NAME_NUMBER_SEPARATOR)\(number)"
        }
    }
    
    class func cardAvailableAmount(_ card: RSKDictionary) -> String {
        return showAmount(card, key: "montoDisponible")
    }
    
    class func cardNextDueDate(_ card: RSKDictionary) -> String {
        if let timestamp = card["proximoVencimiento"] as? TimeInterval {
            return Date.rsk_dateForTimestamp(timestamp)
        }
        return "No Disponible"
    }
    
    class func cardExpiryDate(_ card: RSKDictionary) -> String {
        let timestamp = card["vencimiento"] as! TimeInterval
        return Date.rsk_dateForTimestamp(timestamp)
    }
    
    class func cardTotalDebtAmount(_ card: FIEDictionary) -> FIEAmount {
        return FIEAmount(decimal:  (card["deudaTotal"] as! NSNumber).decimalValue)
    }
    
    class func cardInstallmentAmount(_ card: FIEDictionary) -> FIEAmount {
        return FIEAmount(decimal:  (card["pagoMinimo"] as! NSNumber).decimalValue)
    }
    
    // CARD MOVEMENT
    
    class func cardMovementDescription(_ movement: FIEDictionary) -> String {
        return movement["descripcion"] as! String
    }
    
    class func cardMovementTimeinterval(_ movement: FIEDictionary) -> TimeInterval? {
        if let processTimestamp = movement["fechaProceso"] as? TimeInterval {
            return processTimestamp
        } else if let operationTimestamp = movement["fechaOperacion"] as? TimeInterval {
            return operationTimestamp
        }
        return nil
    }
    
    class func cardMovementDate(_ movement: FIEDictionary) -> String {
        if let interval = cardMovementTimeinterval(movement) {
            return Date.rsk_dateForTimestamp(interval)
        }
        return ""
    }
    
    class func cardMovementAmount(_ movement: FIEDictionary, card: FIEDictionary) -> String {
        return showAmount(movement, key: "importe", usingCurrencyFromProduct: card)
    }
    
    class func isCardMovementAPayment(_ movement: FIEDictionary) -> Bool {
        let amount = FIEAmount(decimal: (movement["importe"] as! NSNumber).decimalValue)
        return amount.fie_isGreaterThanZero() == false
    }
    
    // LOAN
    
    class func loanID(_ loan: RSKDictionary) -> String {
        return loan["prestamoId"] as! String
    }
    
    class func loanNumber(_ loan: FIEDictionary) -> String {
        return String(loan["numOperacion"] as! Int)
    }
    
    class func loanPersonalizedName(_ loan: FIEDictionary) -> String? {
        return loan["denominacion"] as? String
    }
    
    class func loanNameAndNumber(_ loan: FIEDictionary) -> String {
        let number = loanNumber(loan)
        if let personalizedName = loanPersonalizedName(loan) , personalizedName.rsk_containsValue {
            return "\(personalizedName)\(PRODUCT_NAME_NUMBER_SEPARATOR)\(number)"
        } else {
            return "\(LOAN_DEFAULT_NAME)\(PRODUCT_NAME_NUMBER_SEPARATOR)\(number)"
        }
    }
    
    class func loanNextInstallmentAmountPretty(_ loan: FIEDictionary) -> String {
        return FIEUtility.showAmount(loan, key: "proximoMontoVto")
        //        TODO()
        //        var mutableLoan = loan
        //        mutableLoan["proximoMontoVto"] = Double(loan["proximoMontoVto"] as! String)
        //        return showAmount(mutableLoan, key: "proximoMontoVto")
    }
    
    class func loanNextInstallmentDate(_ loan: FIEDictionary) -> String {
        if let timestamp = loan["proximoVencimiento"] as? TimeInterval {
            return Date.rsk_dateForTimestamp(timestamp)
        }
        return "No Disponible"
    }
    
    class func loanTotalDebtAmount(_ loan: FIEDictionary) -> FIEAmount {
        return FIEAmount(decimal: (loan["deudaTotal"] as! NSNumber).decimalValue)
    }
    
    class func loanTotalExpiredDebtAmount(_ loan: FIEDictionary) -> FIEAmount {
        return FIEAmount(decimal: (loan["deudaExpirada"] as! NSNumber).decimalValue)
    }
    
    class func loanInstallmentAmount(_ loan: FIEDictionary) -> FIEAmount {
        //        TODO()
        //        var mutableLoan = loan
        //        mutableLoan["proximoMontoVto"] = Double(loan["proximoMontoVto"] as! String)
        return FIEAmount(decimal: (loan["proximoMontoVto"] as! NSNumber).decimalValue)
    }
    
    
    // LOAN INSTALLMENT
    
    class func loanInstallmentNumber(_ installment: FIEDictionary) -> String {
        return String(installment["numeroCuota"] as! Int)
    }
    
    class func loanInstallmentExpiryDatePretty(_ installment: FIEDictionary) -> String {
        return Date.rsk_dateForTimestamp(installment["fechaVencimiento"] as! TimeInterval)
    }
    
    class func loanInstallmentPaidDatePretty(_ installment: FIEDictionary) -> String? {
        if let timestamp = installment["fechaPago"] as? TimeInterval {
            return Date.rsk_dateForTimestamp(timestamp)
        }
        return nil
    }
    
    class func loanInstallmentAmountPretty(_ installment: FIEDictionary, loan: FIEDictionary) -> String {
        return FIEUtility.showAmount(installment, key: "montoCuota", usingCurrencyFromProduct: loan)
    }
    
    class func loanInstallmentPaidAmountPretty(_ installment: FIEDictionary, loan: FIEDictionary) -> String {
        return FIEUtility.showAmount(installment, key: "montoPago", usingCurrencyFromProduct: loan)
    }
    
    class func isLoanInstallmentPaid(_ installment: FIEDictionary) -> Bool {
        
        if let state = installment["estado"] as? String {
            if state == "Total" {
                return true
            }
        }
        
        return false
    }
    
    
    
    // CDA
    class func cdaID(_ cda: FIEDictionary) -> String {
        return cda["cdaId"] as! String
    }
    
    class func cdaAccountNumber(_ cda: FIEDictionary) -> String {
        let num = cda["numeroCuenta"] as! Int
        let subNum = cda["numeroSubCuenta"] as! Int
        return "\(num)\(PRODUCT_NUMBER_SEPARATOR)\(subNum)"
    }
    
    
    class func cdaNumber(_ cda: FIEDictionary) -> String {
        return String(cda["numOperacion"] as! Int)
    }
    
    class func cdaPersonalizedName(_ cda: FIEDictionary) -> String? {
        return cda["denominacion"] as? String
    }
    
    class func cdaNameAndNumber(_ cda: FIEDictionary) -> String {
        let number = cdaNumber(cda)
        if let personalizedName = cdaPersonalizedName(cda) , personalizedName.rsk_containsValue {
            return "\(personalizedName)\(PRODUCT_NAME_NUMBER_SEPARATOR)\(number)"
        } else {
            return "\(CDA_DEFAULT_NAME)\(PRODUCT_NAME_NUMBER_SEPARATOR)\(number)"
        }
    }
    
    class func cdaNextCouponDate(_ cda: FIEDictionary) -> String {
        return Date.rsk_dateForTimestamp(cda["fecVencimiento"] as! TimeInterval)
    }
    
    class func cdaCapitalAmountPretty(_ cda: RSKDictionary) -> String {
        return showAmount(cda, key: "capital")
    }
    
    // CDA COUPON
    
    //    class func cdaCouponCapitalAmountPretty(coupon: FIEDictionary, cda: FIEDictionary) -> String {
    //        return FIEUtility.showAmount(coupon, key: "totalCapital", usingCurrencyFromProduct: cda)
    //    }
    
    class func isCDACouponPaid(_ coupon: FIEDictionary) -> Bool {
        
        if let state = coupon["estado"] as? String {
            if state == "Cobrado" {
                return true
            }
            // The other value that "state" can have is Pendiente.
        }
        
        return false
    }
    
}

extension UIView {
    
    func fie_addBorder() {
        layer.borderColor = FIEConstants.Colors.Border.cgColor
        layer.borderWidth = FIEUtility.onePixelWidth()
        layer.cornerRadius = 5
        layer.masksToBounds = true
    }
    
    func fie_addSelectedBorder() {
        layer.borderColor = FIEConstants.Colors.BorderSelected.cgColor
        layer.borderWidth = FIEUtility.onePixelWidth()
        layer.cornerRadius = 5
        layer.masksToBounds = true
    }
    
    func fie_rotateUp(_ animated: Bool = true) {
        UIView.animate(withDuration: animated ? 0.4 : 0, delay: 0, options: UIViewAnimationOptions(), animations: {
            let radians = CGFloat(M_PI)
            self.transform = CGAffineTransform(rotationAngle: radians)
            }, completion: nil)
    }
    
    func fie_rotateDown(_ animated: Bool = true) {
        UIView.animate(withDuration: animated ? 0.4 : 0, delay: 0, options: UIViewAnimationOptions(), animations: {
            let radians = CGFloat(-2 * M_PI)
            self.transform = CGAffineTransform(rotationAngle: radians)
            }, completion: nil)
    }
    
    func fie_zoomAnimate() {
        UIView.animate(withDuration: 0.1, animations: {
            self.transform = self.transform.scaledBy(x: 1.1, y: 1.1)
            }, completion: { finished in
                UIView.animate(withDuration: 0.1, animations: {
                    self.transform = self.transform.scaledBy(x: 0.9, y: 0.9)
                    }, completion: { finished in
                        UIView.animate(withDuration: 0.1, animations: {
                            self.transform = CGAffineTransform.identity
                        }) 
                })
        })
    }
    
    func fie_zoomInAnimate() {
        self.transform = self.transform.scaledBy(x: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.3, animations: {
            self.transform = self.transform.scaledBy(x: 1.1, y: 1.1)
            }, completion: { finished in
                UIView.animate(withDuration: 0.1, animations: {
                    self.transform = self.transform.scaledBy(x: 0.9, y: 0.9)
                    }, completion: { finished in
                        UIView.animate(withDuration: 0.1, animations: {
                            self.transform = CGAffineTransform.identity
                        }) 
                })
        })
    }
    
    //    func shareAsPDFFromViewController(viewController: UIViewController) {
    //        do {
    //            print(NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first!)
    //            let data = try PDFGenerator.generate([self])
    //
    //            var items = RSKArray()
    //            items.append([
    //                "TestPDF": data,
    //            ])
    //            let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)
    //            viewController.presentViewController(activityViewController, animated: true, completion: nil)
    //        } catch (let error) {
    //            print(error)
    //        }
    ////        generatePDF()
    ////        let path = NSBundle.mainBundle().pathForResource("sample1", ofType: "pdf")
    //////        let activityViewController = UIActivityViewController(activityItems: [["Test": NSURL(fileURLWithPath: path!)]], applicationActivities: nil)
    //////        viewController.presentViewController(activityViewController, animated: true, completion: nil)
    //////        do {
    //////            let data = try PDFGenerator.generate([self])
    //////            let activityViewController = UIActivityViewController(activityItems: [["Test": data]], applicationActivities: nil)
    //////            viewController.presentViewController(activityViewController, animated: true, completion: nil)
    //////        } catch (let error) {
    //////            print(error)
    //////        }
    //    }
    //
    //    func generatePDF() {
    //
    //
    //        let dst = NSHomeDirectory().stringByAppendingString("/sample1.pdf")
    //        // outputs as NSData
    //        do {
    //            let data = try PDFGenerator.generate([self])
    //            data.writeToFile(dst, atomically: true)
    //        } catch (let error) {
    //            print(error)
    //        }
    //
    //        // writes to Disk directly.
    //        do {
    //            try PDFGenerator.generate([self], outputPath: dst)
    //        } catch (let error) {
    //            print(error)
    //        }
    //    }
    
}
extension UITextField {
    
    func fie_addDoneButton(_ callback: ((Void) -> Void)? = nil) {
        let toolbar = UIToolbar.rsk_newAutolayout()
        toolbar.tintColor = FIEConstants.Colors.TextDark
        toolbar.backgroundColor = UIColor.white
        let closeBarButtonItem = RSKBarButtonItem(title: "Listo", onClick: { _ in
            self.resignFirstResponder()
            callback?()
        })
        let spaceBarButtonItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.setItems([spaceBarButtonItem, closeBarButtonItem], animated: false)
        let toolbarWrapperView = UIView()
        toolbarWrapperView.addSubview(toolbar)
        toolbar.rsk_fillParentView(0)
        self.inputAccessoryView = toolbar
        self.inputAccessoryView!.backgroundColor = UIColor.white
    }
}

extension UITextView {
    
    func fie_addDoneButton(_ callback: ((Void) -> Void)? = nil) {
        let toolbar = UIToolbar.rsk_newAutolayout()
        toolbar.tintColor = FIEConstants.Colors.TextDark
        toolbar.backgroundColor = UIColor.white
        let closeBarButtonItem = RSKBarButtonItem(title: "Listo", onClick: { _ in
            self.resignFirstResponder()
            callback?()
        })
        let spaceBarButtonItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.setItems([spaceBarButtonItem, closeBarButtonItem], animated: false)
        let toolbarWrapperView = UIView()
        toolbarWrapperView.addSubview(toolbar)
        toolbar.rsk_fillParentView(0)
        self.inputAccessoryView = toolbar
        self.inputAccessoryView!.backgroundColor = UIColor.white
    }
}
