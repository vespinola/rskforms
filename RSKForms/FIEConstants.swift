//
//  FIEConstants.swift
//  Fielco-iOS
//
//  Created by Paul Von Schrottky on 4/13/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

struct FIEConstants {
    
    
    struct Auth {
        static let SHARED_SECRET = "eM3g2h8vKmY_QDudIZ9uzoNYxA6rQ9_ZNxn-w5JrG5QGvWuycU_-V0JmqYNGq8jGgDwWvjfX9i2TLfbeGPltu770UP-c-JyPvXWOym5wST4L4hPJGJAg8i8xcA5wYA=="
#if APPSTORE
        static let URL = "https://siempre.elcomercio.com.py/fielco-api/"
        static let SIMPLE_AND_FAST_URL = "https://siempre.elcomercio.com.py/simplefast/pago"
        static let LOG_REQUESTS = false
#else
        static let URL = "https://phoebe.roshka.com/fielco-api/"
        static let SIMPLE_AND_FAST_URL = "https://phoebe.roshka.com/simplefast/opciones"
        static let LOG_REQUESTS = true
#endif
        static let CLIENT_TYPE = "IOS"
        static let CLIENT_VERSION = "1.0.0"
        static let AUTH_TYPE = "algo"
        static let AUTH_VERSION = "1.0.0"
    }
    
    
    struct Colors {
        static let Border = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
        static let BorderSelected = UIColor(red: 200/255, green: 200/255, blue: 200/255, alpha: 1)
        static let Principal = Blue
        static let Blue = UIColor(red: 0/255.0, green: 55/255.0, blue: 139/255.0, alpha: 1)
        static let Red = UIColor(red: 226/255.0, green: 0/255.0, blue: 26/255.0, alpha: 1)
        static let Orange = UIColor(red: 235/255.0, green: 105/255.0, blue: 11/255.0, alpha: 1)
        static let Yellow = UIColor(red: 255/255.0, green: 221/255.0, blue: 0/255.0, alpha: 1)
        static let Black = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1)
        static let Light = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
        static let GreyLight = UIColor(red: 190/255.0, green: 190/255.0, blue: 190/255.0, alpha: 1)
        static let GreyMedium = UIColor(red: 154/255.0, green: 154/255.0, blue: 154/255.0, alpha: 1)
        static let Grey = UIColor(red: 121/255.0, green: 121/255.0, blue: 121/255.0, alpha: 1)
        static let Negative = Red
        static let Positive = UIColor(red: 51/255.0, green: 153/255.0, blue: 51/255.0, alpha: 1)
        static let BlueSecondary = UIColor(red: 51/255.0, green: 95/255.0, blue: 162/255.0, alpha: 1)
        static let Foreground = UIColor.white
        static let Background = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        static let TextLight = UIColor.white
        static let Text = UIColor.gray
        static let TextDark = UIColor.darkGray
        static let BarIcons = Light
        static let Placeholder = UIColor(red: 199/255, green: 199/255, blue: 205/255, alpha: 1.0)
    }
    
    struct Config {
        static let PARAGUAY_ID = 600
        static let NO_COUNTRY_ID = 0
        static let CLIENT_TYPE = "IOS"
        static let CLIENT_VERSION = "1.0.0"
        static let SHARED_SECRET = "todo"
        static let DEVICE_OS = "IOS"
        static let DEVICE_BRAND = "APPLE"
        static let GOOGLE_MAPS_API_KEY = "todo"
        static let LOG_NETWORK_REQUESTS = true
        static let SHOW_FAKE = false
        static let ENABLE_CACHE = true
        static let ERROR_MAP = RSKUtils.rsk_dataFromJSONFile("error_map")!.rsk_JSONToDictionary()!
        static let DOC_NO_LENGTH = 20
        static let PASSWORD_LENGTH = 10
        static let TX_PIN_LENGTH = 6
        static let STANDARD_FIELD_LENGTH = 20
        static let DEFAULT_CURRENCY_TYPE = FIECurrencyType.pyg
        static let TERMS_AND_CONDITIONS_URL = "https://www.elcomercio.com.py"
        
        /// The Keychain account for this app.
        static let KEYCHAIN_ACCOUNT = "py.com.elcomercio.app.xyz"
        
        /// The Keychain service for this app.
        static let KEYCHAIN_SERVICE = "py.com.elcomercio.app"
        
        
        static let TOUCH_ID_WARNING = "Solo necesitarás tocar el lector de huellas de tu teléfono para acceder al APP, una vez asocies tu huella con tus credenciales de acceso (documento de identidad y contraseña).\n\nNo te recomendamos utilizar el acceso mediante huellas, si compartes tu teléfono con otras personas que puedan registrar sus huellas en el dispositivo.\n\nDesde esta pantalla, puedes activar el acceso mediante huella digital en cualquier momento."
    }
    
    struct UserDefaultKeys {

        static let PASSWORD_TOUCH_ID_STATE_KEY = "fie_touch_id_state"
        
        /// The key used to store the user's document number in NSUserDefaults.
        static let USER_DOCUMENT_KEY = "fie_doc_type_key"
        
        /// The key used to store the user's document number country in NSUserDefaults.
        static let USER_DOCUMENT_COUNTRY_KEY = "fie_doc_type_country_key"
        
        /// The app versión is stored in NSUserDefaults to it is accessible to Settings.bundle and can be shown in Settings.
        static let APP_VERSION = "fie_app_version"
    }
    
    struct Notifications {
        static let NetworkRequestStarted = "NotificationNetworkRequestStarted"
        static let NetworkRequestEnded = "NotificationNetworkRequestEnded"
        static let ShowAlert = "NotificationShowAlert"
        static let InputOnFocus = "InputOnFocus"
        static let InputOnBlur = "InputOnBlur"
        static let ScrollViewOnAdd = "ScrollViewOnAdd"
        static let ScrollViewOnRemove = "ScrollViewOnRemove"
        
    }
    
    struct Fonts {
        
        static let SizeVerySmallWeightLight = UIFont.systemFont(ofSize: 10, weight: UIFontWeightLight)
        static let SizeVerySmallWeightRegular = UIFont.systemFont(ofSize: 10, weight: UIFontWeightRegular)
        static let SizeVerySmallWeightMedium = UIFont.systemFont(ofSize: 10, weight: UIFontWeightMedium)
        static let SizeVerySmallWeightHeavy = UIFont.systemFont(ofSize: 10, weight: UIFontWeightHeavy)
        
        static let SizeSmallWeightLight = UIFont.systemFont(ofSize: 13, weight: UIFontWeightLight)
        static let SizeSmallWeightRegular = UIFont.systemFont(ofSize: 13, weight: UIFontWeightRegular)
        static let SizeSmallWeightMedium = UIFont.systemFont(ofSize: 13, weight: UIFontWeightMedium)
        static let SizeSmallWeightBold = UIFont.systemFont(ofSize: 13, weight: UIFontWeightMedium)
        static let SizeSmallWeightHeavy = UIFont.systemFont(ofSize: 13, weight: UIFontWeightHeavy)
        
        static let SizeStandardWeightLight = UIFont.systemFont(ofSize: 15, weight: UIFontWeightLight)
        static let SizeStandardWeightRegular = UIFont.systemFont(ofSize: 15, weight: UIFontWeightRegular)
        static let SizeStandardWeightMedium = UIFont.systemFont(ofSize: 15, weight: UIFontWeightMedium)
        static let SizeStandardWeightHeavy = UIFont.systemFont(ofSize: 15, weight: UIFontWeightHeavy)
        
        static let SizeMediumWeightLight = UIFont.systemFont(ofSize: 17, weight: UIFontWeightLight)
        static let SizeMediumWeightRegular = UIFont.systemFont(ofSize: 17, weight: UIFontWeightRegular)
        static let SizeMediumWeightMedium = UIFont.systemFont(ofSize: 17, weight: UIFontWeightMedium)
        
        static let SizeLargeWeightLight = UIFont.systemFont(ofSize: 21, weight: UIFontWeightLight)
        static let SizeLargeWeightRegular = UIFont.systemFont(ofSize: 21, weight: UIFontWeightRegular)
        static let SizeLargeWeightMedium = UIFont.systemFont(ofSize: 21, weight: UIFontWeightMedium)
        
        static let SizeExtraLargeWeightLight = UIFont.systemFont(ofSize: 25, weight: UIFontWeightLight)
        static let SizeExtraLargeWeightRegular = UIFont.systemFont(ofSize: 25, weight: UIFontWeightRegular)
        static let SizeExtraLargeWeightMedium = UIFont.systemFont(ofSize: 25, weight: UIFontWeightMedium)
        
        
        
        static let Small = UIFont.systemFont(ofSize: 11)
        static let SmallBold = UIFont.boldSystemFont(ofSize: 11)
        static let Medium = UIFont.systemFont(ofSize: 14)
        static let MediumBold = UIFont.boldSystemFont(ofSize: 14)
        static let Large = UIFont.systemFont(ofSize: 17)
        static let LargeBold = UIFont.boldSystemFont(ofSize: 17)
        static let ExtraLarge = UIFont.systemFont(ofSize: 21)
        static let ExtraLargeBold = UIFont.boldSystemFont(ofSize: 21)
    }
    

}
