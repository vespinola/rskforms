//
//  RSKMenuViewController.swift
//  RSKMenu
//
//  Created by Paul Von Schrottky on 9/21/15.
//  Copyright © 2015 Roshka. All rights reserved.
//

import UIKit

enum RSKMenuDevice {
    case pad
    case phone
    
    static func device() -> RSKMenuDevice {
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            return phone
        case .pad, .tv, .carPlay, .unspecified:
            return pad
        }
    }
}

enum RSKMenuState {
    case open
    case closed
}

enum RSKMenuAnimation {
    case slideAcross
    case fadeIn
    case none
}

class RSKMenuViewController: UIViewController, UIGestureRecognizerDelegate {
    
    var menuState = RSKMenuState.closed
    var panGestureRecognizer: UIPanGestureRecognizer!
    var contentContainerView: UIView!
    
    var menuLayoutConstraint: NSLayoutConstraint!
    var menuMainView: UIView!
    var phonePeekWidth: CGFloat = 0 {
        willSet {
            menuLayoutConstraint.constant = newValue
        }
    }
    let padMenuWidth: CGFloat = 300
    
    var closeButton: UIButton?
    let CLOSE_BUTTON_MIN_ALPHA: CGFloat = 0
    let CLOSE_BUTTON_MAX_ALPHA: CGFloat = 0.2
    
    let MENU_VIEW_MIN_SCALE: CGFloat = 0.8
    let MENU_VIEW_MAX_SCALE: CGFloat = 1
    
    // Normally if a screen is already open, selecting it in the menu will not reload it, it will simply be shown.
    // Set this to true to force a reload of the current screeen regardless.
    var reloadCurrentScreenOnSelect = false
    var shouldRecognizeSimultaneousGestures = false
    var enabled: Bool? {
        set {
            self.panGestureRecognizer?.isEnabled = newValue!
        }
        get {
            return self.panGestureRecognizer?.isEnabled
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        menuMainView.layoutIfNeeded()
        menuMainView.translatesAutoresizingMaskIntoConstraints = false
        
        
        contentContainerView = UIView()
        contentContainerView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(contentContainerView)
        
        if RSKMenuDevice.device() != .pad {
            contentContainerView.layer.shadowColor = UIColor.black.cgColor
            contentContainerView.layer.shadowOffset = CGSize(width: 0, height: 0)
            contentContainerView.layer.shadowOpacity = 0.5
        }
        
        
        
        if RSKMenuDevice.device() == .pad {
            menuLayoutConstraint.constant = UIScreen.main.bounds.width - padMenuWidth
            self.contentContainerView.frame = CGRect(x: padMenuWidth, y: 0, width: self.view.frame.width - padMenuWidth, height: self.view.frame.height)
        } else {
            menuLayoutConstraint.constant = phonePeekWidth
            let autolayoutViews = ["contentContainerView" : contentContainerView]
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[contentContainerView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: autolayoutViews))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[contentContainerView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: autolayoutViews))
            
            // Add the gesture recognizer for opening/closing the menu.
            self.panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(RSKMenuViewController.handlePanGesture(_:)))
            panGestureRecognizer.delegate = self
            self.view.addGestureRecognizer(self.panGestureRecognizer!)
            self.panGestureRecognizer.isEnabled = enabled!
        }
    }
    
    func addCloseButton() {
        if closeButton != nil {
            return
        }
//        contentContainerView.layoutIfNeeded()
        closeButton = UIButton(type: .custom)
        closeButton?.backgroundColor = UIColor.black
        closeButton?.alpha = CLOSE_BUTTON_MIN_ALPHA
        contentContainerView.addSubview(closeButton!)
//        closeButton!.frame = contentContainerView.bounds
        let autolayoutViews = ["closeButton" : closeButton!]
        contentContainerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[closeButton]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: autolayoutViews))
        contentContainerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[closeButton]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: autolayoutViews))
        closeButton!.addTarget(self, action: #selector(RSKMenuViewController.closeMenuButton(_:)), for: .touchUpInside)
    }
    
    func removeCloseButton() {
        closeButton?.removeFromSuperview()
        closeButton = nil
    }
    
    func closeMenuButton(_ sender: UIButton) {
        self.closeMenu(true)
    }
    
    // #MARK: - UIGestureRecognizerDelegate
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return shouldRecognizeSimultaneousGestures
    }
    
    func handlePanGesture(_ recognizer: UIPanGestureRecognizer) {
        if self.enabled == false {
            return
        }
        contentContainerView.setNeedsLayout()
        contentContainerView.layoutIfNeeded()
        
        let width = self.contentContainerView.bounds.size.width
        
        switch(recognizer.state) {
            
        case .changed:
            
            // translationInView gives the offset since the gesture started, or since setTranslation is last called.
            // Here we only allow the content to slide if it doesn't go too far left or right.
            let newPosition = contentContainerView.center.x + recognizer.translation(in: contentContainerView).x
            let isNotTooFarLeft = newPosition >= (width / 2)
            let isNotTooFarRight = newPosition <= width + (width / 2) - phonePeekWidth
            if isNotTooFarLeft && isNotTooFarRight {
                contentContainerView.center.x = newPosition
            }
            
            // Zoom the menu in/out with the slide.
            let percentage = (newPosition - width / 2) / (width - phonePeekWidth) // 0 to 1
            let scale = 0.8 + percentage * 0.2
            self.closeButton?.alpha = percentage * 0.2
            menuMainView.transform = CGAffineTransform(scaleX: scale, y: scale)
            
            // This resets the value of translationInView to zero
            recognizer.setTranslation(CGPoint.zero, in: self.contentContainerView)
        case .ended:
            
            // Animate the side panel open or closed based on whether the view has moved more or less than halfway
            // accross the screen.
            //            let hasMovedGreaterThanHalfway = self.contentContainerView.center.x > width
            if recognizer.velocity(in: contentContainerView).x > 0 {
                openMenu(true)
            } else {
                closeMenu(true)
            }
            
        default:
            break
        }
    }
    
    // #MARK: - RSKMenuDelegate
    
    func openMenu(_ animated: Bool) {
        self.view.endEditing(true)
        self.addCloseButton()
        
        UIView.animate(withDuration: animated ? 0.3 : 0, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0, options: UIViewAnimationOptions(), animations: {
            self.closeButton!.alpha = self.CLOSE_BUTTON_MAX_ALPHA
            self.menuMainView.transform = CGAffineTransform(scaleX: self.MENU_VIEW_MAX_SCALE, y: self.MENU_VIEW_MAX_SCALE)
            self.contentContainerView.frame.origin.x = self.contentContainerView.bounds.size.width - CGFloat(self.phonePeekWidth)
            }, completion: { completed in
                self.menuState = .open
                self.closeButton?.alpha = self.CLOSE_BUTTON_MAX_ALPHA
        })
    }
    
    func closeMenu(_ animated: Bool) {
        UIView.animate(withDuration: animated ? 0.3 : 0, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0, options: UIViewAnimationOptions(), animations: {
            self.contentContainerView.frame.origin.x = 0
            self.closeButton?.alpha = self.CLOSE_BUTTON_MIN_ALPHA
            self.menuMainView.transform = CGAffineTransform(scaleX: self.MENU_VIEW_MIN_SCALE, y: self.MENU_VIEW_MIN_SCALE)
            }, completion: { completed in
                self.menuState = .closed
                self.closeButton?.alpha = self.CLOSE_BUTTON_MIN_ALPHA
                self.removeCloseButton()
        })
    }
    
    func toggleMenu(_ animated: Bool) {
        if self.enabled == false {
            return
        }
        if self.menuState == .open {
            closeMenu(animated)
        } else {
            openMenu(animated)
        }
    }
    
    func showViewControllerWithStoryboardID(_ id: String, callback: ((RSKMenuContentViewController) -> Void)? = nil) {
        self.showViewControllerWithStoryboardID(id, animation: .slideAcross, callback: callback)
    }
    
    func showViewControllerWithStoryboardID(_ id: String, animation: RSKMenuAnimation, callback: ((RSKMenuContentViewController) -> Void)? = nil) {
        let newViewController = self.storyboard!.instantiateViewController(withIdentifier: id) as! RSKMenuContentViewController
        self.goToViewController(newViewController, animation: animation, onViewDidLoad: callback)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let newViewController = segue.destination as! RSKMenuContentViewController
        self.goToViewController(newViewController)
    }
    
    func goToViewController(_ viewController: RSKMenuContentViewController, animation: RSKMenuAnimation? = nil, onViewDidLoad: ((RSKMenuContentViewController) -> Void)? = nil) {
        
        viewController.onViewDidLoad = onViewDidLoad
        let newNavigationViewController = UINavigationController(rootViewController: viewController)
        
        if self.childViewControllers.count == 0 {
            self.addChildViewController(newNavigationViewController)
            newNavigationViewController.view.frame = self.contentContainerView.bounds
            self.contentContainerView.addSubview(newNavigationViewController.view)
            newNavigationViewController.didMove(toParentViewController: self)
        } else {
            let oldViewController = self.childViewControllers[0]
            newNavigationViewController.view.frame = oldViewController.view.frame
            oldViewController.willMove(toParentViewController: nil)
            self.addChildViewController(newNavigationViewController)
            
            let duration: TimeInterval = 0    // Any larger value makes the navigation var animate slowly and jumpily.
            
            self.transition(from: oldViewController, to: newNavigationViewController, duration: duration, options: .transitionCrossDissolve, animations: nil) { completed in
                oldViewController.removeFromParentViewController()
                newNavigationViewController.didMove(toParentViewController: self)
            }
        }
        
        if RSKMenuDevice.device() != .pad {
            self.closeMenu(true)
        }
    }
}


class RSKMenuContentViewController: UIViewController {
    
    enum RSKMenuContentLeftButton {
        case back
        case menu
    }
    
    var showMenuButton = false
    
    var backBarButtonItem: UIBarButtonItem!
    var menuBarButtonItem: UIBarButtonItem!
    
    var onViewDidLoad: ((RSKMenuContentViewController) -> Void)?
    
    
    var menuViewController: RSKMenuViewController? {
        get {
            return self.parent?.parent as? RSKMenuViewController
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.onViewDidLoad?(self)
        
        
        if RSKMenuDevice.device() == .phone && (self.showMenuButton == true || self.navigationController?.viewControllers.count == 1) {
            assert(menuBarButtonItem.image != nil, "'menuBarButtonItem' must have an image when running on Phone")
            self.menuViewController?.phonePeekWidth = menuBarButtonItem.image!.size.width + 40
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if RSKMenuDevice.device() == .pad {
            self.menuViewController?.enabled = false
        } else if RSKMenuDevice.device() == .phone && (self.showMenuButton == true || self.navigationController?.viewControllers.count == 1) {
            self.navigationItem.leftBarButtonItem = self.menuBarButtonItem
            self.menuViewController?.enabled = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationItem.backBarButtonItem = self.backBarButtonItem
    }
}

class RSKStoryboardSegue: UIStoryboardSegue {
    override func perform() {
        
    }
}
