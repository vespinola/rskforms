//
//  RSKHamburger.swift
//  Fielco-iOS
//
//  Created by Roshka on 6/23/16.
//  Copyright © 2016 Roshka. All rights reserved.
//

import UIKit

/**
    Current device.
 
    - Phone: For iPhones.
    - Pad: For anything else.
 */
enum RSKHamburgerDevice {
    case pad, phone
}

/**
    State of the menu.
 
    - Open: For visible menu.
    - Closed: For closed menu.
 */
enum RSKHamburgerState {
    case open, closed
}

/**
    Animation to use when showing screen corresponding to menu option.
 
    - SlideAcross: Default slide-across.
    - FadeIn: Fade-in.
    - None: No animation.
 */
enum RSKHamburgerAnimation {
    case slideAcross, fadeIn, none
}

struct RSKHamburger {
    static let ANIMATION_DURATION: TimeInterval = 0.3
    static let MIN_SCALE: CGFloat = 0.8
    static let MAX_SCALE: CGFloat = 1.0
    static let MIN_ALPHA: CGFloat = 0
    static let MAX_ALPHA: CGFloat = 0.2
}

class RSKHamburgerFooterView: UIView {
    
    // Used for allowing gestures in the gesture view (which is outside of the bounds of the footer).
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        for subview in subviews {
            if let view = subview.hitTest(convert(point, to: subview), with: event) {
                return view
            }
        }
        return super.hitTest(point, with: event)
    }
}

/// A side menu controller.
class RSKHamburgerMenuViewController: UIViewController {
    
    /// The state of the menu.
    var hamburgerState = RSKHamburgerState.closed
    
    /// For Phone devices, the gesture recognizer used to open and close the menu.
    var panGestureRecognizer: UIPanGestureRecognizer?
    
    /// The prinicpal view which contains the menu.
    var menuView: UIView!
    
    /// A view in which to add screens corresponding to menu options.
    var containerView: UIView!
    
    ///
    var containerLeadingLayoutConstraint: NSLayoutConstraint!
    
    ///
    var containerTrailingLayoutConstraint: NSLayoutConstraint!
    
    ///
    var shouldRecognizeSimultaneousGestures = false
    
    /// 
    var closeButton: UIButton?
    
    
    /// 
    var footerView = RSKHamburgerFooterView()
    
    /// Current device.
    var device: RSKHamburgerDevice {
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            return .phone
        case .pad, .tv, .carPlay, .unspecified:
            return .pad
        }
    }
    
    /// True if the menu is enabled, false otherwise.
    var enabled: Bool {
        set {
            panGestureRecognizer?.isEnabled = newValue
        }
        get {
            return panGestureRecognizer?.isEnabled ?? false
        }
    }
    
    ///
    var menuLayoutConstraint: NSLayoutConstraint!
    
    var phonePeekWidth: CGFloat = 0 {
        willSet {
            menuLayoutConstraint.constant = newValue
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        menuView.translatesAutoresizingMaskIntoConstraints = false
        
        containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(containerView)
        
        footerView = RSKHamburgerFooterView()
        footerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(footerView)
        
        
        
        if device == .phone {
            
            // The container view will hold all the screens corresponding to menu items.
            let views = [
                "containerView" : containerView,
                "footerView" : footerView,
            ]
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[containerView][footerView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
            view.addConstraint(NSLayoutConstraint(item: containerView, attribute: .left, relatedBy: .equal, toItem: footerView, attribute: .left, multiplier: 1, constant: 0))
            view.addConstraint(NSLayoutConstraint(item: containerView, attribute: .right, relatedBy: .equal, toItem: footerView, attribute: .right, multiplier: 1, constant: 0))
            containerLeadingLayoutConstraint = NSLayoutConstraint(item: view, attribute: .left, relatedBy: .equal, toItem: containerView, attribute: .left, multiplier: 1, constant: 0)
            containerTrailingLayoutConstraint = NSLayoutConstraint(item: view, attribute: .right, relatedBy: .equal, toItem: containerView, attribute: .right, multiplier: 1, constant: 0)
            view.addConstraints([containerLeadingLayoutConstraint, containerTrailingLayoutConstraint])
            
            containerView.setNeedsLayout()
            containerView.layoutIfNeeded()
            print(containerView)
            
            panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture))
            panGestureRecognizer!.delegate = self
            view.addGestureRecognizer(panGestureRecognizer!)
            panGestureRecognizer!.isEnabled = enabled
        } else if device == .pad {
            menuLayoutConstraint.constant = UIScreen.main.bounds.width - 300
            containerView.frame = CGRect(x: 300, y: 0, width: view.frame.width - 300, height: view.frame.height)
        }
    }
    
    func addCloseButton() {
        guard closeButton == nil else { return }
        
        closeButton = UIButton(type: .custom)
        closeButton?.translatesAutoresizingMaskIntoConstraints = false
        closeButton!.backgroundColor = UIColor.black
        closeButton!.alpha = RSKHamburger.MIN_ALPHA
        containerView.addSubview(closeButton!)
        let views = ["closeButton" : closeButton!]
        containerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[closeButton]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        containerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[closeButton]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        closeButton!.addTarget(self, action: #selector(closeMenuButton), for: .touchUpInside)
        closeButton?.setNeedsLayout()
        closeButton?.layoutIfNeeded()
    }
    
    func removeCloseButton() {
        closeButton?.removeFromSuperview()
        closeButton = nil
    }
    
    func closeMenuButton(_ sender: UIButton) {
        hideMenu(true)
    }
    
    func toggleMenu(_ animated: Bool) {
        guard enabled == true else { return }
        if hamburgerState == .open {
            hideMenu(animated)
        } else {
            showMenu(animated)
        }
    }
    
    func showMenu(_ animated: Bool) {
        self.addCloseButton()
        animateMenuToState(.open, animated: animated)
    }
    
    func hideMenu(_ animated: Bool) {
        animateMenuToState(.closed, animated: animated)
    }
    
    func animateMenuToState(_ newState: RSKHamburgerState, animated: Bool) {
        view.endEditing(true)
        
        let animationDuration = animated ? RSKHamburger.ANIMATION_DURATION : 0
        var xPosition = containerView.bounds.size.width - phonePeekWidth
        
        var zoomAffineTransform = CGAffineTransform(scaleX: RSKHamburger.MAX_SCALE, y: RSKHamburger.MAX_SCALE)
        var closeButtonAlpha = RSKHamburger.MAX_ALPHA
        
        if newState == .closed {
            xPosition = 0
            zoomAffineTransform = CGAffineTransform(scaleX: RSKHamburger.MIN_SCALE, y: RSKHamburger.MIN_SCALE)
            closeButtonAlpha = RSKHamburger.MIN_ALPHA
        }
        
        containerLeadingLayoutConstraint.constant = -xPosition
        containerTrailingLayoutConstraint.constant = -xPosition
        
        UIView.animate(withDuration: animationDuration, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0, options: UIViewAnimationOptions(), animations: {
            self.menuView.transform = zoomAffineTransform
            self.closeButton?.alpha = closeButtonAlpha
            self.view.layoutIfNeeded()
        }, completion: { completed in
            self.hamburgerState = newState
            if newState == .closed {
                self.removeCloseButton()
            }
        })
    }
    
    func handlePanGesture(_ panGestureRecognizer: UIPanGestureRecognizer) {
        guard enabled else { return }
        
        containerView.setNeedsLayout()
        containerView.layoutIfNeeded()
        
        let width = containerView.bounds.size.width
        
        switch(panGestureRecognizer.state) {
            
        case .changed:
            
            // translationInView gives the offset since the gesture started, or since setTranslation is last called.
            // Here we only allow the content to slide if it doesn't go too far left or right.
            let xTranslation = panGestureRecognizer.translation(in: containerView).x
            let isNotTooFarLeft = (containerView.center.x + xTranslation) >= (width / 2)
            let isNotTooFarRight = (containerView.center.x + xTranslation) <= width + (width / 2) - phonePeekWidth
            if isNotTooFarLeft && isNotTooFarRight {
                containerLeadingLayoutConstraint.constant -= xTranslation
                containerTrailingLayoutConstraint.constant -= xTranslation
            }
            
            // Zoom the menu in/out with the slide.
            let percentage = containerView.frame.origin.x / (width - phonePeekWidth) // 0 to 1
            let scale = 0.8 + percentage * 0.2
            self.closeButton?.alpha = percentage * 0.2
            menuView.transform = CGAffineTransform(scaleX: scale, y: scale)

            // This resets the value of translationInView to zero
            panGestureRecognizer.setTranslation(CGPoint.zero, in: containerView)
        case .ended:
            
            // Animate the side panel open or closed based on whether the view has been dragged left or right.
            if panGestureRecognizer.velocity(in: containerView).x > 0 {
                showMenu(true)
            } else {
                hideMenu(true)
            }
            
        default:
            break
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let newViewController = segue.destination as? RSKHamburgerViewController {
            self.showViewController(newViewController)
        }
    }
    
    func showViewController(_ viewController: RSKHamburgerViewController, animation: RSKHamburgerAnimation? = nil, onViewDidLoad: ((RSKHamburgerViewController) -> Void)? = nil) {
        
        viewController.onViewDidLoad = onViewDidLoad
        let newNavigationViewController = UINavigationController(rootViewController: viewController)
        
        if childViewControllers.count == 0 {
       
            self.addChildViewController(newNavigationViewController)
            newNavigationViewController.view.frame = containerView.bounds
            self.containerView.addSubview(newNavigationViewController.view)
            newNavigationViewController.didMove(toParentViewController: self)
            
        } else {
            
            let oldViewController = self.childViewControllers[0]
            newNavigationViewController.view.frame = oldViewController.view.frame
            oldViewController.willMove(toParentViewController: nil)
            self.addChildViewController(newNavigationViewController)
            self.transition(from: oldViewController, to: newNavigationViewController, duration: 0, options: .transitionCrossDissolve, animations: nil) { completed in
                oldViewController.removeFromParentViewController()
                newNavigationViewController.didMove(toParentViewController: self)
            }
        }
        
        if device != .pad {
            self.hideMenu(true)
        }
    }
}

extension RSKHamburgerMenuViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return shouldRecognizeSimultaneousGestures
    }
}



class RSKHamburgerViewController: UIViewController {
    
    enum RSKHamburgerButton {
        case back
        case menu
    }
    
    var showMenuButton = false
    
    var backBarButtonItem: UIBarButtonItem!
    var menuBarButtonItem: UIBarButtonItem!
    
    /// A closure called when a RSKHamburgerViewController is loaded.
    var onViewDidLoad: ((RSKHamburgerViewController) -> Void)?
    
    /// A reference to the menu.
    var menuViewController: RSKHamburgerMenuViewController? {
        get {
            return self.parent?.parent as? RSKHamburgerMenuViewController
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.onViewDidLoad?(self)
        
        if menuViewController?.device == .phone && (showMenuButton == true || navigationController?.viewControllers.count == 1) {
            assert(menuBarButtonItem.image != nil, "'menuBarButtonItem' must have an image when running on Phone")
            menuViewController?.phonePeekWidth = menuBarButtonItem.image!.size.width + 40
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if menuViewController?.device == .pad {
            menuViewController?.enabled = false
        } else if menuViewController?.device == .phone && (showMenuButton == true || navigationController?.viewControllers.count == 1) {
            navigationItem.leftBarButtonItem = menuBarButtonItem
            menuViewController?.enabled = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationItem.backBarButtonItem = self.backBarButtonItem
    }
}

class RSKHamburgerSegue: UIStoryboardSegue {
    override func perform() {
        /// Do nothing, we override prepareForSegue.
    }
}
