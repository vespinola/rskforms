//
//  RSKUtils.swift
//  biggie-ios
//
//  Created by Paul Von Schrottky on 7/1/15.
//  Copyright (c) 2015 roshka.com. All rights reserved.
//

import UIKit

typealias RSKDictionary = Dictionary<String, AnyObject>
typealias RSKArray = Array<RSKDictionary>

func rsk_delay(_ delay:Double, closure:@escaping () -> ()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}



class RSKUtils: NSObject {
    
    class func matchesForRegexInText(_ regex: String!, text: String!) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [])
            let nsString = text as NSString
            let results = regex.matches(in: text,
                options: [], range: NSMakeRange(0, nsString.length))
            return results.map { nsString.substring(with: $0.range)}
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    class func containsMatchForRegex(_ regex: String!, inText text: String!) -> Bool {
        return RSKUtils.matchesForRegexInText(regex, text: text).count > 0
    }

    
    class func topViewController() -> UIViewController {
        var topController = UIApplication.shared.keyWindow?.rootViewController
        
        while (topController?.presentedViewController != nil) {
            topController = topController?.presentedViewController
        }
        
        return topController!
    }
    
    // Note: Set "View controller-based status bar appearance" to NO in Info.plist for this to work.
    class func rsk_statusBarDarkContent() {
        UIApplication.shared.setStatusBarStyle(.default, animated: false)
    }
    
    // Note: Set "View controller-based status bar appearance" to NO in Info.plist for this to work.
    class func rsk_statusBarLightContent() {
        UIApplication.shared.setStatusBarStyle(.lightContent, animated: false)
    }
    
    class func rsk_dataFromJSONFile(_ named: String) -> Data? {
        if let path = Bundle.main.path(forResource: named, ofType: "json") {
            do  {
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: NSData.ReadingOptions.mappedIfSafe)
                return jsonData
            } catch let caught as NSError {
                
            }
        }
        
        return nil
    }
    
    class func rsk_objectFromJSONFile(_ named: String) -> AnyObject? {
        let data = RSKUtils.rsk_dataFromJSONFile(named)
        let object = data?.rsk_JSONToObject()
        return object
    }
    
    class func rsk_valueOrEmpty(_ optionalString: String?) -> String {
        if optionalString == nil {
            return ""
        } else {
            return optionalString!
        }
    }
    

    
//    class func firstDayOfMonth(month: Int) -> String {
//        
//        let calender = NSCalendar.currentCalendar()
//        var startDateComponents = calender.components([NSCalendarUnit.Year, NSCalendarUnit.Month, NSCalendarUnit.Day, NSCalendarUnit.Hour, NSCalendarUnit.Minute, NSCalendarUnit.Second], fromDate: NSDate())
//        startDateComponents.year = 0
//        startDateComponents.month = month
//        startDateComponents.day = -startDateComponents.day + 1
//        startDateComponents.hour = -startDateComponents.hour
//        startDateComponents.minute = -startDateComponents.minute
//        startDateComponents.second = -startDateComponents.second
//        
//        let startDate = calender.dateByAddingComponents(startDateComponents, toDate: NSDate(), options: NSCalendarOptions(rawValue: 0))
//        let startDateString = startDate!.rsk_ISO8601()
//        return startDateString
//    }
    
    
    class func firstDayOfMonth(_ month: Int) -> String {
        
        let currentCalender = Calendar.current
        var currentDateComponents = (currentCalender as NSCalendar).components([.year, .month, .day], from: Date())
        currentDateComponents.day = 1
        currentDateComponents.month = currentDateComponents.month! + month
        let startDateOfMonth = currentCalender.date(from: currentDateComponents)
        let firstDay = startDateOfMonth!.rsk_dayMonthYear()
        return firstDay
    }
    
    class func lastDayOfMonth(_ month: Int) -> String {
        
        let calender = Calendar.current
        var startDateComponents = (calender as NSCalendar).components([.year, .month, .day, .hour, .minute, .second], from: Date())
        startDateComponents.year = 0
        startDateComponents.month = 1 + month
        startDateComponents.day = startDateComponents.day! + 1
        startDateComponents.hour = startDateComponents.hour
        startDateComponents.minute = startDateComponents.minute
        startDateComponents.second = startDateComponents.second! - 1
        
        let startDate = (calender as NSCalendar).date(byAdding: startDateComponents, to: Date(), options: NSCalendar.Options(rawValue: 0))
        let startDateString = startDate!.rsk_dayMonthYear()
        return startDateString
    }
    
    class func firstDayDateOfMonth(_ month: Int) -> Date {
        
        let currentCalender = Calendar.current
        var currentDateComponents = (currentCalender as NSCalendar).components([.year, .month, .day], from: Date())
        currentDateComponents.day = 1
        currentDateComponents.month = currentDateComponents.month! + month
        let startDateOfMonth = currentCalender.date(from: currentDateComponents)
        return startDateOfMonth!
    }
    
    class func lastDayDateOfMonth(_ month: Int) -> Date {
        
        let calender = Calendar.current
        var startDateComponents = (calender as NSCalendar).components([.year, .month, .day, .hour, .minute, .second], from: Date())
        startDateComponents.year = 0
        startDateComponents.month = 1 + month
        startDateComponents.day = startDateComponents.day! + 1
        startDateComponents.hour = startDateComponents.hour
        startDateComponents.minute = startDateComponents.minute
        startDateComponents.second = startDateComponents.second! - 1
        
        let startDate = (calender as NSCalendar).date(byAdding: startDateComponents, to: Date(), options: NSCalendar.Options(rawValue: 0))
        return startDate!
    }
    
    
    class func yearNameForMonth(_ month: Int) -> String {
        
        let currentCalender = Calendar.current
        var currentDateComponents = (currentCalender as NSCalendar).components([.year, .month, .day], from: Date())
        currentDateComponents.month = currentDateComponents.month! + month
        let startDateOfMonth = currentCalender.date(from: currentDateComponents)
        let yearFormatter = DateFormatter()
        yearFormatter.locale = Locale(identifier: "es")
        yearFormatter.dateFormat = "YYYY"
        let yearName = yearFormatter.string(from: startDateOfMonth!).capitalized
        return yearName
    }
    
    class func monthName(_ month: Int) -> String {
        
        let currentCalender = Calendar.current
        var currentDateComponents = (currentCalender as NSCalendar).components([.year, .month, .day], from: Date())
        currentDateComponents.month = currentDateComponents.month! + month
        let startDateOfMonth = currentCalender.date(from: currentDateComponents)
        let monthFormatter = DateFormatter()
        monthFormatter.locale = Locale(identifier: "es")
        monthFormatter.dateFormat = "MMMM"
        let monthName = monthFormatter.string(from: startDateOfMonth!).capitalized
        return monthName
    }
}

extension URL {
    
    func rsk_queryParamsToDictionary() -> [String: String] {
        let URLComponents = Foundation.URLComponents(string: self.absoluteString)!
        
        let params = URLComponents.queryItems!.reduce([String: String]()) { sum, param in
            var item = [String: String]()
            //let key = String(param.name).stringByRemovingPercentEncoding!
            let key = String(param.name)?.removingPercentEncoding
            let value = String(param.value!).removingPercentEncoding
            item[key!] = value!
            return sum + item
        }
        
        return params
    }

}

extension UIButton {
    func rsk_disable() {
        self.isUserInteractionEnabled = false
        self.alpha = 0.5
    }
    func rsk_enable() {
        self.isUserInteractionEnabled = true
        self.alpha = 1
    }
}


extension UINavigationBar {
    
    func rsk_backgroundColor(_ color: UIColor) {
        self.barTintColor = color
        self.isTranslucent = false
    }
    
    class func rsk_backgroundColorAllBars(_ color: UIColor) {
        UINavigationBar.appearance().barTintColor = color
    }
    
    
}

extension UINavigationController {
    
    func rsk_backgroundImage(_ imageNamed: String) {
        let image = UIImage(named: imageNamed)
        let imageView = UIImageView(image: image)
        self.navigationItem.titleView = imageView
    }
    
    func rsk_popViewControllers(_ number: Int) {
        for i in 0..<number {
            self.popViewController(animated: i == number - 1)
        }
    }
    

}

extension Data {
    func rsk_JSONToDictionary() -> RSKDictionary? {
        
        do {
            let dictionary = try JSONSerialization.jsonObject(with: self, options: []) as? RSKDictionary
            return dictionary
        } catch let caught as NSError {
            
        }
        
        return nil
    }
    
    func rsk_JSONToArray() -> RSKArray? {
        
        do {
            let dictionary = try JSONSerialization.jsonObject(with: self, options: []) as? RSKArray
            return dictionary
        } catch let caught as NSError {
            
        }
        
        return nil
    }
    
    func rsk_JSONToObject() -> AnyObject? {
        
        do {
            let dictionary = try JSONSerialization.jsonObject(with: self, options: [])
            return dictionary as AnyObject?
        } catch let caught as NSError {

        }
        return nil
    }
    
    func rsk_JSONDictionaryPrettyPrinted() -> String {
        let object = self.rsk_JSONToDictionary()
        let error: NSError?
        let data: Data?
        do {
            data = try JSONSerialization.data(withJSONObject: object!, options: .prettyPrinted)
        } catch let error1 as NSError {
            error = error1
            data = nil
        }
        return NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
        
    }
    
    func rsk_JSONToString() -> String {
        
        let data = try! JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions(rawValue: 0))
        return NSString(data: data, encoding: String.Encoding.utf8.rawValue) as! String
    }
}

extension Dictionary {
    func rsk_toJSON() -> NSString {
        let JSONData: Data?
        do {
            JSONData = try JSONSerialization.data(withJSONObject: self as AnyObject, options: JSONSerialization.WritingOptions())
        } catch let error1 as NSError {
            JSONData = nil
        }
        let JSONString = NSString(data: JSONData!, encoding: String.Encoding.utf8.rawValue)
        return JSONString!
    }
}

extension String {
    
    var rsk_trimmed: String {
        let whitespaceCharacterSet = CharacterSet.whitespaces
        return self.trimmingCharacters(in: whitespaceCharacterSet)
    }
    
    var rsk_isEmpty: Bool {
        return rsk_trimmed.isEmpty
    }
    
    var rsk_containsValue: Bool {
        return !rsk_isEmpty
    }
    
    func rsk_indexOfCharacter(_ char: Character) -> Int? {
        if let idx = self.characters.index(of: char) {
            return self.characters.distance(from: self.startIndex, to: idx)
        }
        return nil
    }
    
    func rsk_JSONToArray() -> Array<AnyObject> {
        var error: NSError?
        let JSONData = self.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let JSONArray = (try! JSONSerialization.jsonObject(with: JSONData!, options: [])) as! Array<AnyObject>
        return JSONArray
    }
    
    func rsk_JSONToDictionary() -> Dictionary<String, AnyObject> {
        var error: NSError?
        let JSONData = self.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let JSONDictionary = (try! JSONSerialization.jsonObject(with: JSONData!, options: [])) as! Dictionary<String, AnyObject>
        return JSONDictionary
    }
    
    func rsk_replace(_ string:String, replacement:String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
    }
    
    func rsk_removeWhitespace() -> String {
        return self.rsk_replace(" ", replacement: "")
    }
    
    // Searches for all the space-delimited queries in the 'query' string, in the self string.
    // Returns true if all queries were found.
    func rsk_matches(_ searchString: String) -> Bool {
        let searchQueries = searchString.characters.split {$0 == " "}.map { String($0) }
        let matches = searchQueries.filter() {
            return self.lowercased().range(of: $0.lowercased()) != nil
        }
        return matches.count == searchQueries.count
    }
}


extension UIView {
    class func rsk_loadFromNibNamed(_ nibNamed: String, bundle : Bundle? = nil) -> UIView? {
        return UINib(nibName: nibNamed, bundle: bundle).instantiate(withOwner: nil, options: nil)[0] as? UIView
    }
    
//    class func rsk_fromNibNamed(nibNamed: String, bundle : NSBundle? = nil) -> Self {
//        return UINib(nibName: nibNamed, bundle: bundle).instantiateWithOwner(nil, options: nil).first! as! UIView
//    }
    
    
    class func rsk_newAutolayoutInParentView(_ parentView: UIView) -> Self {
        let view = self.rsk_newAutolayout()
        parentView.addSubview(view)
        return view
    }
    
    class func rsk_newAutolayout() -> Self {
        
        // We use Self here because self could be a UIView or any of it's subclasses.
        let view = self.init()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    func rsk_fillParentView(_ margin: CGFloat = 16) -> (
        topLayoutConstraint: NSLayoutConstraint,
        rightLayoutConstraint: NSLayoutConstraint,
        bottomLayoutConstraint: NSLayoutConstraint,
        leftLayoutConstraint: NSLayoutConstraint) {
        let horizontalConstraints = self.rsk_clipHorizontallyToParentView(margin)
        let verticalConstraints = self.rsk_clipVerticallyToParentView(margin)
        return (
            verticalConstraints.topLayoutConstraint,
            horizontalConstraints.rightLayoutConstraint,
            verticalConstraints.bottomLayoutConstraint,
            horizontalConstraints.leftLayoutConstraint
        )
    }
    
    func rsk_clipHorizontallyToParentView(_ margin: CGFloat = 16) -> (leftLayoutConstraint: NSLayoutConstraint, rightLayoutConstraint: NSLayoutConstraint) {
        let childView = self
        let views = ["childView": childView]
        let layoutConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-(\(margin))-[childView]-(\(margin))-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        self.superview!.addConstraints(layoutConstraints)
        return (
            layoutConstraints[0],
            layoutConstraints[1]
        )
    }
    
    func rsk_clipHorizontallyToView(_ view: UIView, margin: CGFloat = 16) -> (leftLayoutConstraint: NSLayoutConstraint, rightLayoutConstraint: NSLayoutConstraint) {
        let leftLayoutConstraint = NSLayoutConstraint(item: self, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1, constant: margin)
        view.addConstraint(leftLayoutConstraint)
        let rightLayoutConstraint = NSLayoutConstraint(item: self, attribute: .right, relatedBy: .equal, toItem: view, attribute: .right, multiplier: 1, constant: -margin)
        view.addConstraint(rightLayoutConstraint)
        return (leftLayoutConstraint, rightLayoutConstraint)
    }
    
    func rsk_clipVerticallyToParentView(_ margin: CGFloat = 0) -> (topLayoutConstraint: NSLayoutConstraint, bottomLayoutConstraint: NSLayoutConstraint) {
        let childView = self
        let views = ["childView": childView]
        let layoutConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-(\(margin))-[childView]-(\(margin))-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        self.superview!.addConstraints(layoutConstraints)
        return (
            layoutConstraints[0],
            layoutConstraints[1]
        )
    }
    
    // TOP
    func rsk_clipTopToTopOfParentView(_ margin: CGFloat = 16) -> NSLayoutConstraint {
        return self.rsk_clipTopToTopOfView(self.superview!, margin: margin)
    }
    
    func rsk_clipTopToTopOfView(_ view: UIView, margin: CGFloat = 16) -> NSLayoutConstraint {
        let layoutConstraint = NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: margin)
        self.superview!.addConstraint(layoutConstraint)
        return layoutConstraint
    }
    
    func rsk_clipTopToBottomOfParentView(_ margin: CGFloat = 16) -> NSLayoutConstraint {
        return self.rsk_clipTopToBottomOfView(self.superview!, margin: margin)
    }
    
    func rsk_clipTopToBottomOfView(_ view: UIView, margin: CGFloat = 16) -> NSLayoutConstraint {
        let layoutConstraint = NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: margin)
        self.superview!.addConstraint(layoutConstraint)
        return layoutConstraint
    }
    
    // RIGHT
    func rsk_clipRightToRightOfParentView(_ margin: CGFloat = FIEUtility.StdSpace) -> NSLayoutConstraint {
        return self.rsk_clipRightToRightOfView(self.superview!, margin: margin)
    }
    
    func rsk_clipRightToRightOfView(_ view: UIView, margin: CGFloat = -FIEUtility.StdSpace) -> NSLayoutConstraint {
        let layoutConstraint = NSLayoutConstraint(item: view, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: margin)
        view.addConstraint(layoutConstraint)
        return layoutConstraint
    }
    
    func rsk_clipRightToLeftOfParentView(_ margin: CGFloat = FIEUtility.StdSpace) -> NSLayoutConstraint {
        return self.rsk_clipRightToLeftOfView(self.superview!, margin: margin)
    }
    
    func rsk_clipRightToLeftOfView(_ view: UIView, margin: CGFloat = FIEUtility.StdSpace) -> NSLayoutConstraint {
        let layoutConstraint = NSLayoutConstraint(item: self, attribute: .right, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1, constant: margin)
        self.superview!.addConstraint(layoutConstraint)
        return layoutConstraint
    }
    
    // BOTTOM
    func rsk_clipBottomToBottomOfParentView(_ margin: CGFloat = -16) -> NSLayoutConstraint {
        return self.rsk_clipBottomToBottomOfView(self.superview!, margin: margin)
    }
    
    func rsk_clipBottomToBottomOfView(_ view: UIView, margin: CGFloat = -16) -> NSLayoutConstraint {
        let layoutConstraint = NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: margin)
        self.superview!.addConstraint(layoutConstraint)
        return layoutConstraint
    }
    
    func rsk_clipBottomToTopOfParentView(_ margin: CGFloat = 16) -> NSLayoutConstraint {
        return self.rsk_clipBottomToTopOfView(self.superview!, margin: margin)
    }
    
    func rsk_clipBottomToTopOfView(_ view: UIView, margin: CGFloat = 16) -> NSLayoutConstraint {
        let layoutConstraint = NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: margin)
        self.superview!.addConstraint(layoutConstraint)
        return layoutConstraint
    }
    
    // LEFT
    func rsk_clipLeftToLeftOfParentView(_ margin: CGFloat = 16) -> NSLayoutConstraint {
        return self.rsk_clipLeftToLeftOfView(self.superview!, margin: margin)
    }
    
    func rsk_clipLeftToLeftOfView(_ view: UIView, margin: CGFloat = 16) -> NSLayoutConstraint {
        let layoutConstraint = NSLayoutConstraint(item: self, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1, constant: margin)
        view.addConstraint(layoutConstraint)
        return layoutConstraint
    }
    
    func rsk_clipLeftToRightOfParentView(_ margin: CGFloat = 16) -> NSLayoutConstraint {
        return self.rsk_clipLeftToRightOfView(self.superview!, margin: margin)
    }
    
    func rsk_clipLeftToRightOfView(_ view: UIView, margin: CGFloat = 16) -> NSLayoutConstraint {
        let layoutConstraint = NSLayoutConstraint(item: self, attribute: .left, relatedBy: .equal, toItem: view, attribute: .right, multiplier: 1, constant: margin)
        self.superview!.addConstraint(layoutConstraint)
        return layoutConstraint
    }
    
    func rsk_clipUpToParentView(_ margin: CGFloat = 16) -> NSLayoutConstraint {
        return self.rsk_clipUpToView(self.superview!, margin: margin)
    }
    
    func rsk_clipUpToView(_ view: UIView, margin: CGFloat = 16) -> NSLayoutConstraint {
        let layoutConstraint = NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: margin)
        self.superview!.addConstraint(layoutConstraint)
        return layoutConstraint
    }
    
    func rsk_clipDownToParentView(_ margin: CGFloat = 16) -> NSLayoutConstraint {
        return self.rsk_clipDownToView(self.superview!, margin: margin)
    }
    
    func rsk_clipDownToView(_ view: UIView, margin: CGFloat = 16) -> NSLayoutConstraint {
        let layoutConstraint = NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: margin)
        self.superview!.addConstraint(layoutConstraint)
        return layoutConstraint
    }
    
    func rsk_setWidth(_ width: CGFloat) -> NSLayoutConstraint {
        let layoutConstraint = NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: self.superview, attribute: .width, multiplier: 0, constant: width)
        self.superview!.addConstraint(layoutConstraint)
        return layoutConstraint
    }
    
    func rsk_setHeight(_ height: CGFloat) -> NSLayoutConstraint {
        let layoutConstraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: self.superview, attribute: .height, multiplier: 0, constant: height)
        self.superview!.addConstraint(layoutConstraint)
        return layoutConstraint
    }
    
    func rsk_centerInParentView() -> (horizontalLayoutConstraint: NSLayoutConstraint, verticalLayoutConstraint: NSLayoutConstraint) {
        let horizontalConstraint = self.rsk_centerHorizontallyInParentView()
        let verticalConstraint = self.rsk_centerVerticallyInParentView()
        self.superview!.addConstraints([horizontalConstraint, verticalConstraint])
        return (
            horizontalConstraint,
            verticalConstraint
        )
    }
    
    func rsk_centerHorizontallyInParentView() -> NSLayoutConstraint {
        let layoutConstraint = NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: self.superview!, attribute: .centerX, multiplier: 1, constant: 0)
        self.superview!.addConstraint(layoutConstraint)
        return layoutConstraint
    }
    
    func rsk_centerVerticallyInParentView() -> NSLayoutConstraint {
        let layoutConstraint = NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: self.superview!, attribute: .centerY, multiplier: 1, constant: 0)
        self.superview!.addConstraint(layoutConstraint)
        return layoutConstraint
    }
    
    func rsk_clipToTopOfParentViewWithYOffset(_ parentView: UIView, YOffset: CGFloat) -> NSLayoutConstraint {
        let layoutConstraint = NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: parentView, attribute: .top, multiplier: 1, constant: YOffset)
        parentView.addConstraint(layoutConstraint)
        return layoutConstraint
    }
    
    func rsk_clipToLeftOfParentViewWithXOffset(_ XOffset: CGFloat = 16) -> NSLayoutConstraint {
        let layoutConstraint = NSLayoutConstraint(item: self, attribute: .left, relatedBy: .equal, toItem: self.superview, attribute: .left, multiplier: 1, constant: XOffset)
        self.superview!.addConstraint(layoutConstraint)
        return layoutConstraint
    }
    
    func rsk_clipToRightOfParentViewWithXOffset(_ XOffset: CGFloat = 16) -> NSLayoutConstraint {
        let layoutConstraint = NSLayoutConstraint(item: self, attribute: .right, relatedBy: .equal, toItem: self.superview, attribute: .right, multiplier: 1, constant: -XOffset)
        self.superview!.addConstraint(layoutConstraint)
        return layoutConstraint
    }
    
    func rsk_addTopMargin(_ margin: CGFloat = 16) -> [NSLayoutConstraint] {
        let layoutConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-(\(margin))-[childView]", options: NSLayoutFormatOptions(), metrics: nil, views: ["childView": self])
        self.superview!.addConstraints(layoutConstraints)
        return layoutConstraints 
    }
}

extension UIImage {
    class func rsk_imageWithColor(_ color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image ?? UIImage()
    }
}

func += <K, V> (left: inout [K : V], right: [K : V]) {
    for (k, v) in right {
        left[k] = v
    }
}

func + <K, V>(left: Dictionary<K, V>, right: Dictionary<K, V>) -> Dictionary<K,V>
{
    var map = Dictionary<K, V>()
    for (k, v) in left {
        map[k] = v
    }
    for (k, v) in right {
        map[k] = v
    }
    return map
}


extension String {
    
    subscript (i: Int) -> Character {
        return self[self.characters.index(self.startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        return substring(with: (characters.index(startIndex, offsetBy: r.lowerBound) ..< characters.index(startIndex, offsetBy: r.upperBound)))
    }
    
    func rsk_toBase64() -> String{
        
        let data = self.data(using: String.Encoding.utf8)
        
        return data!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        
    }
}

extension Date {
    
    static func rsk_hyphenedDateForTimestamp(_ timestamp: TimeInterval) -> String {
        let date = Date(timeIntervalSince1970: timestamp / 1000)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: date)
    }
    static func rsk_dateForTimestamp(_ timestamp: TimeInterval) -> String {
        let date = Date(timeIntervalSince1970: timestamp / 1000)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter.string(from: date)
    }
    static func rsk_dateAndTimeForTimestamp(_ timestamp: TimeInterval) -> String {
        let date = Date(timeIntervalSince1970: timestamp / 1000)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy - HH:mm:ss"
        return dateFormatter.string(from: date)
    }
    static func rsk_ISO8601ForNow() -> String {
        return Date().rsk_ISO8601()
    }
    
    static func rsk_forNow() -> String {
        let dateFormatter = DateFormatter()
        let enUSPOSIXLocale = Locale(identifier: "en_US_POSIX")
        dateFormatter.locale = enUSPOSIXLocale
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let string = dateFormatter.string(from: Date())
        return string
    }
    
    func rsk_dayMonthYear() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "es")
//        dateFormatter.dateFormat = "dd-MM-yy"
        dateFormatter.dateFormat = "yy-MM-dd"
        let iso8601String = dateFormatter.string(from: self)
        return iso8601String
    }
    
    func rsk_month() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "es")
        dateFormatter.dateFormat = "MMMM"
        let iso8601String = dateFormatter.string(from: self)
        return iso8601String
    }
    
    func rsk_year() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "es")
        dateFormatter.dateFormat = "YYYY"
        let string = dateFormatter.string(from: self)
        return string
    }
    
    func rsk_ISO8601() -> String {
        let dateFormatter = DateFormatter()
        let enUSPOSIXLocale = Locale(identifier: "en_US_POSIX")
        dateFormatter.locale = enUSPOSIXLocale
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        let iso8601String = dateFormatter.string(from: self)
        return iso8601String
    }
    
    func rsk_dateByAddingMonths(_ months: Int) -> Date {
        
        let currentCalender = Calendar.current
        let components = NSCalendar.Unit(rawValue: UInt.max) // All components.
        var currentDateComponents = (currentCalender as NSCalendar).components(components, from: self)
        currentDateComponents.month = currentDateComponents.month! + months
        let calculatedDate = currentCalender.date(from: currentDateComponents)!
        return calculatedDate
    }
    
}

//extension String {
//    
//    func rsk_sha1() -> String {
//        let data = self.dataUsingEncoding(NSUTF8StringEncoding)!
//        var digest = [UInt8](count:Int(CC_SHA1_DIGEST_LENGTH), repeatedValue: 0)
//        CC_SHA1(data.bytes, CC_LONG(data.length), &digest)
//        let hexBytes = digest.map { String(format: "%02hhx", $0) }
//        return hexBytes.joinWithSeparator("")
//    }
//    
//    func rsk_base64sha1() -> String {
//        let data = self.dataUsingEncoding(NSUTF8StringEncoding)!
//        var digest = [UInt8](count:Int(CC_SHA1_DIGEST_LENGTH), repeatedValue: 0)
//        CC_SHA1(data.bytes, CC_LONG(data.length), &digest)
//        let data2 = NSData(bytes: digest, length: digest.count)
//        return data2.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
//
//    }
//}



// 1. Declare outside class definition (or in its own file).
// 2. UIKit must be included in file where this code is added.
// 3. Extends UIDevice class, thus is available anywhere in app.
//
// Usage example:
//
//    if UIDevice().type == .simulator {
//       print("You're running on the simulator... boring!")
//    } else {
//       print("Wow! Running on a \(UIDevice().type.rawValue)")
//    }

public enum Model : String {
    case simulator = "simulator/sandbox",
    iPod1          = "iPod 1",
    iPod2          = "iPod 2",
    iPod3          = "iPod 3",
    iPod4          = "iPod 4",
    iPod5          = "iPod 5",
    iPad2          = "iPad 2",
    iPad3          = "iPad 3",
    iPad4          = "iPad 4",
    iPhone4        = "iPhone 4",
    iPhone4S       = "iPhone 4S",
    iPhone5        = "iPhone 5",
    iPhone5S       = "iPhone 5S",
    iPhone5C       = "iPhone 5C",
    iPadMini1      = "iPad Mini 1",
    iPadMini2      = "iPad Mini 2",
    iPadMini3      = "iPad Mini 3",
    iPadAir1       = "iPad Air 1",
    iPadAir2       = "iPad Air 2",
    iPhone6        = "iPhone 6",
    iPhone6plus    = "iPhone 6 Plus",
    iPhone6S       = "iPhone 6S",
    iPhone6Splus   = "iPhone 6S Plus",
    unrecognized   = "?unrecognized?"
}


enum RSKDevice {
    case pad
    case phone
    
    static func device() -> RSKDevice {
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            return phone
        case .pad, .tv, .carPlay, .unspecified:
            return pad
        }
    }
}

public extension UIDevice {
    public var type: Model {
        var systemInfo = utsname()
        uname(&systemInfo)
        let modelCode = withUnsafeMutablePointer(to: &systemInfo.machine) {
            ptr in String(cString: UnsafeRawPointer(ptr).assumingMemoryBound(to: CChar.self))
        }
        var modelMap : [ String : Model ] = [
            "i386"      : .simulator,
            "x86_64"    : .simulator,
            "iPod1,1"   : .iPod1,
            "iPod2,1"   : .iPod2,
            "iPod3,1"   : .iPod3,
            "iPod4,1"   : .iPod4,
            "iPod5,1"   : .iPod5,
            "iPad2,1"   : .iPad2,
            "iPad2,2"   : .iPad2,
            "iPad2,3"   : .iPad2,
            "iPad2,4"   : .iPad2,
            "iPad2,5"   : .iPadMini1,
            "iPad2,6"   : .iPadMini1,
            "iPad2,7"   : .iPadMini1,
            "iPhone3,1" : .iPhone4,
            "iPhone3,2" : .iPhone4,
            "iPhone3,3" : .iPhone4,
            "iPhone4,1" : .iPhone4S,
            "iPhone5,1" : .iPhone5,
            "iPhone5,2" : .iPhone5,
            "iPhone5,3" : .iPhone5C,
            "iPhone5,4" : .iPhone5C,
            "iPad3,1"   : .iPad3,
            "iPad3,2"   : .iPad3,
            "iPad3,3"   : .iPad3,
            "iPad3,4"   : .iPad4,
            "iPad3,5"   : .iPad4,
            "iPad3,6"   : .iPad4,
            "iPhone6,1" : .iPhone5S,
            "iPhone6,2" : .iPhone5S,
            "iPad4,1"   : .iPadAir1,
            "iPad4,2"   : .iPadAir2,
            "iPad4,4"   : .iPadMini2,
            "iPad4,5"   : .iPadMini2,
            "iPad4,6"   : .iPadMini2,
            "iPad4,7"   : .iPadMini3,
            "iPad4,8"   : .iPadMini3,
            "iPad4,9"   : .iPadMini3,
            "iPhone7,1" : .iPhone6plus,
            "iPhone7,2" : .iPhone6,
            "iPhone8,1" : .iPhone6S,
            "iPhone8,2" : .iPhone6Splus
        ]
        
        if let model = modelMap[modelCode] {
            return model
        }
        return Model.unrecognized
    }
    
}
extension UIColor {
    class func rsk_random() -> UIColor {
        let hue : CGFloat = CGFloat(arc4random() % 256) / 256 // use 256 to get full range from 0.0 to 1.0
        let saturation : CGFloat = CGFloat(arc4random() % 128) / 256 + 0.5 // from 0.5 to 1.0 to stay away from white
        let brightness : CGFloat = CGFloat(arc4random() % 128) / 256 + 0.5 // from 0.5 to 1.0 to stay away from black
        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1)
    }
}

extension UIImage {
    class func rsk_imageWithView(_ view: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img ?? UIImage()
    }
    
    func rsk_resizeTo(_ newSize: CGSize) -> UIImage {
        let scale = size.width / size.width
        let newHeight = size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newSize.width, height: newHeight))
        draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage ?? UIImage()
    }
}

extension UISegmentedControl {
    func rsk_removeBorders() {
        setBackgroundImage(imageWithColor(backgroundColor!, size: bounds.size), for: UIControlState(), barMetrics: .default)
        setBackgroundImage(imageWithColor(tintColor!), for: .selected, barMetrics: .default)
        setDividerImage(imageWithColor(UIColor.clear), forLeftSegmentState: UIControlState(), rightSegmentState: UIControlState(), barMetrics: .default)
    }
    
    // create a 1x1 image with this color
    fileprivate func imageWithColor(_ color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor);
        context!.fill(rect);
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return image ?? UIImage()
    }
}
