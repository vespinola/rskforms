//
//  FIECurrencyUtils.swift
//  Fielco-iOS
//
//  Created by Paul Von Schrottky on 11/23/15.
//  Copyright © 2015 Roshka. All rights reserved.
//

import UIKit

typealias FIEAmount = NSDecimalNumber

enum FIECurrencyType {
    case none
    case pyg
    case usd
    
    static func typeForISO(_ code: String) -> FIECurrencyType? {
        switch code {
        case "PYG":
            return .pyg
        case "GS":
            return .pyg
        case "USD":
            return .usd
        default:
            return nil
        }
    }
    
    func code() -> Int? {
        switch self {
        case .pyg:
            return 6900
        case .usd:
            return 1
        default:
            return nil
        }
    }
    
    func plural() -> String? {
        switch self {
        case .pyg:
            return "guaraníes"
        case .usd:
            return "dólares estadounidenses"
        default:
            return nil
        }
    }
}

extension FIEAmount {
    
    func fie_formatThousandsWithCurrency(_ currency: FIECurrencyType, currencySymbol: String? = nil, showCurrency: Bool = true, showMinimalCents: Bool = false) -> String {
        
        let GROUPING_SEPARATOR = "."
        let DECIMAL_SEPARATOR = ","
        let MAX_DECIMAL_PLACES = 2
        
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = GROUPING_SEPARATOR
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        formatter.decimalSeparator = DECIMAL_SEPARATOR
        
//        var decimalPlaces = 0   // 0, 1 or 2 decimal places can be shown.
//        let decimalPointRange = stringValue.rangeOfString(DECIMAL_SEPARATOR)
//        
//        if let decimalPointRange = decimalPointRange {
//            let decimalPointIndex = stringValue.startIndex.distanceTo(decimalPointRange.startIndex)
//            print(decimalPointIndex)
//            decimalPlaces = stringValue.characters.count - 1 - decimalPointIndex
//        }
//        
//        decimalPlaces = decimalPlaces > MAX_DECIMAL_PLACES ? MAX_DECIMAL_PLACES : decimalPlaces // Limit decimal places
//        
//        if currency == .PYG {
//            decimalPlaces = 0
//        }
        
        
        if (currency == .pyg) {
            formatter.minimumFractionDigits = 0
            formatter.maximumFractionDigits = 0
        } else if currency == .usd {
            formatter.minimumFractionDigits = showMinimalCents ? 0 : MAX_DECIMAL_PLACES
            formatter.maximumFractionDigits = MAX_DECIMAL_PLACES
        }
        
        var formattedAmount = formatter.string(from: self)!
        
        if let currencySymbol = currencySymbol , showCurrency == true {
            formattedAmount = formattedAmount + " " + currencySymbol
        }
        
        return formattedAmount
    }
    
    func fie_formatThousandsWithCurrencyFromProduct(_ product: FIEDictionary, showCurrency: Bool = true) -> String? {
        let currencyType = FIEUtility.currencyTypeForProduct(product)
        let currencySymbol = FIEUtility.currencySymbolForProduct(product)
        return self.fie_formatThousandsWithCurrency(currencyType, currencySymbol: currencySymbol, showCurrency: showCurrency)
    }
    
    func fie_formatThousandsWithCurrencyInfo(_ currencyInfo: FIEDictionary, showCurrency: Bool = true) -> String? {
        let currencyType = FIEUtility.currencyTypeForCurrencyInfo(currencyInfo)
        let currencySymbol = FIEUtility.currencySymbolForCurrencyInfo(currencyInfo)
        return self.fie_formatThousandsWithCurrency(currencyType, currencySymbol: currencySymbol, showCurrency: showCurrency)
    }
    
    func fie_isGreaterThanZero() -> Bool {
        return compare(NSDecimalNumber.zero) == ComparisonResult.orderedDescending
    }
}

extension String {
    func fie_unformatAmount() -> FIEAmount? {
        if self == "" {
            return nil
        }
        var components = self.components(separatedBy: " ")
        
        // Discard anything after a space, e.g. "34.434,54 U$D." -> "34.434,54"
        var raw = components[0].replacingOccurrences(of: ".", with: "")
        raw = raw.replacingOccurrences(of: ",", with: ".")
        return FIEAmount(string: raw)
    }
    
    func fie_reformatAmountInRange(_ range: NSRange, string: String, currencyType: FIECurrencyType) -> String {
        let newText = (self as NSString).replacingCharacters(in: range, with: string)
        let newRawAmount = newText.fie_unformatAmount()
        let newlyFormattedAmountString = newRawAmount!.fie_formatThousandsWithCurrency(currencyType)
        return newlyFormattedAmountString
    }
}


